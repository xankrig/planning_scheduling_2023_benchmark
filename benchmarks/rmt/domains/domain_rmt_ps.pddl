(define (domain ps_domain_rmt)
    (:requirements :strips :fluents :durative-actions :timed-initial-literals :typing :conditional-effects :negative-preconditions :duration-inequalities :equality :ps-task)
    (:types 
        resource - object
        process - object
        attribute - object
        machine - resource
        configuration - attribute
    )
    (:attributes 
        (configured ?m - machine ?c - configuration) 
        (configurable ?m - machine ?c - configuration)
    ) 	
    (:static
        (not-same ?c1 - configuration ?c2 - configuration)
        (processable ?c - configuration ?p - process) 
        (depends-one ?p1 - process ?p2 - process) 
        (not-depends ?p - process)
    )
    (:predicates 
        (completed ?p - process)
        (not-completed ?p - process)      
    )
    (:functions
        (reconfiguration-time ?c1 - configuration ?c2 - configuration)
	(reconfiguration-cost ?c1 - configuration ?c2 - configuration)
        (processing-time ?c - configuration ?p - process)       
	(processing-cost ?c - configuration ?p - process)          
    )
    (:maintenance-activity reconfigure_machine 
        :parameters 
        (
            ?c1 ?c2 - configuration         
        )
        :resource
        (
            ?m - machine
        ) 
        :attributes
        (and
            (configured ?m ?c1) 
        )
        :duration (= ?duration (reconfiguration-time ?c1 ?c2)) 
        :cost (= ?cost (reconfiguration-cost ?c1 ?c2))
        :static (and (configurable ?m ?c2) (not-same ?c1 ?c2))
        :rem-effect
        (and (configured ?m ?c1))
        :add-effect 
        (and (configured ?m ?c2)
        )
    )
    (:production-activity process_process_without 
        :parameters 
        (
            ?c - configuration
            ?p - process
        )
        :attributes 
        (and
            (for ?m - machine) (and (configured ?m ?c)) 
        ) 
        :duration (= ?duration (processing-time ?c ?p)) 
        :cost (= ?cost (processing-cost ?c ?p))
        :static 
        (and 
            (not-depends ?p) (processable ?c ?p))
        :precondition 
        (and 
            (not-completed ?p)
        )
        :del-effect 
        (and 
            (not-completed ?p)
        )
        :add-effect 
        (and 
            (completed ?p))
    )
    (:production-activity process_process_with_1p
        :parameters
        (
            ?c - configuration
            ?p1 ?p2 - process
        )
        :attributes 
        (and
            (for ?m - machine) (and (configured ?m ?c))
        )
        :duration (= ?duration (processing-time ?c ?p1))
        :cost (= ?cost (processing-cost ?c ?p1))
        :static 
        (and
            (depends-one ?p1 ?p2)
            (processable ?c ?p1)
        )
        :precondition 
        (and
             (completed ?p2) (not-completed ?p1)

        )
        :del-effect
        (and
            (not-completed ?p1))
        :add-effect
        (and
            (completed ?p1)))
)