(define (domain ps_domain_rmt)
    (:requirements :strips :typing :conditional-effects :negative-preconditions :equality)
    (:types
        resource - object
        process - object
        attribute - object
        machine - resource
        configuration - attribute
    )
    (:predicates
        (completed ?p - process)
        (not-completed ?p - process)
        (configured ?m - machine ?c - configuration)
        (configurable ?m - machine ?c - configuration)
        (not-same ?c1 - configuration ?c2 - configuration)
        (processable ?c - configuration ?p - process)
        (depends-one ?p1 - process ?p2 - process)
        (not-depends ?p - process)
    )
    (:functions
        (reconfiguration-cost ?c1 - configuration ?c2 - configuration)
        (processing-cost ?c - configuration ?p - process)
        (total-cost)
    )
    (:action reconfigure_machine
        :parameters
        (
            ?c1 ?c2 - configuration
            ?m - machine
        )
        :precondition
        (and
            (configurable ?m ?c2)
            (not-same ?c1 ?c2)
            (configured ?m ?c1)
        )
        :effect
        (and
            (not (configured ?m ?c1))
            (configured ?m ?c2)
            (increase (total-cost) (reconfiguration-cost ?c1 ?c2))
        )
    )
    (:action process_process_without
        :parameters
        (
            ?c - configuration
            ?p - process
            ?m - machine
        )
        :precondition
        (and 
            (not-depends ?p)
            (processable ?c ?p)
            (not-completed ?p)
            (configured ?m ?c)
        )
        :effect
        (and
            (not (not-completed ?p))
            (completed ?p)
            (increase (total-cost) (processing-cost ?c ?p))
        )
    )
    (:action process_process_with_1p
        :parameters
        (
            ?c - configuration
            ?p1 ?p2 - process
            ?m - machine
        )
        :precondition
        (and
            (depends-one ?p1 ?p2)
            (processable ?c ?p1)
            (completed ?p2)
            (not-completed ?p1)
            (configured ?m ?c)
        )
        :effect
        (and
            (not (not-completed ?p1))
            (completed ?p1)
            (increase (total-cost) (processing-cost ?c ?p1))
        )
    )
)