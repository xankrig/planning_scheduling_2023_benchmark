(define (domain ps_domain_rmt)

(:requirements :strips :fluents :durative-actions :timed-initial-literals :typing :conditional-effects :negative-preconditions :duration-inequalities :equality)
    (:types
        resource - object
        process - object
        attribute - object
        machine - resource
        configuration - attribute
    )
    (:predicates
	    (completed ?p - process)
	    (not-completed ?p - process)
	    (configured ?m - machine ?c - configuration)
	    (configurable ?m - machine ?c - configuration)
	    (not-same ?c1 - configuration ?c2 - configuration)
	    (processable ?c - configuration ?p - process)
	    (depends-one ?p1 - process ?p2 - process)
	    (not-depends ?p - process)
	    (available ?r - resource)
    )
    (:functions
	    (reconfiguration-time ?c1 - configuration ?c2 - configuration)
	    (processing-time ?c - configuration ?p - process)
    )
    (:durative-action reconfigure_machine
	:parameters
	(
	    ?c1 ?c2 - configuration
	    ?m - machine
	)
	:duration (= ?duration (reconfiguration-time ?c1 ?c2)) 
	:condition
	(and
	    (at start (configurable ?m ?c2))
	    (at start (not-same ?c1 ?c2))
	    (at start (configured ?m ?c1))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (configured ?m ?c1)))
	    (at start (not (available ?m)))
	    (at end (configured ?m ?c2))
	    (at end (available ?m))
	)
    )
    (:durative-action process_process_without
	:parameters
	(
	    ?c - configuration
	    ?p - process
	    ?m - machine
	)
	:duration (= ?duration (processing-time ?c ?p)) 
	:condition
        (and 
	    (at start (not-depends ?p))
	    (at start (processable ?c ?p))
	    (at start (not-completed ?p))
	    (at start (configured ?m ?c))
	    (at start (available ?m))
        )
	:effect
	(and
	    (at start (not (not-completed ?p)))
	    (at start (not (available ?m)))
	    (at end (completed ?p))
	    (at end (available ?m))
	)
    )
    (:durative-action process_process_with_1p
	:parameters
	(
	    ?c - configuration
	    ?p1 ?p2 - process
	    ?m - machine
	)
	:duration (= ?duration (processing-time ?c ?p1))
	:condition
        (and
	    (at start (depends-one ?p1 ?p2))
	    (at start (processable ?c ?p1))
	    (at start (completed ?p2))
	    (at start (not-completed ?p1))
	    (at start (configured ?m ?c))
	    (at start (available ?m))
        )
	:effect
	(and
	    (at start (not (not-completed ?p1)))
	    (at start (not (available ?m)))
	    (at end (completed ?p1))
	    (at end (available ?m))
	)
    )
)
