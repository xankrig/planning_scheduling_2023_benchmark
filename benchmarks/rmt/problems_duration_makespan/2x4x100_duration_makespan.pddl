(define
	(problem ps_domain_rmt-instance)
	(:domain ps_domain_rmt)
	(:objects
		m0 m1 - machine
		c0 c1 c2 c3 - configuration
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 p30 p31 p32 p33 p34 p35 p36 p37 p38 p39 p40 p41 p42 p43 p44 p45 p46 p47 p48 p49 p50 p51 p52 p53 p54 p55 p56 p57 p58 p59 p60 p61 p62 p63 p64 p65 p66 p67 p68 p69 p70 p71 p72 p73 p74 p75 p76 p77 p78 p79 p80 p81 p82 p83 p84 p85 p86 p87 p88 p89 p90 p91 p92 p93 p94 p95 p96 p97 p98 p99 - process
	)
	(:init
		(available m0)
		(available m1)
		(configured m0 c0)
		(configured m1 c3)
		(configurable m0 c0)
		(configurable m0 c1)
		(configurable m0 c2)
		(configurable m1 c2)
		(configurable m1 c3)
		(= (reconfiguration-time c0 c1) 5)
		(= (reconfiguration-time c1 c0) 5)
		(not-same c0 c1)
		(not-same c1 c0)
		(= (reconfiguration-time c0 c2) 9)
		(= (reconfiguration-time c2 c0) 9)
		(not-same c0 c2)
		(not-same c2 c0)
		(= (reconfiguration-time c1 c2) 4)
		(= (reconfiguration-time c2 c1) 4)
		(not-same c1 c2)
		(not-same c2 c1)
		(= (reconfiguration-time c2 c3) 1)
		(= (reconfiguration-time c3 c2) 1)
		(not-same c2 c3)
		(not-same c3 c2)
		(processable c0 p4)
		(processable c0 p5)
		(processable c0 p6)
		(processable c0 p7)
		(processable c0 p8)
		(processable c0 p9)
		(processable c0 p10)
		(processable c0 p11)
		(processable c0 p20)
		(processable c0 p21)
		(processable c0 p22)
		(processable c0 p23)
		(processable c0 p32)
		(processable c0 p33)
		(processable c0 p34)
		(processable c0 p35)
		(processable c0 p44)
		(processable c0 p45)
		(processable c0 p46)
		(processable c0 p47)
		(processable c0 p48)
		(processable c0 p49)
		(processable c0 p50)
		(processable c0 p51)
		(processable c0 p76)
		(processable c0 p77)
		(processable c0 p78)
		(processable c0 p79)
		(processable c0 p92)
		(processable c0 p93)
		(processable c0 p94)
		(processable c0 p95)
		(processable c1 p0)
		(processable c1 p1)
		(processable c1 p2)
		(processable c1 p3)
		(processable c1 p8)
		(processable c1 p9)
		(processable c1 p10)
		(processable c1 p11)
		(processable c1 p12)
		(processable c1 p13)
		(processable c1 p14)
		(processable c1 p15)
		(processable c1 p20)
		(processable c1 p21)
		(processable c1 p22)
		(processable c1 p23)
		(processable c1 p24)
		(processable c1 p25)
		(processable c1 p26)
		(processable c1 p27)
		(processable c1 p32)
		(processable c1 p33)
		(processable c1 p34)
		(processable c1 p35)
		(processable c1 p48)
		(processable c1 p49)
		(processable c1 p50)
		(processable c1 p51)
		(processable c1 p56)
		(processable c1 p57)
		(processable c1 p58)
		(processable c1 p59)
		(processable c1 p60)
		(processable c1 p61)
		(processable c1 p62)
		(processable c1 p63)
		(processable c1 p72)
		(processable c1 p73)
		(processable c1 p74)
		(processable c1 p75)
		(processable c1 p76)
		(processable c1 p77)
		(processable c1 p78)
		(processable c1 p79)
		(processable c1 p88)
		(processable c1 p89)
		(processable c1 p90)
		(processable c1 p91)
		(processable c1 p92)
		(processable c1 p93)
		(processable c1 p94)
		(processable c1 p95)
		(processable c2 p28)
		(processable c2 p29)
		(processable c2 p30)
		(processable c2 p31)
		(processable c2 p36)
		(processable c2 p37)
		(processable c2 p38)
		(processable c2 p39)
		(processable c2 p40)
		(processable c2 p41)
		(processable c2 p42)
		(processable c2 p43)
		(processable c2 p52)
		(processable c2 p53)
		(processable c2 p54)
		(processable c2 p55)
		(processable c3 p16)
		(processable c3 p17)
		(processable c3 p18)
		(processable c3 p19)
		(processable c3 p44)
		(processable c3 p45)
		(processable c3 p46)
		(processable c3 p47)
		(processable c3 p64)
		(processable c3 p65)
		(processable c3 p66)
		(processable c3 p67)
		(processable c3 p68)
		(processable c3 p69)
		(processable c3 p70)
		(processable c3 p71)
		(processable c3 p72)
		(processable c3 p73)
		(processable c3 p74)
		(processable c3 p75)
		(processable c3 p80)
		(processable c3 p81)
		(processable c3 p82)
		(processable c3 p83)
		(processable c3 p84)
		(processable c3 p85)
		(processable c3 p86)
		(processable c3 p87)
		(processable c3 p96)
		(processable c3 p97)
		(processable c3 p98)
		(processable c3 p99)
		(= (processing-time c0 p4) 3)
		(= (processing-time c0 p5) 2)
		(= (processing-time c0 p6) 1)
		(= (processing-time c0 p7) 2)
		(= (processing-time c0 p8) 1)
		(= (processing-time c0 p9) 1)
		(= (processing-time c0 p10) 2)
		(= (processing-time c0 p11) 4)
		(= (processing-time c0 p20) 3)
		(= (processing-time c0 p21) 1)
		(= (processing-time c0 p22) 1)
		(= (processing-time c0 p23) 2)
		(= (processing-time c0 p32) 1)
		(= (processing-time c0 p33) 1)
		(= (processing-time c0 p34) 1)
		(= (processing-time c0 p35) 2)
		(= (processing-time c0 p44) 3)
		(= (processing-time c0 p45) 1)
		(= (processing-time c0 p46) 1)
		(= (processing-time c0 p47) 2)
		(= (processing-time c0 p48) 2)
		(= (processing-time c0 p49) 1)
		(= (processing-time c0 p50) 1)
		(= (processing-time c0 p51) 2)
		(= (processing-time c0 p76) 1)
		(= (processing-time c0 p77) 1)
		(= (processing-time c0 p78) 1)
		(= (processing-time c0 p79) 3)
		(= (processing-time c0 p92) 1)
		(= (processing-time c0 p93) 1)
		(= (processing-time c0 p94) 2)
		(= (processing-time c0 p95) 2)
		(= (processing-time c1 p0) 6)
		(= (processing-time c1 p1) 1)
		(= (processing-time c1 p2) 2)
		(= (processing-time c1 p3) 2)
		(= (processing-time c1 p8) 2)
		(= (processing-time c1 p9) 1)
		(= (processing-time c1 p10) 1)
		(= (processing-time c1 p11) 2)
		(= (processing-time c1 p12) 6)
		(= (processing-time c1 p13) 1)
		(= (processing-time c1 p14) 1)
		(= (processing-time c1 p15) 2)
		(= (processing-time c1 p20) 1)
		(= (processing-time c1 p21) 1)
		(= (processing-time c1 p22) 1)
		(= (processing-time c1 p23) 3)
		(= (processing-time c1 p24) 1)
		(= (processing-time c1 p25) 1)
		(= (processing-time c1 p26) 1)
		(= (processing-time c1 p27) 2)
		(= (processing-time c1 p32) 2)
		(= (processing-time c1 p33) 1)
		(= (processing-time c1 p34) 1)
		(= (processing-time c1 p35) 2)
		(= (processing-time c1 p48) 3)
		(= (processing-time c1 p49) 1)
		(= (processing-time c1 p50) 1)
		(= (processing-time c1 p51) 2)
		(= (processing-time c1 p56) 2)
		(= (processing-time c1 p57) 1)
		(= (processing-time c1 p58) 1)
		(= (processing-time c1 p59) 2)
		(= (processing-time c1 p60) 2)
		(= (processing-time c1 p61) 1)
		(= (processing-time c1 p62) 1)
		(= (processing-time c1 p63) 2)
		(= (processing-time c1 p72) 1)
		(= (processing-time c1 p73) 1)
		(= (processing-time c1 p74) 1)
		(= (processing-time c1 p75) 7)
		(= (processing-time c1 p76) 3)
		(= (processing-time c1 p77) 1)
		(= (processing-time c1 p78) 2)
		(= (processing-time c1 p79) 2)
		(= (processing-time c1 p88) 1)
		(= (processing-time c1 p89) 1)
		(= (processing-time c1 p90) 1)
		(= (processing-time c1 p91) 2)
		(= (processing-time c1 p92) 1)
		(= (processing-time c1 p93) 1)
		(= (processing-time c1 p94) 1)
		(= (processing-time c1 p95) 2)
		(= (processing-time c2 p28) 2)
		(= (processing-time c2 p29) 1)
		(= (processing-time c2 p30) 2)
		(= (processing-time c2 p31) 2)
		(= (processing-time c2 p36) 1)
		(= (processing-time c2 p37) 2)
		(= (processing-time c2 p38) 2)
		(= (processing-time c2 p39) 3)
		(= (processing-time c2 p40) 2)
		(= (processing-time c2 p41) 2)
		(= (processing-time c2 p42) 1)
		(= (processing-time c2 p43) 2)
		(= (processing-time c2 p52) 4)
		(= (processing-time c2 p53) 1)
		(= (processing-time c2 p54) 2)
		(= (processing-time c2 p55) 4)
		(= (processing-time c3 p16) 1)
		(= (processing-time c3 p17) 1)
		(= (processing-time c3 p18) 2)
		(= (processing-time c3 p19) 2)
		(= (processing-time c3 p44) 1)
		(= (processing-time c3 p45) 2)
		(= (processing-time c3 p46) 1)
		(= (processing-time c3 p47) 2)
		(= (processing-time c3 p64) 4)
		(= (processing-time c3 p65) 1)
		(= (processing-time c3 p66) 1)
		(= (processing-time c3 p67) 3)
		(= (processing-time c3 p68) 1)
		(= (processing-time c3 p69) 1)
		(= (processing-time c3 p70) 1)
		(= (processing-time c3 p71) 2)
		(= (processing-time c3 p72) 3)
		(= (processing-time c3 p73) 1)
		(= (processing-time c3 p74) 2)
		(= (processing-time c3 p75) 2)
		(= (processing-time c3 p80) 1)
		(= (processing-time c3 p81) 1)
		(= (processing-time c3 p82) 1)
		(= (processing-time c3 p83) 2)
		(= (processing-time c3 p84) 1)
		(= (processing-time c3 p85) 1)
		(= (processing-time c3 p86) 2)
		(= (processing-time c3 p87) 2)
		(= (processing-time c3 p96) 5)
		(= (processing-time c3 p97) 4)
		(= (processing-time c3 p98) 2)
		(= (processing-time c3 p99) 2)
		(not-depends p0)
		(depends-one p1 p0)
		(depends-one p2 p1)
		(depends-one p3 p2)
		(not-depends p4)
		(not-depends p5)
		(not-depends p6)
		(not-depends p7)
		(depends-one p8 p9)
		(depends-one p9 p10)
		(depends-one p10 p11)
		(not-depends p11)
		(depends-one p12 p13)
		(not-depends p13)
		(depends-one p14 p12)
		(not-depends p15)
		(depends-one p16 p17)
		(not-depends p17)
		(not-depends p18)
		(not-depends p19)
		(not-depends p20)
		(not-depends p21)
		(not-depends p22)
		(not-depends p23)
		(not-depends p24)
		(not-depends p25)
		(not-depends p26)
		(depends-one p27 p24)
		(not-depends p28)
		(depends-one p29 p30)
		(depends-one p30 p31)
		(not-depends p31)
		(not-depends p32)
		(not-depends p33)
		(not-depends p34)
		(not-depends p35)
		(not-depends p36)
		(depends-one p37 p38)
		(not-depends p38)
		(not-depends p39)
		(depends-one p40 p43)
		(depends-one p41 p43)
		(not-depends p42)
		(not-depends p43)
		(not-depends p44)
		(not-depends p45)
		(not-depends p46)
		(depends-one p47 p46)
		(not-depends p48)
		(not-depends p49)
		(depends-one p50 p49)
		(not-depends p51)
		(not-depends p52)
		(not-depends p53)
		(not-depends p54)
		(not-depends p55)
		(not-depends p56)
		(depends-one p57 p58)
		(not-depends p58)
		(not-depends p59)
		(depends-one p60 p61)
		(not-depends p61)
		(not-depends p62)
		(not-depends p63)
		(not-depends p64)
		(not-depends p65)
		(not-depends p66)
		(not-depends p67)
		(not-depends p68)
		(depends-one p69 p70)
		(not-depends p70)
		(not-depends p71)
		(depends-one p72 p74)
		(not-depends p73)
		(not-depends p74)
		(depends-one p75 p72)
		(not-depends p76)
		(not-depends p77)
		(not-depends p78)
		(not-depends p79)
		(depends-one p80 p82)
		(not-depends p81)
		(depends-one p82 p81)
		(not-depends p83)
		(depends-one p84 p86)
		(not-depends p85)
		(not-depends p86)
		(not-depends p87)
		(depends-one p88 p90)
		(not-depends p89)
		(depends-one p90 p91)
		(not-depends p91)
		(depends-one p92 p93)
		(not-depends p93)
		(not-depends p94)
		(not-depends p95)
		(not-depends p96)
		(depends-one p97 p98)
		(not-depends p98)
		(not-depends p99)
		(not-completed p0)
		(not-completed p1)
		(not-completed p2)
		(not-completed p3)
		(not-completed p4)
		(not-completed p5)
		(not-completed p6)
		(not-completed p7)
		(not-completed p8)
		(not-completed p9)
		(not-completed p10)
		(not-completed p11)
		(not-completed p12)
		(not-completed p13)
		(not-completed p14)
		(not-completed p15)
		(not-completed p16)
		(not-completed p17)
		(not-completed p18)
		(not-completed p19)
		(not-completed p20)
		(not-completed p21)
		(not-completed p22)
		(not-completed p23)
		(not-completed p24)
		(not-completed p25)
		(not-completed p26)
		(not-completed p27)
		(not-completed p28)
		(not-completed p29)
		(not-completed p30)
		(not-completed p31)
		(not-completed p32)
		(not-completed p33)
		(not-completed p34)
		(not-completed p35)
		(not-completed p36)
		(not-completed p37)
		(not-completed p38)
		(not-completed p39)
		(not-completed p40)
		(not-completed p41)
		(not-completed p42)
		(not-completed p43)
		(not-completed p44)
		(not-completed p45)
		(not-completed p46)
		(not-completed p47)
		(not-completed p48)
		(not-completed p49)
		(not-completed p50)
		(not-completed p51)
		(not-completed p52)
		(not-completed p53)
		(not-completed p54)
		(not-completed p55)
		(not-completed p56)
		(not-completed p57)
		(not-completed p58)
		(not-completed p59)
		(not-completed p60)
		(not-completed p61)
		(not-completed p62)
		(not-completed p63)
		(not-completed p64)
		(not-completed p65)
		(not-completed p66)
		(not-completed p67)
		(not-completed p68)
		(not-completed p69)
		(not-completed p70)
		(not-completed p71)
		(not-completed p72)
		(not-completed p73)
		(not-completed p74)
		(not-completed p75)
		(not-completed p76)
		(not-completed p77)
		(not-completed p78)
		(not-completed p79)
		(not-completed p80)
		(not-completed p81)
		(not-completed p82)
		(not-completed p83)
		(not-completed p84)
		(not-completed p85)
		(not-completed p86)
		(not-completed p87)
		(not-completed p88)
		(not-completed p89)
		(not-completed p90)
		(not-completed p91)
		(not-completed p92)
		(not-completed p93)
		(not-completed p94)
		(not-completed p95)
		(not-completed p96)
		(not-completed p97)
		(not-completed p98)
		(not-completed p99)
	)
	(:goal
		(and
			(completed p0)
			(completed p1)
			(completed p2)
			(completed p3)
			(completed p4)
			(completed p5)
			(completed p6)
			(completed p7)
			(completed p8)
			(completed p9)
			(completed p10)
			(completed p11)
			(completed p12)
			(completed p13)
			(completed p14)
			(completed p15)
			(completed p16)
			(completed p17)
			(completed p18)
			(completed p19)
			(completed p20)
			(completed p21)
			(completed p22)
			(completed p23)
			(completed p24)
			(completed p25)
			(completed p26)
			(completed p27)
			(completed p28)
			(completed p29)
			(completed p30)
			(completed p31)
			(completed p32)
			(completed p33)
			(completed p34)
			(completed p35)
			(completed p36)
			(completed p37)
			(completed p38)
			(completed p39)
			(completed p40)
			(completed p41)
			(completed p42)
			(completed p43)
			(completed p44)
			(completed p45)
			(completed p46)
			(completed p47)
			(completed p48)
			(completed p49)
			(completed p50)
			(completed p51)
			(completed p52)
			(completed p53)
			(completed p54)
			(completed p55)
			(completed p56)
			(completed p57)
			(completed p58)
			(completed p59)
			(completed p60)
			(completed p61)
			(completed p62)
			(completed p63)
			(completed p64)
			(completed p65)
			(completed p66)
			(completed p67)
			(completed p68)
			(completed p69)
			(completed p70)
			(completed p71)
			(completed p72)
			(completed p73)
			(completed p74)
			(completed p75)
			(completed p76)
			(completed p77)
			(completed p78)
			(completed p79)
			(completed p80)
			(completed p81)
			(completed p82)
			(completed p83)
			(completed p84)
			(completed p85)
			(completed p86)
			(completed p87)
			(completed p88)
			(completed p89)
			(completed p90)
			(completed p91)
			(completed p92)
			(completed p93)
			(completed p94)
			(completed p95)
			(completed p96)
			(completed p97)
			(completed p98)
			(completed p99)
		)
	)
	(:metric minimize (total-time))		; optimal cost = 629
)