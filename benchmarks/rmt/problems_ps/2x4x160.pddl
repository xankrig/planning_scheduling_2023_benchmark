(define

	(problem ps_domain_rmt-instance)
	(:domain ps_domain_rmt)

	(:objects
		m0 m1 - machine
		c0 c1 c2 c3 - configuration
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 p30 p31 p32 p33 p34 p35 p36 p37 p38 p39 p40 p41 p42 p43 p44 p45 p46 p47 p48 p49 p50 p51 p52 p53 p54 p55 p56 p57 p58 p59 p60 p61 p62 p63 p64 p65 p66 p67 p68 p69 p70 p71 p72 p73 p74 p75 p76 p77 p78 p79 p80 p81 p82 p83 p84 p85 p86 p87 p88 p89 p90 p91 p92 p93 p94 p95 p96 p97 p98 p99 p100 p101 p102 p103 p104 p105 p106 p107 p108 p109 p110 p111 p112 p113 p114 p115 p116 p117 p118 p119 p120 p121 p122 p123 p124 p125 p126 p127 p128 p129 p130 p131 p132 p133 p134 p135 p136 p137 p138 p139 p140 p141 p142 p143 p144 p145 p146 p147 p148 p149 p150 p151 p152 p153 p154 p155 p156 p157 p158 p159 - process
	)

	(:init

		(configured m0 c1)
		(configured m1 c0)

		(configurable m0 c0)
		(configurable m0 c1)
		(configurable m0 c3)
		(configurable m1 c0)
		(configurable m1 c2)
		(= (reconfiguration-cost c0 c1) 20)
		(= (reconfiguration-time c0 c1) 10)
		(= (reconfiguration-cost c1 c0) 14)
		(= (reconfiguration-time c1 c0) 10)
		(not-same c0 c1)
		(not-same c1 c0)
		(= (reconfiguration-cost c0 c2) 15)
		(= (reconfiguration-time c0 c2) 8)
		(= (reconfiguration-cost c2 c0) 33)
		(= (reconfiguration-time c2 c0) 8)
		(not-same c0 c2)
		(not-same c2 c0)
		(= (reconfiguration-cost c0 c3) 25)
		(= (reconfiguration-time c0 c3) 5)
		(= (reconfiguration-cost c3 c0) 13)
		(= (reconfiguration-time c3 c0) 5)
		(not-same c0 c3)
		(not-same c3 c0)
		(= (reconfiguration-cost c1 c3) 4)
		(= (reconfiguration-time c1 c3) 1)
		(= (reconfiguration-cost c3 c1) 4)
		(= (reconfiguration-time c3 c1) 1)
		(not-same c1 c3)
		(not-same c3 c1)


		(processable c0 p0)
		(processable c0 p1)
		(processable c0 p2)
		(processable c0 p3)
		(processable c0 p4)
		(processable c0 p5)
		(processable c0 p6)
		(processable c0 p7)
		(processable c0 p12)
		(processable c0 p13)
		(processable c0 p14)
		(processable c0 p15)
		(processable c0 p44)
		(processable c0 p45)
		(processable c0 p46)
		(processable c0 p47)
		(processable c0 p88)
		(processable c0 p89)
		(processable c0 p90)
		(processable c0 p91)
		(processable c0 p96)
		(processable c0 p97)
		(processable c0 p98)
		(processable c0 p99)
		(processable c0 p116)
		(processable c0 p117)
		(processable c0 p118)
		(processable c0 p119)
		(processable c0 p120)
		(processable c0 p121)
		(processable c0 p122)
		(processable c0 p123)
		(processable c0 p132)
		(processable c0 p133)
		(processable c0 p134)
		(processable c0 p135)
		(processable c0 p136)
		(processable c0 p137)
		(processable c0 p138)
		(processable c0 p139)
		(processable c0 p156)
		(processable c0 p157)
		(processable c0 p158)
		(processable c0 p159)
		(processable c1 p0)
		(processable c1 p1)
		(processable c1 p2)
		(processable c1 p3)
		(processable c1 p8)
		(processable c1 p9)
		(processable c1 p10)
		(processable c1 p11)
		(processable c1 p20)
		(processable c1 p21)
		(processable c1 p22)
		(processable c1 p23)
		(processable c1 p32)
		(processable c1 p33)
		(processable c1 p34)
		(processable c1 p35)
		(processable c1 p44)
		(processable c1 p45)
		(processable c1 p46)
		(processable c1 p47)
		(processable c1 p48)
		(processable c1 p49)
		(processable c1 p50)
		(processable c1 p51)
		(processable c1 p52)
		(processable c1 p53)
		(processable c1 p54)
		(processable c1 p55)
		(processable c1 p68)
		(processable c1 p69)
		(processable c1 p70)
		(processable c1 p71)
		(processable c1 p80)
		(processable c1 p81)
		(processable c1 p82)
		(processable c1 p83)
		(processable c1 p84)
		(processable c1 p85)
		(processable c1 p86)
		(processable c1 p87)
		(processable c1 p108)
		(processable c1 p109)
		(processable c1 p110)
		(processable c1 p111)
		(processable c1 p112)
		(processable c1 p113)
		(processable c1 p114)
		(processable c1 p115)
		(processable c1 p116)
		(processable c1 p117)
		(processable c1 p118)
		(processable c1 p119)
		(processable c1 p120)
		(processable c1 p121)
		(processable c1 p122)
		(processable c1 p123)
		(processable c1 p128)
		(processable c1 p129)
		(processable c1 p130)
		(processable c1 p131)
		(processable c1 p136)
		(processable c1 p137)
		(processable c1 p138)
		(processable c1 p139)
		(processable c1 p140)
		(processable c1 p141)
		(processable c1 p142)
		(processable c1 p143)
		(processable c1 p156)
		(processable c1 p157)
		(processable c1 p158)
		(processable c1 p159)
		(processable c2 p16)
		(processable c2 p17)
		(processable c2 p18)
		(processable c2 p19)
		(processable c2 p28)
		(processable c2 p29)
		(processable c2 p30)
		(processable c2 p31)
		(processable c2 p36)
		(processable c2 p37)
		(processable c2 p38)
		(processable c2 p39)
		(processable c2 p60)
		(processable c2 p61)
		(processable c2 p62)
		(processable c2 p63)
		(processable c2 p80)
		(processable c2 p81)
		(processable c2 p82)
		(processable c2 p83)
		(processable c2 p88)
		(processable c2 p89)
		(processable c2 p90)
		(processable c2 p91)
		(processable c2 p104)
		(processable c2 p105)
		(processable c2 p106)
		(processable c2 p107)
		(processable c2 p124)
		(processable c2 p125)
		(processable c2 p126)
		(processable c2 p127)
		(processable c2 p128)
		(processable c2 p129)
		(processable c2 p130)
		(processable c2 p131)
		(processable c2 p148)
		(processable c2 p149)
		(processable c2 p150)
		(processable c2 p151)
		(processable c2 p152)
		(processable c2 p153)
		(processable c2 p154)
		(processable c2 p155)
		(processable c3 p4)
		(processable c3 p5)
		(processable c3 p6)
		(processable c3 p7)
		(processable c3 p16)
		(processable c3 p17)
		(processable c3 p18)
		(processable c3 p19)
		(processable c3 p24)
		(processable c3 p25)
		(processable c3 p26)
		(processable c3 p27)
		(processable c3 p28)
		(processable c3 p29)
		(processable c3 p30)
		(processable c3 p31)
		(processable c3 p32)
		(processable c3 p33)
		(processable c3 p34)
		(processable c3 p35)
		(processable c3 p36)
		(processable c3 p37)
		(processable c3 p38)
		(processable c3 p39)
		(processable c3 p40)
		(processable c3 p41)
		(processable c3 p42)
		(processable c3 p43)
		(processable c3 p48)
		(processable c3 p49)
		(processable c3 p50)
		(processable c3 p51)
		(processable c3 p52)
		(processable c3 p53)
		(processable c3 p54)
		(processable c3 p55)
		(processable c3 p56)
		(processable c3 p57)
		(processable c3 p58)
		(processable c3 p59)
		(processable c3 p64)
		(processable c3 p65)
		(processable c3 p66)
		(processable c3 p67)
		(processable c3 p72)
		(processable c3 p73)
		(processable c3 p74)
		(processable c3 p75)
		(processable c3 p76)
		(processable c3 p77)
		(processable c3 p78)
		(processable c3 p79)
		(processable c3 p92)
		(processable c3 p93)
		(processable c3 p94)
		(processable c3 p95)
		(processable c3 p96)
		(processable c3 p97)
		(processable c3 p98)
		(processable c3 p99)
		(processable c3 p100)
		(processable c3 p101)
		(processable c3 p102)
		(processable c3 p103)
		(processable c3 p124)
		(processable c3 p125)
		(processable c3 p126)
		(processable c3 p127)
		(processable c3 p132)
		(processable c3 p133)
		(processable c3 p134)
		(processable c3 p135)
		(processable c3 p140)
		(processable c3 p141)
		(processable c3 p142)
		(processable c3 p143)
		(processable c3 p144)
		(processable c3 p145)
		(processable c3 p146)
		(processable c3 p147)
		(processable c3 p152)
		(processable c3 p153)
		(processable c3 p154)
		(processable c3 p155)

		(= (processing-cost c0 p0) 7)
		(= (processing-time c0 p0) 2)
		(= (processing-cost c0 p1) 4)
		(= (processing-time c0 p1) 1)
		(= (processing-cost c0 p2) 3)
		(= (processing-time c0 p2) 1)
		(= (processing-cost c0 p3) 16)
		(= (processing-time c0 p3) 2)
		(= (processing-cost c0 p4) 9)
		(= (processing-time c0 p4) 2)
		(= (processing-cost c0 p5) 7)
		(= (processing-time c0 p5) 1)
		(= (processing-cost c0 p6) 8)
		(= (processing-time c0 p6) 1)
		(= (processing-cost c0 p7) 16)
		(= (processing-time c0 p7) 2)
		(= (processing-cost c0 p12) 9)
		(= (processing-time c0 p12) 1)
		(= (processing-cost c0 p13) 2)
		(= (processing-time c0 p13) 1)
		(= (processing-cost c0 p14) 10)
		(= (processing-time c0 p14) 1)
		(= (processing-cost c0 p15) 8)
		(= (processing-time c0 p15) 2)
		(= (processing-cost c0 p44) 22)
		(= (processing-time c0 p44) 3)
		(= (processing-cost c0 p45) 7)
		(= (processing-time c0 p45) 1)
		(= (processing-cost c0 p46) 4)
		(= (processing-time c0 p46) 1)
		(= (processing-cost c0 p47) 7)
		(= (processing-time c0 p47) 2)
		(= (processing-cost c0 p88) 22)
		(= (processing-time c0 p88) 4)
		(= (processing-cost c0 p89) 5)
		(= (processing-time c0 p89) 3)
		(= (processing-cost c0 p90) 7)
		(= (processing-time c0 p90) 1)
		(= (processing-cost c0 p91) 10)
		(= (processing-time c0 p91) 2)
		(= (processing-cost c0 p96) 11)
		(= (processing-time c0 p96) 1)
		(= (processing-cost c0 p97) 10)
		(= (processing-time c0 p97) 1)
		(= (processing-cost c0 p98) 15)
		(= (processing-time c0 p98) 5)
		(= (processing-cost c0 p99) 7)
		(= (processing-time c0 p99) 2)
		(= (processing-cost c0 p116) 12)
		(= (processing-time c0 p116) 2)
		(= (processing-cost c0 p117) 14)
		(= (processing-time c0 p117) 3)
		(= (processing-cost c0 p118) 5)
		(= (processing-time c0 p118) 1)
		(= (processing-cost c0 p119) 10)
		(= (processing-time c0 p119) 2)
		(= (processing-cost c0 p120) 7)
		(= (processing-time c0 p120) 1)
		(= (processing-cost c0 p121) 3)
		(= (processing-time c0 p121) 1)
		(= (processing-cost c0 p122) 22)
		(= (processing-time c0 p122) 5)
		(= (processing-cost c0 p123) 11)
		(= (processing-time c0 p123) 4)
		(= (processing-cost c0 p132) 12)
		(= (processing-time c0 p132) 1)
		(= (processing-cost c0 p133) 14)
		(= (processing-time c0 p133) 1)
		(= (processing-cost c0 p134) 8)
		(= (processing-time c0 p134) 1)
		(= (processing-cost c0 p135) 7)
		(= (processing-time c0 p135) 2)
		(= (processing-cost c0 p136) 4)
		(= (processing-time c0 p136) 1)
		(= (processing-cost c0 p137) 16)
		(= (processing-time c0 p137) 2)
		(= (processing-cost c0 p138) 12)
		(= (processing-time c0 p138) 1)
		(= (processing-cost c0 p139) 9)
		(= (processing-time c0 p139) 2)
		(= (processing-cost c0 p156) 8)
		(= (processing-time c0 p156) 1)
		(= (processing-cost c0 p157) 13)
		(= (processing-time c0 p157) 2)
		(= (processing-cost c0 p158) 11)
		(= (processing-time c0 p158) 1)
		(= (processing-cost c0 p159) 9)
		(= (processing-time c0 p159) 2)
		(= (processing-cost c1 p0) 6)
		(= (processing-time c1 p0) 2)
		(= (processing-cost c1 p1) 12)
		(= (processing-time c1 p1) 1)
		(= (processing-cost c1 p2) 6)
		(= (processing-time c1 p2) 2)
		(= (processing-cost c1 p3) 10)
		(= (processing-time c1 p3) 2)
		(= (processing-cost c1 p8) 7)
		(= (processing-time c1 p8) 4)
		(= (processing-cost c1 p9) 6)
		(= (processing-time c1 p9) 1)
		(= (processing-cost c1 p10) 5)
		(= (processing-time c1 p10) 1)
		(= (processing-cost c1 p11) 9)
		(= (processing-time c1 p11) 2)
		(= (processing-cost c1 p20) 2)
		(= (processing-time c1 p20) 1)
		(= (processing-cost c1 p21) 9)
		(= (processing-time c1 p21) 1)
		(= (processing-cost c1 p22) 11)
		(= (processing-time c1 p22) 1)
		(= (processing-cost c1 p23) 6)
		(= (processing-time c1 p23) 2)
		(= (processing-cost c1 p32) 16)
		(= (processing-time c1 p32) 2)
		(= (processing-cost c1 p33) 2)
		(= (processing-time c1 p33) 2)
		(= (processing-cost c1 p34) 8)
		(= (processing-time c1 p34) 1)
		(= (processing-cost c1 p35) 7)
		(= (processing-time c1 p35) 2)
		(= (processing-cost c1 p44) 12)
		(= (processing-time c1 p44) 2)
		(= (processing-cost c1 p45) 12)
		(= (processing-time c1 p45) 2)
		(= (processing-cost c1 p46) 6)
		(= (processing-time c1 p46) 1)
		(= (processing-cost c1 p47) 15)
		(= (processing-time c1 p47) 2)
		(= (processing-cost c1 p48) 6)
		(= (processing-time c1 p48) 1)
		(= (processing-cost c1 p49) 11)
		(= (processing-time c1 p49) 1)
		(= (processing-cost c1 p50) 9)
		(= (processing-time c1 p50) 2)
		(= (processing-cost c1 p51) 4)
		(= (processing-time c1 p51) 4)
		(= (processing-cost c1 p52) 6)
		(= (processing-time c1 p52) 1)
		(= (processing-cost c1 p53) 13)
		(= (processing-time c1 p53) 6)
		(= (processing-cost c1 p54) 6)
		(= (processing-time c1 p54) 1)
		(= (processing-cost c1 p55) 9)
		(= (processing-time c1 p55) 2)
		(= (processing-cost c1 p68) 7)
		(= (processing-time c1 p68) 1)
		(= (processing-cost c1 p69) 7)
		(= (processing-time c1 p69) 1)
		(= (processing-cost c1 p70) 7)
		(= (processing-time c1 p70) 1)
		(= (processing-cost c1 p71) 9)
		(= (processing-time c1 p71) 2)
		(= (processing-cost c1 p80) 2)
		(= (processing-time c1 p80) 2)
		(= (processing-cost c1 p81) 9)
		(= (processing-time c1 p81) 1)
		(= (processing-cost c1 p82) 9)
		(= (processing-time c1 p82) 1)
		(= (processing-cost c1 p83) 8)
		(= (processing-time c1 p83) 2)
		(= (processing-cost c1 p84) 9)
		(= (processing-time c1 p84) 1)
		(= (processing-cost c1 p85) 5)
		(= (processing-time c1 p85) 1)
		(= (processing-cost c1 p86) 4)
		(= (processing-time c1 p86) 1)
		(= (processing-cost c1 p87) 11)
		(= (processing-time c1 p87) 2)
		(= (processing-cost c1 p108) 8)
		(= (processing-time c1 p108) 2)
		(= (processing-cost c1 p109) 22)
		(= (processing-time c1 p109) 5)
		(= (processing-cost c1 p110) 6)
		(= (processing-time c1 p110) 1)
		(= (processing-cost c1 p111) 17)
		(= (processing-time c1 p111) 2)
		(= (processing-cost c1 p112) 23)
		(= (processing-time c1 p112) 5)
		(= (processing-cost c1 p113) 7)
		(= (processing-time c1 p113) 1)
		(= (processing-cost c1 p114) 4)
		(= (processing-time c1 p114) 1)
		(= (processing-cost c1 p115) 4)
		(= (processing-time c1 p115) 2)
		(= (processing-cost c1 p116) 3)
		(= (processing-time c1 p116) 1)
		(= (processing-cost c1 p117) 6)
		(= (processing-time c1 p117) 1)
		(= (processing-cost c1 p118) 12)
		(= (processing-time c1 p118) 1)
		(= (processing-cost c1 p119) 8)
		(= (processing-time c1 p119) 2)
		(= (processing-cost c1 p120) 43)
		(= (processing-time c1 p120) 10)
		(= (processing-cost c1 p121) 9)
		(= (processing-time c1 p121) 1)
		(= (processing-cost c1 p122) 5)
		(= (processing-time c1 p122) 1)
		(= (processing-cost c1 p123) 11)
		(= (processing-time c1 p123) 2)
		(= (processing-cost c1 p128) 6)
		(= (processing-time c1 p128) 6)
		(= (processing-cost c1 p129) 7)
		(= (processing-time c1 p129) 2)
		(= (processing-cost c1 p130) 9)
		(= (processing-time c1 p130) 1)
		(= (processing-cost c1 p131) 2)
		(= (processing-time c1 p131) 2)
		(= (processing-cost c1 p136) 5)
		(= (processing-time c1 p136) 1)
		(= (processing-cost c1 p137) 15)
		(= (processing-time c1 p137) 5)
		(= (processing-cost c1 p138) 8)
		(= (processing-time c1 p138) 1)
		(= (processing-cost c1 p139) 10)
		(= (processing-time c1 p139) 2)
		(= (processing-cost c1 p140) 12)
		(= (processing-time c1 p140) 3)
		(= (processing-cost c1 p141) 5)
		(= (processing-time c1 p141) 1)
		(= (processing-cost c1 p142) 8)
		(= (processing-time c1 p142) 1)
		(= (processing-cost c1 p143) 6)
		(= (processing-time c1 p143) 2)
		(= (processing-cost c1 p156) 12)
		(= (processing-time c1 p156) 4)
		(= (processing-cost c1 p157) 9)
		(= (processing-time c1 p157) 1)
		(= (processing-cost c1 p158) 7)
		(= (processing-time c1 p158) 1)
		(= (processing-cost c1 p159) 8)
		(= (processing-time c1 p159) 2)
		(= (processing-cost c2 p16) 18)
		(= (processing-time c2 p16) 2)
		(= (processing-cost c2 p17) 2)
		(= (processing-time c2 p17) 1)
		(= (processing-cost c2 p18) 6)
		(= (processing-time c2 p18) 1)
		(= (processing-cost c2 p19) 9)
		(= (processing-time c2 p19) 2)
		(= (processing-cost c2 p28) 36)
		(= (processing-time c2 p28) 9)
		(= (processing-cost c2 p29) 11)
		(= (processing-time c2 p29) 1)
		(= (processing-cost c2 p30) 5)
		(= (processing-time c2 p30) 1)
		(= (processing-cost c2 p31) 12)
		(= (processing-time c2 p31) 2)
		(= (processing-cost c2 p36) 17)
		(= (processing-time c2 p36) 5)
		(= (processing-cost c2 p37) 12)
		(= (processing-time c2 p37) 2)
		(= (processing-cost c2 p38) 10)
		(= (processing-time c2 p38) 1)
		(= (processing-cost c2 p39) 9)
		(= (processing-time c2 p39) 2)
		(= (processing-cost c2 p60) 6)
		(= (processing-time c2 p60) 1)
		(= (processing-cost c2 p61) 26)
		(= (processing-time c2 p61) 4)
		(= (processing-cost c2 p62) 6)
		(= (processing-time c2 p62) 1)
		(= (processing-cost c2 p63) 11)
		(= (processing-time c2 p63) 2)
		(= (processing-cost c2 p80) 5)
		(= (processing-time c2 p80) 2)
		(= (processing-cost c2 p81) 9)
		(= (processing-time c2 p81) 2)
		(= (processing-cost c2 p82) 13)
		(= (processing-time c2 p82) 1)
		(= (processing-cost c2 p83) 11)
		(= (processing-time c2 p83) 3)
		(= (processing-cost c2 p88) 6)
		(= (processing-time c2 p88) 2)
		(= (processing-cost c2 p89) 16)
		(= (processing-time c2 p89) 3)
		(= (processing-cost c2 p90) 7)
		(= (processing-time c2 p90) 1)
		(= (processing-cost c2 p91) 11)
		(= (processing-time c2 p91) 3)
		(= (processing-cost c2 p104) 1)
		(= (processing-time c2 p104) 1)
		(= (processing-cost c2 p105) 6)
		(= (processing-time c2 p105) 1)
		(= (processing-cost c2 p106) 2)
		(= (processing-time c2 p106) 1)
		(= (processing-cost c2 p107) 7)
		(= (processing-time c2 p107) 2)
		(= (processing-cost c2 p124) 4)
		(= (processing-time c2 p124) 1)
		(= (processing-cost c2 p125) 8)
		(= (processing-time c2 p125) 1)
		(= (processing-cost c2 p126) 10)
		(= (processing-time c2 p126) 2)
		(= (processing-cost c2 p127) 6)
		(= (processing-time c2 p127) 2)
		(= (processing-cost c2 p128) 2)
		(= (processing-time c2 p128) 1)
		(= (processing-cost c2 p129) 10)
		(= (processing-time c2 p129) 1)
		(= (processing-cost c2 p130) 10)
		(= (processing-time c2 p130) 2)
		(= (processing-cost c2 p131) 13)
		(= (processing-time c2 p131) 2)
		(= (processing-cost c2 p148) 11)
		(= (processing-time c2 p148) 3)
		(= (processing-cost c2 p149) 5)
		(= (processing-time c2 p149) 1)
		(= (processing-cost c2 p150) 4)
		(= (processing-time c2 p150) 1)
		(= (processing-cost c2 p151) 17)
		(= (processing-time c2 p151) 2)
		(= (processing-cost c2 p152) 11)
		(= (processing-time c2 p152) 3)
		(= (processing-cost c2 p153) 1)
		(= (processing-time c2 p153) 1)
		(= (processing-cost c2 p154) 6)
		(= (processing-time c2 p154) 1)
		(= (processing-cost c2 p155) 16)
		(= (processing-time c2 p155) 2)
		(= (processing-cost c3 p4) 22)
		(= (processing-time c3 p4) 4)
		(= (processing-cost c3 p5) 3)
		(= (processing-time c3 p5) 1)
		(= (processing-cost c3 p6) 3)
		(= (processing-time c3 p6) 1)
		(= (processing-cost c3 p7) 2)
		(= (processing-time c3 p7) 2)
		(= (processing-cost c3 p16) 23)
		(= (processing-time c3 p16) 8)
		(= (processing-cost c3 p17) 11)
		(= (processing-time c3 p17) 1)
		(= (processing-cost c3 p18) 11)
		(= (processing-time c3 p18) 1)
		(= (processing-cost c3 p19) 5)
		(= (processing-time c3 p19) 2)
		(= (processing-cost c3 p24) 14)
		(= (processing-time c3 p24) 2)
		(= (processing-cost c3 p25) 15)
		(= (processing-time c3 p25) 2)
		(= (processing-cost c3 p26) 9)
		(= (processing-time c3 p26) 2)
		(= (processing-cost c3 p27) 11)
		(= (processing-time c3 p27) 2)
		(= (processing-cost c3 p28) 14)
		(= (processing-time c3 p28) 3)
		(= (processing-cost c3 p29) 13)
		(= (processing-time c3 p29) 2)
		(= (processing-cost c3 p30) 12)
		(= (processing-time c3 p30) 2)
		(= (processing-cost c3 p31) 3)
		(= (processing-time c3 p31) 3)
		(= (processing-cost c3 p32) 11)
		(= (processing-time c3 p32) 2)
		(= (processing-cost c3 p33) 19)
		(= (processing-time c3 p33) 3)
		(= (processing-cost c3 p34) 5)
		(= (processing-time c3 p34) 1)
		(= (processing-cost c3 p35) 18)
		(= (processing-time c3 p35) 2)
		(= (processing-cost c3 p36) 6)
		(= (processing-time c3 p36) 3)
		(= (processing-cost c3 p37) 4)
		(= (processing-time c3 p37) 1)
		(= (processing-cost c3 p38) 1)
		(= (processing-time c3 p38) 1)
		(= (processing-cost c3 p39) 7)
		(= (processing-time c3 p39) 2)
		(= (processing-cost c3 p40) 5)
		(= (processing-time c3 p40) 1)
		(= (processing-cost c3 p41) 7)
		(= (processing-time c3 p41) 1)
		(= (processing-cost c3 p42) 4)
		(= (processing-time c3 p42) 1)
		(= (processing-cost c3 p43) 16)
		(= (processing-time c3 p43) 2)
		(= (processing-cost c3 p48) 14)
		(= (processing-time c3 p48) 1)
		(= (processing-cost c3 p49) 2)
		(= (processing-time c3 p49) 2)
		(= (processing-cost c3 p50) 7)
		(= (processing-time c3 p50) 1)
		(= (processing-cost c3 p51) 14)
		(= (processing-time c3 p51) 2)
		(= (processing-cost c3 p52) 6)
		(= (processing-time c3 p52) 2)
		(= (processing-cost c3 p53) 4)
		(= (processing-time c3 p53) 1)
		(= (processing-cost c3 p54) 13)
		(= (processing-time c3 p54) 1)
		(= (processing-cost c3 p55) 8)
		(= (processing-time c3 p55) 2)
		(= (processing-cost c3 p56) 12)
		(= (processing-time c3 p56) 4)
		(= (processing-cost c3 p57) 14)
		(= (processing-time c3 p57) 3)
		(= (processing-cost c3 p58) 15)
		(= (processing-time c3 p58) 3)
		(= (processing-cost c3 p59) 9)
		(= (processing-time c3 p59) 2)
		(= (processing-cost c3 p64) 9)
		(= (processing-time c3 p64) 2)
		(= (processing-cost c3 p65) 11)
		(= (processing-time c3 p65) 1)
		(= (processing-cost c3 p66) 13)
		(= (processing-time c3 p66) 1)
		(= (processing-cost c3 p67) 8)
		(= (processing-time c3 p67) 2)
		(= (processing-cost c3 p72) 2)
		(= (processing-time c3 p72) 1)
		(= (processing-cost c3 p73) 1)
		(= (processing-time c3 p73) 1)
		(= (processing-cost c3 p74) 4)
		(= (processing-time c3 p74) 1)
		(= (processing-cost c3 p75) 8)
		(= (processing-time c3 p75) 2)
		(= (processing-cost c3 p76) 3)
		(= (processing-time c3 p76) 1)
		(= (processing-cost c3 p77) 6)
		(= (processing-time c3 p77) 1)
		(= (processing-cost c3 p78) 4)
		(= (processing-time c3 p78) 1)
		(= (processing-cost c3 p79) 9)
		(= (processing-time c3 p79) 2)
		(= (processing-cost c3 p92) 11)
		(= (processing-time c3 p92) 5)
		(= (processing-cost c3 p93) 15)
		(= (processing-time c3 p93) 2)
		(= (processing-cost c3 p94) 9)
		(= (processing-time c3 p94) 1)
		(= (processing-cost c3 p95) 15)
		(= (processing-time c3 p95) 3)
		(= (processing-cost c3 p96) 3)
		(= (processing-time c3 p96) 1)
		(= (processing-cost c3 p97) 3)
		(= (processing-time c3 p97) 1)
		(= (processing-cost c3 p98) 12)
		(= (processing-time c3 p98) 1)
		(= (processing-cost c3 p99) 4)
		(= (processing-time c3 p99) 2)
		(= (processing-cost c3 p100) 4)
		(= (processing-time c3 p100) 1)
		(= (processing-cost c3 p101) 4)
		(= (processing-time c3 p101) 1)
		(= (processing-cost c3 p102) 10)
		(= (processing-time c3 p102) 1)
		(= (processing-cost c3 p103) 14)
		(= (processing-time c3 p103) 2)
		(= (processing-cost c3 p124) 9)
		(= (processing-time c3 p124) 5)
		(= (processing-cost c3 p125) 8)
		(= (processing-time c3 p125) 2)
		(= (processing-cost c3 p126) 5)
		(= (processing-time c3 p126) 1)
		(= (processing-cost c3 p127) 8)
		(= (processing-time c3 p127) 2)
		(= (processing-cost c3 p132) 14)
		(= (processing-time c3 p132) 1)
		(= (processing-cost c3 p133) 19)
		(= (processing-time c3 p133) 4)
		(= (processing-cost c3 p134) 16)
		(= (processing-time c3 p134) 2)
		(= (processing-cost c3 p135) 12)
		(= (processing-time c3 p135) 2)
		(= (processing-cost c3 p140) 15)
		(= (processing-time c3 p140) 5)
		(= (processing-cost c3 p141) 4)
		(= (processing-time c3 p141) 1)
		(= (processing-cost c3 p142) 12)
		(= (processing-time c3 p142) 1)
		(= (processing-cost c3 p143) 10)
		(= (processing-time c3 p143) 2)
		(= (processing-cost c3 p144) 4)
		(= (processing-time c3 p144) 2)
		(= (processing-cost c3 p145) 5)
		(= (processing-time c3 p145) 2)
		(= (processing-cost c3 p146) 14)
		(= (processing-time c3 p146) 2)
		(= (processing-cost c3 p147) 11)
		(= (processing-time c3 p147) 2)
		(= (processing-cost c3 p152) 10)
		(= (processing-time c3 p152) 1)
		(= (processing-cost c3 p153) 2)
		(= (processing-time c3 p153) 1)
		(= (processing-cost c3 p154) 6)
		(= (processing-time c3 p154) 1)
		(= (processing-cost c3 p155) 2)
		(= (processing-time c3 p155) 2)

		(depends-one p0 p2)

		(not-depends p1)

		(not-depends p2)

		(not-depends p3)

		(not-depends p4)

		(not-depends p5)

		(depends-one p6 p7)

		(not-depends p7)

		(not-depends p8)

		(depends-one p9 p8)

		(not-depends p10)

		(depends-one p11 p10)

		(not-depends p12)

		(depends-one p13 p14)

		(not-depends p14)

		(not-depends p15)

		(not-depends p16)

		(not-depends p17)

		(depends-one p18 p16)

		(not-depends p19)

		(not-depends p20)

		(not-depends p21)

		(depends-one p22 p20)

		(depends-one p23 p20)

		(depends-one p24 p27)

		(not-depends p25)

		(not-depends p26)

		(not-depends p27)

		(not-depends p28)

		(not-depends p29)

		(not-depends p30)

		(depends-one p31 p28)

		(not-depends p32)

		(depends-one p33 p32)

		(not-depends p34)

		(not-depends p35)

		(not-depends p36)

		(not-depends p37)

		(not-depends p38)

		(not-depends p39)

		(not-depends p40)

		(not-depends p41)

		(depends-one p42 p40)

		(depends-one p43 p42)

		(depends-one p44 p47)

		(not-depends p45)

		(depends-one p46 p45)

		(not-depends p47)

		(not-depends p48)

		(not-depends p49)

		(not-depends p50)

		(not-depends p51)

		(not-depends p52)

		(not-depends p53)

		(not-depends p54)

		(not-depends p55)

		(not-depends p56)

		(not-depends p57)

		(depends-one p58 p59)

		(not-depends p59)

		(depends-one p60 p63)

		(not-depends p61)

		(not-depends p62)

		(not-depends p63)

		(not-depends p64)

		(not-depends p65)

		(not-depends p66)

		(depends-one p67 p64)

		(depends-one p68 p69)

		(not-depends p69)

		(not-depends p70)

		(not-depends p71)

		(depends-one p72 p74)

		(not-depends p73)

		(not-depends p74)

		(not-depends p75)

		(not-depends p76)

		(not-depends p77)

		(not-depends p78)

		(not-depends p79)

		(not-depends p80)

		(not-depends p81)

		(depends-one p82 p81)

		(depends-one p83 p82)

		(not-depends p84)

		(not-depends p85)

		(depends-one p86 p85)

		(not-depends p87)

		(depends-one p88 p91)

		(not-depends p89)

		(depends-one p90 p89)

		(not-depends p91)

		(not-depends p92)

		(depends-one p93 p95)

		(not-depends p94)

		(not-depends p95)

		(not-depends p96)

		(depends-one p97 p99)

		(not-depends p98)

		(not-depends p99)

		(depends-one p100 p103)

		(not-depends p101)

		(not-depends p102)

		(not-depends p103)

		(not-depends p104)

		(not-depends p105)

		(not-depends p106)

		(not-depends p107)

		(not-depends p108)

		(depends-one p109 p108)

		(depends-one p110 p108)

		(not-depends p111)

		(not-depends p112)

		(depends-one p113 p114)

		(not-depends p114)

		(not-depends p115)

		(not-depends p116)

		(not-depends p117)

		(not-depends p118)

		(depends-one p119 p116)

		(not-depends p120)

		(depends-one p121 p123)

		(not-depends p122)

		(not-depends p123)

		(not-depends p124)

		(not-depends p125)

		(not-depends p126)

		(depends-one p127 p126)

		(not-depends p128)

		(not-depends p129)

		(not-depends p130)

		(not-depends p131)

		(depends-one p132 p133)

		(not-depends p133)

		(not-depends p134)

		(depends-one p135 p132)

		(not-depends p136)

		(not-depends p137)

		(not-depends p138)

		(not-depends p139)

		(not-depends p140)

		(not-depends p141)

		(depends-one p142 p143)

		(not-depends p143)

		(not-depends p144)

		(depends-one p145 p144)

		(not-depends p146)

		(not-depends p147)

		(not-depends p148)

		(not-depends p149)

		(depends-one p150 p151)

		(not-depends p151)

		(depends-one p152 p155)

		(not-depends p153)

		(depends-one p154 p155)

		(not-depends p155)

		(depends-one p156 p158)

		(not-depends p157)

		(not-depends p158)

		(not-depends p159)

		(not-completed p0)

		(not-completed p1)

		(not-completed p2)

		(not-completed p3)

		(not-completed p4)

		(not-completed p5)

		(not-completed p6)

		(not-completed p7)

		(not-completed p8)

		(not-completed p9)

		(not-completed p10)

		(not-completed p11)

		(not-completed p12)

		(not-completed p13)

		(not-completed p14)

		(not-completed p15)

		(not-completed p16)

		(not-completed p17)

		(not-completed p18)

		(not-completed p19)

		(not-completed p20)

		(not-completed p21)

		(not-completed p22)

		(not-completed p23)

		(not-completed p24)

		(not-completed p25)

		(not-completed p26)

		(not-completed p27)

		(not-completed p28)

		(not-completed p29)

		(not-completed p30)

		(not-completed p31)

		(not-completed p32)

		(not-completed p33)

		(not-completed p34)

		(not-completed p35)

		(not-completed p36)

		(not-completed p37)

		(not-completed p38)

		(not-completed p39)

		(not-completed p40)

		(not-completed p41)

		(not-completed p42)

		(not-completed p43)

		(not-completed p44)

		(not-completed p45)

		(not-completed p46)

		(not-completed p47)

		(not-completed p48)

		(not-completed p49)

		(not-completed p50)

		(not-completed p51)

		(not-completed p52)

		(not-completed p53)

		(not-completed p54)

		(not-completed p55)

		(not-completed p56)

		(not-completed p57)

		(not-completed p58)

		(not-completed p59)

		(not-completed p60)

		(not-completed p61)

		(not-completed p62)

		(not-completed p63)

		(not-completed p64)

		(not-completed p65)

		(not-completed p66)

		(not-completed p67)

		(not-completed p68)

		(not-completed p69)

		(not-completed p70)

		(not-completed p71)

		(not-completed p72)

		(not-completed p73)

		(not-completed p74)

		(not-completed p75)

		(not-completed p76)

		(not-completed p77)

		(not-completed p78)

		(not-completed p79)

		(not-completed p80)

		(not-completed p81)

		(not-completed p82)

		(not-completed p83)

		(not-completed p84)

		(not-completed p85)

		(not-completed p86)

		(not-completed p87)

		(not-completed p88)

		(not-completed p89)

		(not-completed p90)

		(not-completed p91)

		(not-completed p92)

		(not-completed p93)

		(not-completed p94)

		(not-completed p95)

		(not-completed p96)

		(not-completed p97)

		(not-completed p98)

		(not-completed p99)

		(not-completed p100)

		(not-completed p101)

		(not-completed p102)

		(not-completed p103)

		(not-completed p104)

		(not-completed p105)

		(not-completed p106)

		(not-completed p107)

		(not-completed p108)

		(not-completed p109)

		(not-completed p110)

		(not-completed p111)

		(not-completed p112)

		(not-completed p113)

		(not-completed p114)

		(not-completed p115)

		(not-completed p116)

		(not-completed p117)

		(not-completed p118)

		(not-completed p119)

		(not-completed p120)

		(not-completed p121)

		(not-completed p122)

		(not-completed p123)

		(not-completed p124)

		(not-completed p125)

		(not-completed p126)

		(not-completed p127)

		(not-completed p128)

		(not-completed p129)

		(not-completed p130)

		(not-completed p131)

		(not-completed p132)

		(not-completed p133)

		(not-completed p134)

		(not-completed p135)

		(not-completed p136)

		(not-completed p137)

		(not-completed p138)

		(not-completed p139)

		(not-completed p140)

		(not-completed p141)

		(not-completed p142)

		(not-completed p143)

		(not-completed p144)

		(not-completed p145)

		(not-completed p146)

		(not-completed p147)

		(not-completed p148)

		(not-completed p149)

		(not-completed p150)

		(not-completed p151)

		(not-completed p152)

		(not-completed p153)

		(not-completed p154)

		(not-completed p155)

		(not-completed p156)

		(not-completed p157)

		(not-completed p158)

		(not-completed p159)


	)

	(:goal
		(and
			(completed p0)
			(completed p1)
			(completed p2)
			(completed p3)
			(completed p4)
			(completed p5)
			(completed p6)
			(completed p7)
			(completed p8)
			(completed p9)
			(completed p10)
			(completed p11)
			(completed p12)
			(completed p13)
			(completed p14)
			(completed p15)
			(completed p16)
			(completed p17)
			(completed p18)
			(completed p19)
			(completed p20)
			(completed p21)
			(completed p22)
			(completed p23)
			(completed p24)
			(completed p25)
			(completed p26)
			(completed p27)
			(completed p28)
			(completed p29)
			(completed p30)
			(completed p31)
			(completed p32)
			(completed p33)
			(completed p34)
			(completed p35)
			(completed p36)
			(completed p37)
			(completed p38)
			(completed p39)
			(completed p40)
			(completed p41)
			(completed p42)
			(completed p43)
			(completed p44)
			(completed p45)
			(completed p46)
			(completed p47)
			(completed p48)
			(completed p49)
			(completed p50)
			(completed p51)
			(completed p52)
			(completed p53)
			(completed p54)
			(completed p55)
			(completed p56)
			(completed p57)
			(completed p58)
			(completed p59)
			(completed p60)
			(completed p61)
			(completed p62)
			(completed p63)
			(completed p64)
			(completed p65)
			(completed p66)
			(completed p67)
			(completed p68)
			(completed p69)
			(completed p70)
			(completed p71)
			(completed p72)
			(completed p73)
			(completed p74)
			(completed p75)
			(completed p76)
			(completed p77)
			(completed p78)
			(completed p79)
			(completed p80)
			(completed p81)
			(completed p82)
			(completed p83)
			(completed p84)
			(completed p85)
			(completed p86)
			(completed p87)
			(completed p88)
			(completed p89)
			(completed p90)
			(completed p91)
			(completed p92)
			(completed p93)
			(completed p94)
			(completed p95)
			(completed p96)
			(completed p97)
			(completed p98)
			(completed p99)
			(completed p100)
			(completed p101)
			(completed p102)
			(completed p103)
			(completed p104)
			(completed p105)
			(completed p106)
			(completed p107)
			(completed p108)
			(completed p109)
			(completed p110)
			(completed p111)
			(completed p112)
			(completed p113)
			(completed p114)
			(completed p115)
			(completed p116)
			(completed p117)
			(completed p118)
			(completed p119)
			(completed p120)
			(completed p121)
			(completed p122)
			(completed p123)
			(completed p124)
			(completed p125)
			(completed p126)
			(completed p127)
			(completed p128)
			(completed p129)
			(completed p130)
			(completed p131)
			(completed p132)
			(completed p133)
			(completed p134)
			(completed p135)
			(completed p136)
			(completed p137)
			(completed p138)
			(completed p139)
			(completed p140)
			(completed p141)
			(completed p142)
			(completed p143)
			(completed p144)
			(completed p145)
			(completed p146)
			(completed p147)
			(completed p148)
			(completed p149)
			(completed p150)
			(completed p151)
			(completed p152)
			(completed p153)
			(completed p154)
			(completed p155)
			(completed p156)
			(completed p157)
			(completed p158)
			(completed p159)
		)
	)

	(:metric minimize (total-time))		; optimal cost = 749

)