(define

	(problem ps_domain_rmt-instance)
	(:domain ps_domain_rmt)

	(:objects
		m0 m1 - machine
		c0 c1 c2 c3 - configuration
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 p30 p31 p32 p33 p34 p35 p36 p37 p38 p39 p40 p41 p42 p43 p44 p45 p46 p47 p48 p49 p50 p51 p52 p53 p54 p55 p56 p57 p58 p59 p60 p61 p62 p63 p64 p65 p66 p67 p68 p69 p70 p71 p72 p73 p74 p75 p76 p77 p78 p79 p80 p81 p82 p83 p84 p85 p86 p87 p88 p89 p90 p91 p92 p93 p94 p95 p96 p97 p98 p99 p100 p101 p102 p103 p104 p105 p106 p107 p108 p109 p110 p111 p112 p113 p114 p115 p116 p117 p118 p119 p120 p121 p122 p123 p124 p125 p126 p127 p128 p129 p130 p131 p132 p133 p134 p135 p136 p137 p138 p139 p140 p141 p142 p143 p144 p145 p146 p147 p148 p149 p150 p151 p152 p153 p154 p155 p156 p157 p158 p159 p160 p161 p162 p163 p164 p165 p166 p167 p168 p169 p170 p171 p172 p173 p174 p175 p176 p177 p178 p179 - process
	)

	(:init

		(configured m0 c0)
		(configured m1 c1)

		(configurable m0 c0)
		(configurable m0 c2)
		(configurable m0 c3)
		(configurable m1 c0)
		(configurable m1 c1)
		(= (reconfiguration-cost c0 c1) 24)
		(= (reconfiguration-time c0 c1) 6)
		(= (reconfiguration-cost c1 c0) 13)
		(= (reconfiguration-time c1 c0) 6)
		(not-same c0 c1)
		(not-same c1 c0)
		(= (reconfiguration-cost c0 c2) 15)
		(= (reconfiguration-time c0 c2) 7)
		(= (reconfiguration-cost c2 c0) 12)
		(= (reconfiguration-time c2 c0) 7)
		(not-same c0 c2)
		(not-same c2 c0)
		(= (reconfiguration-cost c0 c3) 18)
		(= (reconfiguration-time c0 c3) 8)
		(= (reconfiguration-cost c3 c0) 20)
		(= (reconfiguration-time c3 c0) 8)
		(not-same c0 c3)
		(not-same c3 c0)
		(= (reconfiguration-cost c2 c3) 23)
		(= (reconfiguration-time c2 c3) 8)
		(= (reconfiguration-cost c3 c2) 20)
		(= (reconfiguration-time c3 c2) 8)
		(not-same c2 c3)
		(not-same c3 c2)


		(processable c0 p0)
		(processable c0 p1)
		(processable c0 p2)
		(processable c0 p3)
		(processable c0 p20)
		(processable c0 p21)
		(processable c0 p22)
		(processable c0 p23)
		(processable c0 p24)
		(processable c0 p25)
		(processable c0 p26)
		(processable c0 p27)
		(processable c0 p36)
		(processable c0 p37)
		(processable c0 p38)
		(processable c0 p39)
		(processable c0 p40)
		(processable c0 p41)
		(processable c0 p42)
		(processable c0 p43)
		(processable c0 p44)
		(processable c0 p45)
		(processable c0 p46)
		(processable c0 p47)
		(processable c0 p52)
		(processable c0 p53)
		(processable c0 p54)
		(processable c0 p55)
		(processable c0 p64)
		(processable c0 p65)
		(processable c0 p66)
		(processable c0 p67)
		(processable c0 p68)
		(processable c0 p69)
		(processable c0 p70)
		(processable c0 p71)
		(processable c0 p72)
		(processable c0 p73)
		(processable c0 p74)
		(processable c0 p75)
		(processable c0 p76)
		(processable c0 p77)
		(processable c0 p78)
		(processable c0 p79)
		(processable c0 p96)
		(processable c0 p97)
		(processable c0 p98)
		(processable c0 p99)
		(processable c0 p100)
		(processable c0 p101)
		(processable c0 p102)
		(processable c0 p103)
		(processable c0 p108)
		(processable c0 p109)
		(processable c0 p110)
		(processable c0 p111)
		(processable c0 p124)
		(processable c0 p125)
		(processable c0 p126)
		(processable c0 p127)
		(processable c0 p136)
		(processable c0 p137)
		(processable c0 p138)
		(processable c0 p139)
		(processable c0 p148)
		(processable c0 p149)
		(processable c0 p150)
		(processable c0 p151)
		(processable c0 p168)
		(processable c0 p169)
		(processable c0 p170)
		(processable c0 p171)
		(processable c0 p172)
		(processable c0 p173)
		(processable c0 p174)
		(processable c0 p175)
		(processable c1 p28)
		(processable c1 p29)
		(processable c1 p30)
		(processable c1 p31)
		(processable c1 p32)
		(processable c1 p33)
		(processable c1 p34)
		(processable c1 p35)
		(processable c1 p40)
		(processable c1 p41)
		(processable c1 p42)
		(processable c1 p43)
		(processable c1 p52)
		(processable c1 p53)
		(processable c1 p54)
		(processable c1 p55)
		(processable c1 p60)
		(processable c1 p61)
		(processable c1 p62)
		(processable c1 p63)
		(processable c1 p84)
		(processable c1 p85)
		(processable c1 p86)
		(processable c1 p87)
		(processable c1 p104)
		(processable c1 p105)
		(processable c1 p106)
		(processable c1 p107)
		(processable c1 p116)
		(processable c1 p117)
		(processable c1 p118)
		(processable c1 p119)
		(processable c1 p120)
		(processable c1 p121)
		(processable c1 p122)
		(processable c1 p123)
		(processable c1 p128)
		(processable c1 p129)
		(processable c1 p130)
		(processable c1 p131)
		(processable c1 p136)
		(processable c1 p137)
		(processable c1 p138)
		(processable c1 p139)
		(processable c1 p148)
		(processable c1 p149)
		(processable c1 p150)
		(processable c1 p151)
		(processable c1 p152)
		(processable c1 p153)
		(processable c1 p154)
		(processable c1 p155)
		(processable c1 p156)
		(processable c1 p157)
		(processable c1 p158)
		(processable c1 p159)
		(processable c1 p176)
		(processable c1 p177)
		(processable c1 p178)
		(processable c1 p179)
		(processable c2 p4)
		(processable c2 p5)
		(processable c2 p6)
		(processable c2 p7)
		(processable c2 p16)
		(processable c2 p17)
		(processable c2 p18)
		(processable c2 p19)
		(processable c2 p36)
		(processable c2 p37)
		(processable c2 p38)
		(processable c2 p39)
		(processable c2 p80)
		(processable c2 p81)
		(processable c2 p82)
		(processable c2 p83)
		(processable c2 p88)
		(processable c2 p89)
		(processable c2 p90)
		(processable c2 p91)
		(processable c2 p96)
		(processable c2 p97)
		(processable c2 p98)
		(processable c2 p99)
		(processable c2 p104)
		(processable c2 p105)
		(processable c2 p106)
		(processable c2 p107)
		(processable c2 p124)
		(processable c2 p125)
		(processable c2 p126)
		(processable c2 p127)
		(processable c2 p128)
		(processable c2 p129)
		(processable c2 p130)
		(processable c2 p131)
		(processable c2 p132)
		(processable c2 p133)
		(processable c2 p134)
		(processable c2 p135)
		(processable c2 p140)
		(processable c2 p141)
		(processable c2 p142)
		(processable c2 p143)
		(processable c2 p144)
		(processable c2 p145)
		(processable c2 p146)
		(processable c2 p147)
		(processable c2 p160)
		(processable c2 p161)
		(processable c2 p162)
		(processable c2 p163)
		(processable c2 p164)
		(processable c2 p165)
		(processable c2 p166)
		(processable c2 p167)
		(processable c2 p168)
		(processable c2 p169)
		(processable c2 p170)
		(processable c2 p171)
		(processable c2 p172)
		(processable c2 p173)
		(processable c2 p174)
		(processable c2 p175)
		(processable c3 p0)
		(processable c3 p1)
		(processable c3 p2)
		(processable c3 p3)
		(processable c3 p8)
		(processable c3 p9)
		(processable c3 p10)
		(processable c3 p11)
		(processable c3 p12)
		(processable c3 p13)
		(processable c3 p14)
		(processable c3 p15)
		(processable c3 p16)
		(processable c3 p17)
		(processable c3 p18)
		(processable c3 p19)
		(processable c3 p44)
		(processable c3 p45)
		(processable c3 p46)
		(processable c3 p47)
		(processable c3 p48)
		(processable c3 p49)
		(processable c3 p50)
		(processable c3 p51)
		(processable c3 p56)
		(processable c3 p57)
		(processable c3 p58)
		(processable c3 p59)
		(processable c3 p60)
		(processable c3 p61)
		(processable c3 p62)
		(processable c3 p63)
		(processable c3 p64)
		(processable c3 p65)
		(processable c3 p66)
		(processable c3 p67)
		(processable c3 p68)
		(processable c3 p69)
		(processable c3 p70)
		(processable c3 p71)
		(processable c3 p92)
		(processable c3 p93)
		(processable c3 p94)
		(processable c3 p95)
		(processable c3 p112)
		(processable c3 p113)
		(processable c3 p114)
		(processable c3 p115)
		(processable c3 p116)
		(processable c3 p117)
		(processable c3 p118)
		(processable c3 p119)
		(processable c3 p120)
		(processable c3 p121)
		(processable c3 p122)
		(processable c3 p123)
		(processable c3 p140)
		(processable c3 p141)
		(processable c3 p142)
		(processable c3 p143)
		(processable c3 p164)
		(processable c3 p165)
		(processable c3 p166)
		(processable c3 p167)

		(= (processing-cost c0 p0) 8)
		(= (processing-time c0 p0) 1)
		(= (processing-cost c0 p1) 13)
		(= (processing-time c0 p1) 5)
		(= (processing-cost c0 p2) 16)
		(= (processing-time c0 p2) 4)
		(= (processing-cost c0 p3) 17)
		(= (processing-time c0 p3) 4)
		(= (processing-cost c0 p20) 15)
		(= (processing-time c0 p20) 3)
		(= (processing-cost c0 p21) 5)
		(= (processing-time c0 p21) 1)
		(= (processing-cost c0 p22) 7)
		(= (processing-time c0 p22) 1)
		(= (processing-cost c0 p23) 9)
		(= (processing-time c0 p23) 2)
		(= (processing-cost c0 p24) 14)
		(= (processing-time c0 p24) 5)
		(= (processing-cost c0 p25) 2)
		(= (processing-time c0 p25) 1)
		(= (processing-cost c0 p26) 5)
		(= (processing-time c0 p26) 1)
		(= (processing-cost c0 p27) 18)
		(= (processing-time c0 p27) 2)
		(= (processing-cost c0 p36) 9)
		(= (processing-time c0 p36) 2)
		(= (processing-cost c0 p37) 16)
		(= (processing-time c0 p37) 2)
		(= (processing-cost c0 p38) 9)
		(= (processing-time c0 p38) 1)
		(= (processing-cost c0 p39) 13)
		(= (processing-time c0 p39) 2)
		(= (processing-cost c0 p40) 19)
		(= (processing-time c0 p40) 5)
		(= (processing-cost c0 p41) 6)
		(= (processing-time c0 p41) 3)
		(= (processing-cost c0 p42) 13)
		(= (processing-time c0 p42) 2)
		(= (processing-cost c0 p43) 11)
		(= (processing-time c0 p43) 3)
		(= (processing-cost c0 p44) 25)
		(= (processing-time c0 p44) 4)
		(= (processing-cost c0 p45) 1)
		(= (processing-time c0 p45) 1)
		(= (processing-cost c0 p46) 9)
		(= (processing-time c0 p46) 1)
		(= (processing-cost c0 p47) 17)
		(= (processing-time c0 p47) 2)
		(= (processing-cost c0 p52) 12)
		(= (processing-time c0 p52) 2)
		(= (processing-cost c0 p53) 11)
		(= (processing-time c0 p53) 1)
		(= (processing-cost c0 p54) 6)
		(= (processing-time c0 p54) 1)
		(= (processing-cost c0 p55) 8)
		(= (processing-time c0 p55) 2)
		(= (processing-cost c0 p64) 4)
		(= (processing-time c0 p64) 1)
		(= (processing-cost c0 p65) 8)
		(= (processing-time c0 p65) 2)
		(= (processing-cost c0 p66) 10)
		(= (processing-time c0 p66) 1)
		(= (processing-cost c0 p67) 9)
		(= (processing-time c0 p67) 2)
		(= (processing-cost c0 p68) 19)
		(= (processing-time c0 p68) 3)
		(= (processing-cost c0 p69) 8)
		(= (processing-time c0 p69) 1)
		(= (processing-cost c0 p70) 6)
		(= (processing-time c0 p70) 3)
		(= (processing-cost c0 p71) 9)
		(= (processing-time c0 p71) 2)
		(= (processing-cost c0 p72) 5)
		(= (processing-time c0 p72) 1)
		(= (processing-cost c0 p73) 4)
		(= (processing-time c0 p73) 1)
		(= (processing-cost c0 p74) 7)
		(= (processing-time c0 p74) 1)
		(= (processing-cost c0 p75) 10)
		(= (processing-time c0 p75) 2)
		(= (processing-cost c0 p76) 8)
		(= (processing-time c0 p76) 1)
		(= (processing-cost c0 p77) 3)
		(= (processing-time c0 p77) 1)
		(= (processing-cost c0 p78) 7)
		(= (processing-time c0 p78) 1)
		(= (processing-cost c0 p79) 8)
		(= (processing-time c0 p79) 2)
		(= (processing-cost c0 p96) 7)
		(= (processing-time c0 p96) 2)
		(= (processing-cost c0 p97) 9)
		(= (processing-time c0 p97) 3)
		(= (processing-cost c0 p98) 1)
		(= (processing-time c0 p98) 1)
		(= (processing-cost c0 p99) 10)
		(= (processing-time c0 p99) 2)
		(= (processing-cost c0 p100) 13)
		(= (processing-time c0 p100) 4)
		(= (processing-cost c0 p101) 7)
		(= (processing-time c0 p101) 1)
		(= (processing-cost c0 p102) 11)
		(= (processing-time c0 p102) 1)
		(= (processing-cost c0 p103) 9)
		(= (processing-time c0 p103) 2)
		(= (processing-cost c0 p108) 5)
		(= (processing-time c0 p108) 1)
		(= (processing-cost c0 p109) 11)
		(= (processing-time c0 p109) 1)
		(= (processing-cost c0 p110) 7)
		(= (processing-time c0 p110) 1)
		(= (processing-cost c0 p111) 8)
		(= (processing-time c0 p111) 2)
		(= (processing-cost c0 p124) 17)
		(= (processing-time c0 p124) 4)
		(= (processing-cost c0 p125) 8)
		(= (processing-time c0 p125) 1)
		(= (processing-cost c0 p126) 3)
		(= (processing-time c0 p126) 1)
		(= (processing-cost c0 p127) 15)
		(= (processing-time c0 p127) 2)
		(= (processing-cost c0 p136) 13)
		(= (processing-time c0 p136) 2)
		(= (processing-cost c0 p137) 4)
		(= (processing-time c0 p137) 2)
		(= (processing-cost c0 p138) 10)
		(= (processing-time c0 p138) 1)
		(= (processing-cost c0 p139) 17)
		(= (processing-time c0 p139) 2)
		(= (processing-cost c0 p148) 7)
		(= (processing-time c0 p148) 4)
		(= (processing-cost c0 p149) 15)
		(= (processing-time c0 p149) 7)
		(= (processing-cost c0 p150) 13)
		(= (processing-time c0 p150) 1)
		(= (processing-cost c0 p151) 8)
		(= (processing-time c0 p151) 2)
		(= (processing-cost c0 p168) 6)
		(= (processing-time c0 p168) 1)
		(= (processing-cost c0 p169) 14)
		(= (processing-time c0 p169) 2)
		(= (processing-cost c0 p170) 2)
		(= (processing-time c0 p170) 1)
		(= (processing-cost c0 p171) 11)
		(= (processing-time c0 p171) 2)
		(= (processing-cost c0 p172) 6)
		(= (processing-time c0 p172) 1)
		(= (processing-cost c0 p173) 10)
		(= (processing-time c0 p173) 1)
		(= (processing-cost c0 p174) 15)
		(= (processing-time c0 p174) 2)
		(= (processing-cost c0 p175) 9)
		(= (processing-time c0 p175) 2)
		(= (processing-cost c1 p28) 11)
		(= (processing-time c1 p28) 2)
		(= (processing-cost c1 p29) 14)
		(= (processing-time c1 p29) 1)
		(= (processing-cost c1 p30) 11)
		(= (processing-time c1 p30) 1)
		(= (processing-cost c1 p31) 10)
		(= (processing-time c1 p31) 2)
		(= (processing-cost c1 p32) 3)
		(= (processing-time c1 p32) 1)
		(= (processing-cost c1 p33) 6)
		(= (processing-time c1 p33) 1)
		(= (processing-cost c1 p34) 1)
		(= (processing-time c1 p34) 1)
		(= (processing-cost c1 p35) 6)
		(= (processing-time c1 p35) 2)
		(= (processing-cost c1 p40) 13)
		(= (processing-time c1 p40) 2)
		(= (processing-cost c1 p41) 8)
		(= (processing-time c1 p41) 2)
		(= (processing-cost c1 p42) 11)
		(= (processing-time c1 p42) 2)
		(= (processing-cost c1 p43) 6)
		(= (processing-time c1 p43) 3)
		(= (processing-cost c1 p52) 10)
		(= (processing-time c1 p52) 1)
		(= (processing-cost c1 p53) 11)
		(= (processing-time c1 p53) 4)
		(= (processing-cost c1 p54) 13)
		(= (processing-time c1 p54) 1)
		(= (processing-cost c1 p55) 10)
		(= (processing-time c1 p55) 2)
		(= (processing-cost c1 p60) 8)
		(= (processing-time c1 p60) 3)
		(= (processing-cost c1 p61) 10)
		(= (processing-time c1 p61) 1)
		(= (processing-cost c1 p62) 3)
		(= (processing-time c1 p62) 1)
		(= (processing-cost c1 p63) 10)
		(= (processing-time c1 p63) 2)
		(= (processing-cost c1 p84) 2)
		(= (processing-time c1 p84) 1)
		(= (processing-cost c1 p85) 9)
		(= (processing-time c1 p85) 1)
		(= (processing-cost c1 p86) 3)
		(= (processing-time c1 p86) 1)
		(= (processing-cost c1 p87) 11)
		(= (processing-time c1 p87) 3)
		(= (processing-cost c1 p104) 5)
		(= (processing-time c1 p104) 1)
		(= (processing-cost c1 p105) 6)
		(= (processing-time c1 p105) 1)
		(= (processing-cost c1 p106) 6)
		(= (processing-time c1 p106) 1)
		(= (processing-cost c1 p107) 15)
		(= (processing-time c1 p107) 2)
		(= (processing-cost c1 p116) 12)
		(= (processing-time c1 p116) 2)
		(= (processing-cost c1 p117) 10)
		(= (processing-time c1 p117) 1)
		(= (processing-cost c1 p118) 4)
		(= (processing-time c1 p118) 1)
		(= (processing-cost c1 p119) 12)
		(= (processing-time c1 p119) 2)
		(= (processing-cost c1 p120) 11)
		(= (processing-time c1 p120) 1)
		(= (processing-cost c1 p121) 5)
		(= (processing-time c1 p121) 2)
		(= (processing-cost c1 p122) 12)
		(= (processing-time c1 p122) 1)
		(= (processing-cost c1 p123) 8)
		(= (processing-time c1 p123) 2)
		(= (processing-cost c1 p128) 6)
		(= (processing-time c1 p128) 2)
		(= (processing-cost c1 p129) 7)
		(= (processing-time c1 p129) 1)
		(= (processing-cost c1 p130) 9)
		(= (processing-time c1 p130) 1)
		(= (processing-cost c1 p131) 17)
		(= (processing-time c1 p131) 2)
		(= (processing-cost c1 p136) 15)
		(= (processing-time c1 p136) 3)
		(= (processing-cost c1 p137) 15)
		(= (processing-time c1 p137) 3)
		(= (processing-cost c1 p138) 13)
		(= (processing-time c1 p138) 1)
		(= (processing-cost c1 p139) 10)
		(= (processing-time c1 p139) 2)
		(= (processing-cost c1 p148) 21)
		(= (processing-time c1 p148) 7)
		(= (processing-cost c1 p149) 2)
		(= (processing-time c1 p149) 1)
		(= (processing-cost c1 p150) 14)
		(= (processing-time c1 p150) 4)
		(= (processing-cost c1 p151) 12)
		(= (processing-time c1 p151) 2)
		(= (processing-cost c1 p152) 15)
		(= (processing-time c1 p152) 3)
		(= (processing-cost c1 p153) 13)
		(= (processing-time c1 p153) 2)
		(= (processing-cost c1 p154) 9)
		(= (processing-time c1 p154) 3)
		(= (processing-cost c1 p155) 12)
		(= (processing-time c1 p155) 2)
		(= (processing-cost c1 p156) 12)
		(= (processing-time c1 p156) 2)
		(= (processing-cost c1 p157) 8)
		(= (processing-time c1 p157) 1)
		(= (processing-cost c1 p158) 12)
		(= (processing-time c1 p158) 1)
		(= (processing-cost c1 p159) 9)
		(= (processing-time c1 p159) 3)
		(= (processing-cost c1 p176) 14)
		(= (processing-time c1 p176) 3)
		(= (processing-cost c1 p177) 2)
		(= (processing-time c1 p177) 1)
		(= (processing-cost c1 p178) 5)
		(= (processing-time c1 p178) 2)
		(= (processing-cost c1 p179) 14)
		(= (processing-time c1 p179) 2)
		(= (processing-cost c2 p4) 4)
		(= (processing-time c2 p4) 1)
		(= (processing-cost c2 p5) 9)
		(= (processing-time c2 p5) 1)
		(= (processing-cost c2 p6) 11)
		(= (processing-time c2 p6) 1)
		(= (processing-cost c2 p7) 14)
		(= (processing-time c2 p7) 2)
		(= (processing-cost c2 p16) 8)
		(= (processing-time c2 p16) 2)
		(= (processing-cost c2 p17) 1)
		(= (processing-time c2 p17) 1)
		(= (processing-cost c2 p18) 6)
		(= (processing-time c2 p18) 1)
		(= (processing-cost c2 p19) 12)
		(= (processing-time c2 p19) 2)
		(= (processing-cost c2 p36) 9)
		(= (processing-time c2 p36) 2)
		(= (processing-cost c2 p37) 4)
		(= (processing-time c2 p37) 2)
		(= (processing-cost c2 p38) 9)
		(= (processing-time c2 p38) 1)
		(= (processing-cost c2 p39) 15)
		(= (processing-time c2 p39) 2)
		(= (processing-cost c2 p80) 4)
		(= (processing-time c2 p80) 2)
		(= (processing-cost c2 p81) 5)
		(= (processing-time c2 p81) 1)
		(= (processing-cost c2 p82) 8)
		(= (processing-time c2 p82) 1)
		(= (processing-cost c2 p83) 7)
		(= (processing-time c2 p83) 4)
		(= (processing-cost c2 p88) 11)
		(= (processing-time c2 p88) 1)
		(= (processing-cost c2 p89) 12)
		(= (processing-time c2 p89) 6)
		(= (processing-cost c2 p90) 13)
		(= (processing-time c2 p90) 1)
		(= (processing-cost c2 p91) 12)
		(= (processing-time c2 p91) 2)
		(= (processing-cost c2 p96) 3)
		(= (processing-time c2 p96) 2)
		(= (processing-cost c2 p97) 7)
		(= (processing-time c2 p97) 1)
		(= (processing-cost c2 p98) 7)
		(= (processing-time c2 p98) 1)
		(= (processing-cost c2 p99) 11)
		(= (processing-time c2 p99) 2)
		(= (processing-cost c2 p104) 25)
		(= (processing-time c2 p104) 6)
		(= (processing-cost c2 p105) 1)
		(= (processing-time c2 p105) 1)
		(= (processing-cost c2 p106) 17)
		(= (processing-time c2 p106) 2)
		(= (processing-cost c2 p107) 6)
		(= (processing-time c2 p107) 2)
		(= (processing-cost c2 p124) 24)
		(= (processing-time c2 p124) 5)
		(= (processing-cost c2 p125) 10)
		(= (processing-time c2 p125) 1)
		(= (processing-cost c2 p126) 2)
		(= (processing-time c2 p126) 2)
		(= (processing-cost c2 p127) 14)
		(= (processing-time c2 p127) 2)
		(= (processing-cost c2 p128) 23)
		(= (processing-time c2 p128) 5)
		(= (processing-cost c2 p129) 2)
		(= (processing-time c2 p129) 1)
		(= (processing-cost c2 p130) 11)
		(= (processing-time c2 p130) 1)
		(= (processing-cost c2 p131) 17)
		(= (processing-time c2 p131) 2)
		(= (processing-cost c2 p132) 11)
		(= (processing-time c2 p132) 3)
		(= (processing-cost c2 p133) 10)
		(= (processing-time c2 p133) 2)
		(= (processing-cost c2 p134) 10)
		(= (processing-time c2 p134) 1)
		(= (processing-cost c2 p135) 7)
		(= (processing-time c2 p135) 2)
		(= (processing-cost c2 p140) 26)
		(= (processing-time c2 p140) 6)
		(= (processing-cost c2 p141) 14)
		(= (processing-time c2 p141) 2)
		(= (processing-cost c2 p142) 10)
		(= (processing-time c2 p142) 1)
		(= (processing-cost c2 p143) 9)
		(= (processing-time c2 p143) 2)
		(= (processing-cost c2 p144) 8)
		(= (processing-time c2 p144) 2)
		(= (processing-cost c2 p145) 15)
		(= (processing-time c2 p145) 3)
		(= (processing-cost c2 p146) 12)
		(= (processing-time c2 p146) 4)
		(= (processing-cost c2 p147) 8)
		(= (processing-time c2 p147) 4)
		(= (processing-cost c2 p160) 18)
		(= (processing-time c2 p160) 8)
		(= (processing-cost c2 p161) 4)
		(= (processing-time c2 p161) 4)
		(= (processing-cost c2 p162) 4)
		(= (processing-time c2 p162) 1)
		(= (processing-cost c2 p163) 12)
		(= (processing-time c2 p163) 2)
		(= (processing-cost c2 p164) 30)
		(= (processing-time c2 p164) 5)
		(= (processing-cost c2 p165) 15)
		(= (processing-time c2 p165) 2)
		(= (processing-cost c2 p166) 7)
		(= (processing-time c2 p166) 2)
		(= (processing-cost c2 p167) 24)
		(= (processing-time c2 p167) 4)
		(= (processing-cost c2 p168) 5)
		(= (processing-time c2 p168) 1)
		(= (processing-cost c2 p169) 6)
		(= (processing-time c2 p169) 2)
		(= (processing-cost c2 p170) 12)
		(= (processing-time c2 p170) 1)
		(= (processing-cost c2 p171) 6)
		(= (processing-time c2 p171) 2)
		(= (processing-cost c2 p172) 16)
		(= (processing-time c2 p172) 2)
		(= (processing-cost c2 p173) 5)
		(= (processing-time c2 p173) 1)
		(= (processing-cost c2 p174) 8)
		(= (processing-time c2 p174) 1)
		(= (processing-cost c2 p175) 6)
		(= (processing-time c2 p175) 2)
		(= (processing-cost c3 p0) 7)
		(= (processing-time c3 p0) 1)
		(= (processing-cost c3 p1) 25)
		(= (processing-time c3 p1) 6)
		(= (processing-cost c3 p2) 8)
		(= (processing-time c3 p2) 1)
		(= (processing-cost c3 p3) 13)
		(= (processing-time c3 p3) 2)
		(= (processing-cost c3 p8) 4)
		(= (processing-time c3 p8) 1)
		(= (processing-cost c3 p9) 12)
		(= (processing-time c3 p9) 2)
		(= (processing-cost c3 p10) 5)
		(= (processing-time c3 p10) 1)
		(= (processing-cost c3 p11) 5)
		(= (processing-time c3 p11) 2)
		(= (processing-cost c3 p12) 38)
		(= (processing-time c3 p12) 7)
		(= (processing-cost c3 p13) 11)
		(= (processing-time c3 p13) 1)
		(= (processing-cost c3 p14) 6)
		(= (processing-time c3 p14) 1)
		(= (processing-cost c3 p15) 8)
		(= (processing-time c3 p15) 2)
		(= (processing-cost c3 p16) 6)
		(= (processing-time c3 p16) 1)
		(= (processing-cost c3 p17) 9)
		(= (processing-time c3 p17) 2)
		(= (processing-cost c3 p18) 10)
		(= (processing-time c3 p18) 1)
		(= (processing-cost c3 p19) 8)
		(= (processing-time c3 p19) 3)
		(= (processing-cost c3 p44) 5)
		(= (processing-time c3 p44) 2)
		(= (processing-cost c3 p45) 3)
		(= (processing-time c3 p45) 1)
		(= (processing-cost c3 p46) 4)
		(= (processing-time c3 p46) 1)
		(= (processing-cost c3 p47) 10)
		(= (processing-time c3 p47) 2)
		(= (processing-cost c3 p48) 12)
		(= (processing-time c3 p48) 1)
		(= (processing-cost c3 p49) 4)
		(= (processing-time c3 p49) 1)
		(= (processing-cost c3 p50) 11)
		(= (processing-time c3 p50) 1)
		(= (processing-cost c3 p51) 3)
		(= (processing-time c3 p51) 2)
		(= (processing-cost c3 p56) 4)
		(= (processing-time c3 p56) 1)
		(= (processing-cost c3 p57) 11)
		(= (processing-time c3 p57) 3)
		(= (processing-cost c3 p58) 9)
		(= (processing-time c3 p58) 3)
		(= (processing-cost c3 p59) 12)
		(= (processing-time c3 p59) 3)
		(= (processing-cost c3 p60) 13)
		(= (processing-time c3 p60) 1)
		(= (processing-cost c3 p61) 7)
		(= (processing-time c3 p61) 1)
		(= (processing-cost c3 p62) 10)
		(= (processing-time c3 p62) 1)
		(= (processing-cost c3 p63) 14)
		(= (processing-time c3 p63) 2)
		(= (processing-cost c3 p64) 17)
		(= (processing-time c3 p64) 7)
		(= (processing-cost c3 p65) 8)
		(= (processing-time c3 p65) 1)
		(= (processing-cost c3 p66) 5)
		(= (processing-time c3 p66) 1)
		(= (processing-cost c3 p67) 11)
		(= (processing-time c3 p67) 3)
		(= (processing-cost c3 p68) 11)
		(= (processing-time c3 p68) 1)
		(= (processing-cost c3 p69) 19)
		(= (processing-time c3 p69) 3)
		(= (processing-cost c3 p70) 10)
		(= (processing-time c3 p70) 1)
		(= (processing-cost c3 p71) 10)
		(= (processing-time c3 p71) 2)
		(= (processing-cost c3 p92) 6)
		(= (processing-time c3 p92) 2)
		(= (processing-cost c3 p93) 4)
		(= (processing-time c3 p93) 2)
		(= (processing-cost c3 p94) 4)
		(= (processing-time c3 p94) 1)
		(= (processing-cost c3 p95) 8)
		(= (processing-time c3 p95) 2)
		(= (processing-cost c3 p112) 9)
		(= (processing-time c3 p112) 1)
		(= (processing-cost c3 p113) 10)
		(= (processing-time c3 p113) 1)
		(= (processing-cost c3 p114) 8)
		(= (processing-time c3 p114) 1)
		(= (processing-cost c3 p115) 8)
		(= (processing-time c3 p115) 2)
		(= (processing-cost c3 p116) 9)
		(= (processing-time c3 p116) 1)
		(= (processing-cost c3 p117) 9)
		(= (processing-time c3 p117) 1)
		(= (processing-cost c3 p118) 9)
		(= (processing-time c3 p118) 1)
		(= (processing-cost c3 p119) 8)
		(= (processing-time c3 p119) 2)
		(= (processing-cost c3 p120) 6)
		(= (processing-time c3 p120) 1)
		(= (processing-cost c3 p121) 11)
		(= (processing-time c3 p121) 1)
		(= (processing-cost c3 p122) 13)
		(= (processing-time c3 p122) 1)
		(= (processing-cost c3 p123) 12)
		(= (processing-time c3 p123) 4)
		(= (processing-cost c3 p140) 23)
		(= (processing-time c3 p140) 6)
		(= (processing-cost c3 p141) 12)
		(= (processing-time c3 p141) 1)
		(= (processing-cost c3 p142) 7)
		(= (processing-time c3 p142) 1)
		(= (processing-cost c3 p143) 13)
		(= (processing-time c3 p143) 2)
		(= (processing-cost c3 p164) 14)
		(= (processing-time c3 p164) 6)
		(= (processing-cost c3 p165) 14)
		(= (processing-time c3 p165) 1)
		(= (processing-cost c3 p166) 8)
		(= (processing-time c3 p166) 2)
		(= (processing-cost c3 p167) 15)
		(= (processing-time c3 p167) 2)

		(not-depends p0)

		(not-depends p1)

		(depends-one p2 p0)

		(depends-one p3 p2)

		(not-depends p4)

		(depends-one p5 p6)

		(not-depends p6)

		(not-depends p7)

		(not-depends p8)

		(not-depends p9)

		(depends-one p10 p11)

		(not-depends p11)

		(not-depends p12)

		(not-depends p13)

		(not-depends p14)

		(not-depends p15)

		(not-depends p16)

		(not-depends p17)

		(not-depends p18)

		(not-depends p19)

		(depends-one p20 p21)

		(not-depends p21)

		(not-depends p22)

		(not-depends p23)

		(not-depends p24)

		(not-depends p25)

		(not-depends p26)

		(not-depends p27)

		(not-depends p28)

		(not-depends p29)

		(depends-one p30 p28)

		(not-depends p31)

		(depends-one p32 p34)

		(depends-one p33 p35)

		(not-depends p34)

		(depends-one p35 p32)

		(not-depends p36)

		(depends-one p37 p36)

		(not-depends p38)

		(not-depends p39)

		(not-depends p40)

		(depends-one p41 p42)

		(not-depends p42)

		(not-depends p43)

		(depends-one p44 p47)

		(depends-one p45 p46)

		(depends-one p46 p47)

		(not-depends p47)

		(not-depends p48)

		(depends-one p49 p51)

		(not-depends p50)

		(not-depends p51)

		(not-depends p52)

		(depends-one p53 p55)

		(not-depends p54)

		(not-depends p55)

		(not-depends p56)

		(depends-one p57 p59)

		(depends-one p58 p56)

		(not-depends p59)

		(not-depends p60)

		(not-depends p61)

		(not-depends p62)

		(depends-one p63 p61)

		(not-depends p64)

		(depends-one p65 p64)

		(not-depends p66)

		(depends-one p67 p64)

		(not-depends p68)

		(not-depends p69)

		(not-depends p70)

		(not-depends p71)

		(not-depends p72)

		(depends-one p73 p74)

		(not-depends p74)

		(not-depends p75)

		(not-depends p76)

		(not-depends p77)

		(not-depends p78)

		(not-depends p79)

		(not-depends p80)

		(not-depends p81)

		(not-depends p82)

		(not-depends p83)

		(depends-one p84 p85)

		(not-depends p85)

		(depends-one p86 p87)

		(not-depends p87)

		(not-depends p88)

		(not-depends p89)

		(depends-one p90 p89)

		(depends-one p91 p90)

		(not-depends p92)

		(not-depends p93)

		(not-depends p94)

		(not-depends p95)

		(not-depends p96)

		(not-depends p97)

		(not-depends p98)

		(not-depends p99)

		(not-depends p100)

		(not-depends p101)

		(not-depends p102)

		(depends-one p103 p100)

		(depends-one p104 p105)

		(not-depends p105)

		(not-depends p106)

		(not-depends p107)

		(depends-one p108 p110)

		(not-depends p109)

		(not-depends p110)

		(depends-one p111 p108)

		(not-depends p112)

		(not-depends p113)

		(not-depends p114)

		(not-depends p115)

		(not-depends p116)

		(not-depends p117)

		(not-depends p118)

		(depends-one p119 p117)

		(not-depends p120)

		(not-depends p121)

		(not-depends p122)

		(not-depends p123)

		(depends-one p124 p126)

		(not-depends p125)

		(not-depends p126)

		(depends-one p127 p125)

		(not-depends p128)

		(depends-one p129 p128)

		(not-depends p130)

		(depends-one p131 p130)

		(not-depends p132)

		(not-depends p133)

		(depends-one p134 p133)

		(depends-one p135 p133)

		(not-depends p136)

		(depends-one p137 p136)

		(not-depends p138)

		(not-depends p139)

		(depends-one p140 p141)

		(depends-one p141 p142)

		(not-depends p142)

		(not-depends p143)

		(not-depends p144)

		(depends-one p145 p147)

		(depends-one p146 p147)

		(not-depends p147)

		(not-depends p148)

		(not-depends p149)

		(not-depends p150)

		(depends-one p151 p149)

		(depends-one p152 p154)

		(not-depends p153)

		(not-depends p154)

		(depends-one p155 p154)

		(not-depends p156)

		(depends-one p157 p156)

		(not-depends p158)

		(not-depends p159)

		(depends-one p160 p163)

		(not-depends p161)

		(not-depends p162)

		(not-depends p163)

		(not-depends p164)

		(depends-one p165 p167)

		(not-depends p166)

		(depends-one p167 p164)

		(not-depends p168)

		(depends-one p169 p171)

		(not-depends p170)

		(depends-one p171 p168)

		(not-depends p172)

		(not-depends p173)

		(depends-one p174 p172)

		(not-depends p175)

		(not-depends p176)

		(not-depends p177)

		(not-depends p178)

		(not-depends p179)

		(not-completed p0)

		(not-completed p1)

		(not-completed p2)

		(not-completed p3)

		(not-completed p4)

		(not-completed p5)

		(not-completed p6)

		(not-completed p7)

		(not-completed p8)

		(not-completed p9)

		(not-completed p10)

		(not-completed p11)

		(not-completed p12)

		(not-completed p13)

		(not-completed p14)

		(not-completed p15)

		(not-completed p16)

		(not-completed p17)

		(not-completed p18)

		(not-completed p19)

		(not-completed p20)

		(not-completed p21)

		(not-completed p22)

		(not-completed p23)

		(not-completed p24)

		(not-completed p25)

		(not-completed p26)

		(not-completed p27)

		(not-completed p28)

		(not-completed p29)

		(not-completed p30)

		(not-completed p31)

		(not-completed p32)

		(not-completed p33)

		(not-completed p34)

		(not-completed p35)

		(not-completed p36)

		(not-completed p37)

		(not-completed p38)

		(not-completed p39)

		(not-completed p40)

		(not-completed p41)

		(not-completed p42)

		(not-completed p43)

		(not-completed p44)

		(not-completed p45)

		(not-completed p46)

		(not-completed p47)

		(not-completed p48)

		(not-completed p49)

		(not-completed p50)

		(not-completed p51)

		(not-completed p52)

		(not-completed p53)

		(not-completed p54)

		(not-completed p55)

		(not-completed p56)

		(not-completed p57)

		(not-completed p58)

		(not-completed p59)

		(not-completed p60)

		(not-completed p61)

		(not-completed p62)

		(not-completed p63)

		(not-completed p64)

		(not-completed p65)

		(not-completed p66)

		(not-completed p67)

		(not-completed p68)

		(not-completed p69)

		(not-completed p70)

		(not-completed p71)

		(not-completed p72)

		(not-completed p73)

		(not-completed p74)

		(not-completed p75)

		(not-completed p76)

		(not-completed p77)

		(not-completed p78)

		(not-completed p79)

		(not-completed p80)

		(not-completed p81)

		(not-completed p82)

		(not-completed p83)

		(not-completed p84)

		(not-completed p85)

		(not-completed p86)

		(not-completed p87)

		(not-completed p88)

		(not-completed p89)

		(not-completed p90)

		(not-completed p91)

		(not-completed p92)

		(not-completed p93)

		(not-completed p94)

		(not-completed p95)

		(not-completed p96)

		(not-completed p97)

		(not-completed p98)

		(not-completed p99)

		(not-completed p100)

		(not-completed p101)

		(not-completed p102)

		(not-completed p103)

		(not-completed p104)

		(not-completed p105)

		(not-completed p106)

		(not-completed p107)

		(not-completed p108)

		(not-completed p109)

		(not-completed p110)

		(not-completed p111)

		(not-completed p112)

		(not-completed p113)

		(not-completed p114)

		(not-completed p115)

		(not-completed p116)

		(not-completed p117)

		(not-completed p118)

		(not-completed p119)

		(not-completed p120)

		(not-completed p121)

		(not-completed p122)

		(not-completed p123)

		(not-completed p124)

		(not-completed p125)

		(not-completed p126)

		(not-completed p127)

		(not-completed p128)

		(not-completed p129)

		(not-completed p130)

		(not-completed p131)

		(not-completed p132)

		(not-completed p133)

		(not-completed p134)

		(not-completed p135)

		(not-completed p136)

		(not-completed p137)

		(not-completed p138)

		(not-completed p139)

		(not-completed p140)

		(not-completed p141)

		(not-completed p142)

		(not-completed p143)

		(not-completed p144)

		(not-completed p145)

		(not-completed p146)

		(not-completed p147)

		(not-completed p148)

		(not-completed p149)

		(not-completed p150)

		(not-completed p151)

		(not-completed p152)

		(not-completed p153)

		(not-completed p154)

		(not-completed p155)

		(not-completed p156)

		(not-completed p157)

		(not-completed p158)

		(not-completed p159)

		(not-completed p160)

		(not-completed p161)

		(not-completed p162)

		(not-completed p163)

		(not-completed p164)

		(not-completed p165)

		(not-completed p166)

		(not-completed p167)

		(not-completed p168)

		(not-completed p169)

		(not-completed p170)

		(not-completed p171)

		(not-completed p172)

		(not-completed p173)

		(not-completed p174)

		(not-completed p175)

		(not-completed p176)

		(not-completed p177)

		(not-completed p178)

		(not-completed p179)

	)

	(:goal
		(and
			(completed p0)
			(completed p1)
			(completed p2)
			(completed p3)
			(completed p4)
			(completed p5)
			(completed p6)
			(completed p7)
			(completed p8)
			(completed p9)
			(completed p10)
			(completed p11)
			(completed p12)
			(completed p13)
			(completed p14)
			(completed p15)
			(completed p16)
			(completed p17)
			(completed p18)
			(completed p19)
			(completed p20)
			(completed p21)
			(completed p22)
			(completed p23)
			(completed p24)
			(completed p25)
			(completed p26)
			(completed p27)
			(completed p28)
			(completed p29)
			(completed p30)
			(completed p31)
			(completed p32)
			(completed p33)
			(completed p34)
			(completed p35)
			(completed p36)
			(completed p37)
			(completed p38)
			(completed p39)
			(completed p40)
			(completed p41)
			(completed p42)
			(completed p43)
			(completed p44)
			(completed p45)
			(completed p46)
			(completed p47)
			(completed p48)
			(completed p49)
			(completed p50)
			(completed p51)
			(completed p52)
			(completed p53)
			(completed p54)
			(completed p55)
			(completed p56)
			(completed p57)
			(completed p58)
			(completed p59)
			(completed p60)
			(completed p61)
			(completed p62)
			(completed p63)
			(completed p64)
			(completed p65)
			(completed p66)
			(completed p67)
			(completed p68)
			(completed p69)
			(completed p70)
			(completed p71)
			(completed p72)
			(completed p73)
			(completed p74)
			(completed p75)
			(completed p76)
			(completed p77)
			(completed p78)
			(completed p79)
			(completed p80)
			(completed p81)
			(completed p82)
			(completed p83)
			(completed p84)
			(completed p85)
			(completed p86)
			(completed p87)
			(completed p88)
			(completed p89)
			(completed p90)
			(completed p91)
			(completed p92)
			(completed p93)
			(completed p94)
			(completed p95)
			(completed p96)
			(completed p97)
			(completed p98)
			(completed p99)
			(completed p100)
			(completed p101)
			(completed p102)
			(completed p103)
			(completed p104)
			(completed p105)
			(completed p106)
			(completed p107)
			(completed p108)
			(completed p109)
			(completed p110)
			(completed p111)
			(completed p112)
			(completed p113)
			(completed p114)
			(completed p115)
			(completed p116)
			(completed p117)
			(completed p118)
			(completed p119)
			(completed p120)
			(completed p121)
			(completed p122)
			(completed p123)
			(completed p124)
			(completed p125)
			(completed p126)
			(completed p127)
			(completed p128)
			(completed p129)
			(completed p130)
			(completed p131)
			(completed p132)
			(completed p133)
			(completed p134)
			(completed p135)
			(completed p136)
			(completed p137)
			(completed p138)
			(completed p139)
			(completed p140)
			(completed p141)
			(completed p142)
			(completed p143)
			(completed p144)
			(completed p145)
			(completed p146)
			(completed p147)
			(completed p148)
			(completed p149)
			(completed p150)
			(completed p151)
			(completed p152)
			(completed p153)
			(completed p154)
			(completed p155)
			(completed p156)
			(completed p157)
			(completed p158)
			(completed p159)
			(completed p160)
			(completed p161)
			(completed p162)
			(completed p163)
			(completed p164)
			(completed p165)
			(completed p166)
			(completed p167)
			(completed p168)
			(completed p169)
			(completed p170)
			(completed p171)
			(completed p172)
			(completed p173)
			(completed p174)
			(completed p175)
			(completed p176)
			(completed p177)
			(completed p178)
			(completed p179)
		)
	)

	(:metric minimize (total-time))		; optimal cost = 1068

)