(define

	(problem ps_domain_rmt-instance)
	(:domain ps_domain_rmt)

	(:objects
		m0 m1 - machine
		c0 c1 c2 c3 - configuration
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 - process
	)

	(:init

		(configured m0 c2)
		(configured m1 c0)

		(configurable m0 c1)
		(configurable m0 c2)
		(configurable m0 c3)
		(= (reconfiguration-cost c1 c2) 32)
		(= (reconfiguration-time c1 c2) 10)
		(= (reconfiguration-cost c2 c1) 12)
		(= (reconfiguration-time c2 c1) 10)
		(not-same c1 c2)
		(not-same c2 c1)
		(= (reconfiguration-cost c1 c3) 24)
		(= (reconfiguration-time c1 c3) 8)
		(= (reconfiguration-cost c3 c1) 36)
		(= (reconfiguration-time c3 c1) 8)
		(not-same c1 c3)
		(not-same c3 c1)
		(= (reconfiguration-cost c2 c3) 21)
		(= (reconfiguration-time c2 c3) 4)
		(= (reconfiguration-cost c3 c2) 23)
		(= (reconfiguration-time c3 c2) 4)
		(not-same c2 c3)
		(not-same c3 c2)


		(processable c0 p0)
		(processable c0 p1)
		(processable c0 p2)
		(processable c0 p3)
		(processable c0 p8)
		(processable c0 p9)
		(processable c0 p10)
		(processable c0 p11)
		(processable c0 p16)
		(processable c0 p17)
		(processable c0 p18)
		(processable c0 p19)
		(processable c1 p4)
		(processable c1 p5)
		(processable c1 p6)
		(processable c1 p7)
		(processable c1 p8)
		(processable c1 p9)
		(processable c1 p10)
		(processable c1 p11)
		(processable c1 p16)
		(processable c1 p17)
		(processable c1 p18)
		(processable c1 p19)
		(processable c2 p0)
		(processable c2 p1)
		(processable c2 p2)
		(processable c2 p3)
		(processable c2 p4)
		(processable c2 p5)
		(processable c2 p6)
		(processable c2 p7)
		(processable c3 p12)
		(processable c3 p13)
		(processable c3 p14)
		(processable c3 p15)
		(= (processing-cost c0 p0) 6)
		(= (processing-time c0 p0) 2)
		(= (processing-cost c0 p1) 8)
		(= (processing-time c0 p1) 1)
		(= (processing-cost c0 p2) 12)
		(= (processing-time c0 p2) 1)
		(= (processing-cost c0 p3) 8)
		(= (processing-time c0 p3) 2)
		(= (processing-cost c0 p8) 8)
		(= (processing-time c0 p8) 2)
		(= (processing-cost c0 p9) 7)
		(= (processing-time c0 p9) 1)
		(= (processing-cost c0 p10) 4)
		(= (processing-time c0 p10) 1)
		(= (processing-cost c0 p11) 8)
		(= (processing-time c0 p11) 4)
		(= (processing-cost c0 p16) 27)
		(= (processing-time c0 p16) 5)
		(= (processing-cost c0 p17) 6)
		(= (processing-time c0 p17) 1)
		(= (processing-cost c0 p18) 5)
		(= (processing-time c0 p18) 2)
		(= (processing-cost c0 p19) 13)
		(= (processing-time c0 p19) 2)
		(= (processing-cost c1 p4) 15)
		(= (processing-time c1 p4) 2)
		(= (processing-cost c1 p5) 12)
		(= (processing-time c1 p5) 2)
		(= (processing-cost c1 p6) 4)
		(= (processing-time c1 p6) 1)
		(= (processing-cost c1 p7) 9)
		(= (processing-time c1 p7) 2)
		(= (processing-cost c1 p8) 9)
		(= (processing-time c1 p8) 1)
		(= (processing-cost c1 p9) 11)
		(= (processing-time c1 p9) 1)
		(= (processing-cost c1 p10) 8)
		(= (processing-time c1 p10) 1)
		(= (processing-cost c1 p11) 6)
		(= (processing-time c1 p11) 2)
		(= (processing-cost c1 p16) 11)
		(= (processing-time c1 p16) 2)
		(= (processing-cost c1 p17) 9)
		(= (processing-time c1 p17) 3)
		(= (processing-cost c1 p18) 17)
		(= (processing-time c1 p18) 4)
		(= (processing-cost c1 p19) 15)
		(= (processing-time c1 p19) 2)
		(= (processing-cost c2 p0) 11)
		(= (processing-time c2 p0) 5)
		(= (processing-cost c2 p1) 8)
		(= (processing-time c2 p1) 1)
		(= (processing-cost c2 p2) 9)
		(= (processing-time c2 p2) 1)
		(= (processing-cost c2 p3) 4)
		(= (processing-time c2 p3) 2)
		(= (processing-cost c2 p4) 8)
		(= (processing-time c2 p4) 2)
		(= (processing-cost c2 p5) 13)
		(= (processing-time c2 p5) 3)
		(= (processing-cost c2 p6) 6)
		(= (processing-time c2 p6) 2)
		(= (processing-cost c2 p7) 15)
		(= (processing-time c2 p7) 3)
		(= (processing-cost c3 p12) 8)
		(= (processing-time c3 p12) 2)
		(= (processing-cost c3 p13) 5)
		(= (processing-time c3 p13) 5)
		(= (processing-cost c3 p14) 6)
		(= (processing-time c3 p14) 1)
		(= (processing-cost c3 p15) 12)
		(= (processing-time c3 p15) 2)


		(not-depends p0)

		(not-depends p1)

		(depends-one p2 p3)

		(not-depends p3)

		(not-depends p4)

		(not-depends p5)

		(not-depends p6)

		(not-depends p7)

		(not-depends p8)

		(not-depends p9)

		(not-depends p10)

		(depends-one p11 p8)

		(not-depends p12)

		(depends-one p13 p15)

		(depends-one p14 p13)

		(not-depends p15)

		(not-depends p16)

		(not-depends p17)

		(not-depends p18)

		(not-depends p19)

		(not-completed p0)

		(not-completed p1)

		(not-completed p2)

		(not-completed p3)

		(not-completed p4)

		(not-completed p5)

		(not-completed p6)

		(not-completed p7)

		(not-completed p8)

		(not-completed p9)

		(not-completed p10)

		(not-completed p11)

		(not-completed p12)

		(not-completed p13)

		(not-completed p14)

		(not-completed p15)

		(not-completed p16)

		(not-completed p17)

		(not-completed p18)

		(not-completed p19)


	)

	(:goal
		(and
			(completed p0)
			(completed p1)
			(completed p2)
			(completed p3)
			(completed p4)
			(completed p5)
			(completed p6)
			(completed p7)
			(completed p8)
			(completed p9)
			(completed p10)
			(completed p11)
			(completed p12)
			(completed p13)
			(completed p14)
			(completed p15)
			(completed p16)
			(completed p17)
			(completed p18)
			(completed p19)
		)
	)

	(:metric minimize (total-time))		; optimal cost = 138

)