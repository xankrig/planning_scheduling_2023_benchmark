(define

	(problem ps_domain_rmt-instance)
	(:domain ps_domain_rmt)

	(:objects
		m0 m1 - machine
		c0 c1 c2 c3 - configuration
		p0 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12 p13 p14 p15 p16 p17 p18 p19 p20 p21 p22 p23 p24 p25 p26 p27 p28 p29 p30 p31 p32 p33 p34 p35 p36 p37 p38 p39 p40 p41 p42 p43 p44 p45 p46 p47 p48 p49 p50 p51 p52 p53 p54 p55 p56 p57 p58 p59 p60 p61 p62 p63 p64 p65 p66 p67 p68 p69 p70 p71 p72 p73 p74 p75 p76 p77 p78 p79 p80 p81 p82 p83 p84 p85 p86 p87 p88 p89 p90 p91 p92 p93 p94 p95 p96 p97 p98 p99 p100 p101 p102 p103 p104 p105 p106 p107 p108 p109 p110 p111 p112 p113 p114 p115 p116 p117 p118 p119 p120 p121 p122 p123 p124 p125 p126 p127 p128 p129 p130 p131 p132 p133 p134 p135 p136 p137 p138 p139 p140 p141 p142 p143 p144 p145 p146 p147 p148 p149 p150 p151 p152 p153 p154 p155 p156 p157 p158 p159 p160 p161 p162 p163 p164 p165 p166 p167 p168 p169 p170 p171 p172 p173 p174 p175 p176 p177 p178 p179 p180 p181 p182 p183 p184 p185 p186 p187 p188 p189 p190 p191 p192 p193 p194 p195 p196 p197 p198 p199 - process
	)

	(:init

		(configured m0 c3)
		(configured m1 c0)

		(configurable m0 c1)
		(configurable m0 c2)
		(configurable m0 c3)
		(configurable m1 c0)
		(configurable m1 c2)
		(configurable m1 c3)
		(= (reconfiguration-cost c0 c2) 7)
		(= (reconfiguration-time c0 c2) 1)
		(= (reconfiguration-cost c2 c0) 9)
		(= (reconfiguration-time c2 c0) 1)
		(not-same c0 c2)
		(not-same c2 c0)
		(= (reconfiguration-cost c0 c3) 22)
		(= (reconfiguration-time c0 c3) 3)
		(= (reconfiguration-cost c3 c0) 12)
		(= (reconfiguration-time c3 c0) 3)
		(not-same c0 c3)
		(not-same c3 c0)
		(= (reconfiguration-cost c1 c2) 15)
		(= (reconfiguration-time c1 c2) 6)
		(= (reconfiguration-cost c2 c1) 26)
		(= (reconfiguration-time c2 c1) 6)
		(not-same c1 c2)
		(not-same c2 c1)
		(= (reconfiguration-cost c1 c3) 8)
		(= (reconfiguration-time c1 c3) 1)
		(= (reconfiguration-cost c3 c1) 4)
		(= (reconfiguration-time c3 c1) 1)
		(not-same c1 c3)
		(not-same c3 c1)
		(= (reconfiguration-cost c2 c3) 10)
		(= (reconfiguration-time c2 c3) 4)
		(= (reconfiguration-cost c3 c2) 18)
		(= (reconfiguration-time c3 c2) 4)
		(not-same c2 c3)
		(not-same c3 c2)


		(processable c0 p24)
		(processable c0 p25)
		(processable c0 p26)
		(processable c0 p27)
		(processable c0 p40)
		(processable c0 p41)
		(processable c0 p42)
		(processable c0 p43)
		(processable c0 p44)
		(processable c0 p45)
		(processable c0 p46)
		(processable c0 p47)
		(processable c0 p56)
		(processable c0 p57)
		(processable c0 p58)
		(processable c0 p59)
		(processable c0 p60)
		(processable c0 p61)
		(processable c0 p62)
		(processable c0 p63)
		(processable c0 p76)
		(processable c0 p77)
		(processable c0 p78)
		(processable c0 p79)
		(processable c0 p104)
		(processable c0 p105)
		(processable c0 p106)
		(processable c0 p107)
		(processable c0 p116)
		(processable c0 p117)
		(processable c0 p118)
		(processable c0 p119)
		(processable c0 p132)
		(processable c0 p133)
		(processable c0 p134)
		(processable c0 p135)
		(processable c0 p136)
		(processable c0 p137)
		(processable c0 p138)
		(processable c0 p139)
		(processable c0 p140)
		(processable c0 p141)
		(processable c0 p142)
		(processable c0 p143)
		(processable c0 p144)
		(processable c0 p145)
		(processable c0 p146)
		(processable c0 p147)
		(processable c0 p156)
		(processable c0 p157)
		(processable c0 p158)
		(processable c0 p159)
		(processable c0 p168)
		(processable c0 p169)
		(processable c0 p170)
		(processable c0 p171)
		(processable c0 p176)
		(processable c0 p177)
		(processable c0 p178)
		(processable c0 p179)
		(processable c0 p180)
		(processable c0 p181)
		(processable c0 p182)
		(processable c0 p183)
		(processable c1 p0)
		(processable c1 p1)
		(processable c1 p2)
		(processable c1 p3)
		(processable c1 p12)
		(processable c1 p13)
		(processable c1 p14)
		(processable c1 p15)
		(processable c1 p28)
		(processable c1 p29)
		(processable c1 p30)
		(processable c1 p31)
		(processable c1 p44)
		(processable c1 p45)
		(processable c1 p46)
		(processable c1 p47)
		(processable c1 p48)
		(processable c1 p49)
		(processable c1 p50)
		(processable c1 p51)
		(processable c1 p64)
		(processable c1 p65)
		(processable c1 p66)
		(processable c1 p67)
		(processable c1 p72)
		(processable c1 p73)
		(processable c1 p74)
		(processable c1 p75)
		(processable c1 p80)
		(processable c1 p81)
		(processable c1 p82)
		(processable c1 p83)
		(processable c1 p92)
		(processable c1 p93)
		(processable c1 p94)
		(processable c1 p95)
		(processable c1 p96)
		(processable c1 p97)
		(processable c1 p98)
		(processable c1 p99)
		(processable c1 p100)
		(processable c1 p101)
		(processable c1 p102)
		(processable c1 p103)
		(processable c1 p108)
		(processable c1 p109)
		(processable c1 p110)
		(processable c1 p111)
		(processable c1 p116)
		(processable c1 p117)
		(processable c1 p118)
		(processable c1 p119)
		(processable c1 p128)
		(processable c1 p129)
		(processable c1 p130)
		(processable c1 p131)
		(processable c1 p156)
		(processable c1 p157)
		(processable c1 p158)
		(processable c1 p159)
		(processable c1 p160)
		(processable c1 p161)
		(processable c1 p162)
		(processable c1 p163)
		(processable c1 p172)
		(processable c1 p173)
		(processable c1 p174)
		(processable c1 p175)
		(processable c1 p188)
		(processable c1 p189)
		(processable c1 p190)
		(processable c1 p191)
		(processable c1 p196)
		(processable c1 p197)
		(processable c1 p198)
		(processable c1 p199)
		(processable c2 p0)
		(processable c2 p1)
		(processable c2 p2)
		(processable c2 p3)
		(processable c2 p4)
		(processable c2 p5)
		(processable c2 p6)
		(processable c2 p7)
		(processable c2 p8)
		(processable c2 p9)
		(processable c2 p10)
		(processable c2 p11)
		(processable c2 p32)
		(processable c2 p33)
		(processable c2 p34)
		(processable c2 p35)
		(processable c2 p52)
		(processable c2 p53)
		(processable c2 p54)
		(processable c2 p55)
		(processable c2 p88)
		(processable c2 p89)
		(processable c2 p90)
		(processable c2 p91)
		(processable c2 p104)
		(processable c2 p105)
		(processable c2 p106)
		(processable c2 p107)
		(processable c2 p108)
		(processable c2 p109)
		(processable c2 p110)
		(processable c2 p111)
		(processable c2 p112)
		(processable c2 p113)
		(processable c2 p114)
		(processable c2 p115)
		(processable c2 p120)
		(processable c2 p121)
		(processable c2 p122)
		(processable c2 p123)
		(processable c2 p124)
		(processable c2 p125)
		(processable c2 p126)
		(processable c2 p127)
		(processable c2 p140)
		(processable c2 p141)
		(processable c2 p142)
		(processable c2 p143)
		(processable c2 p144)
		(processable c2 p145)
		(processable c2 p146)
		(processable c2 p147)
		(processable c2 p148)
		(processable c2 p149)
		(processable c2 p150)
		(processable c2 p151)
		(processable c2 p152)
		(processable c2 p153)
		(processable c2 p154)
		(processable c2 p155)
		(processable c2 p164)
		(processable c2 p165)
		(processable c2 p166)
		(processable c2 p167)
		(processable c2 p184)
		(processable c2 p185)
		(processable c2 p186)
		(processable c2 p187)
		(processable c2 p192)
		(processable c2 p193)
		(processable c2 p194)
		(processable c2 p195)
		(processable c3 p4)
		(processable c3 p5)
		(processable c3 p6)
		(processable c3 p7)
		(processable c3 p8)
		(processable c3 p9)
		(processable c3 p10)
		(processable c3 p11)
		(processable c3 p16)
		(processable c3 p17)
		(processable c3 p18)
		(processable c3 p19)
		(processable c3 p20)
		(processable c3 p21)
		(processable c3 p22)
		(processable c3 p23)
		(processable c3 p36)
		(processable c3 p37)
		(processable c3 p38)
		(processable c3 p39)
		(processable c3 p60)
		(processable c3 p61)
		(processable c3 p62)
		(processable c3 p63)
		(processable c3 p68)
		(processable c3 p69)
		(processable c3 p70)
		(processable c3 p71)
		(processable c3 p72)
		(processable c3 p73)
		(processable c3 p74)
		(processable c3 p75)
		(processable c3 p84)
		(processable c3 p85)
		(processable c3 p86)
		(processable c3 p87)
		(processable c3 p88)
		(processable c3 p89)
		(processable c3 p90)
		(processable c3 p91)
		(processable c3 p92)
		(processable c3 p93)
		(processable c3 p94)
		(processable c3 p95)
		(processable c3 p168)
		(processable c3 p169)
		(processable c3 p170)
		(processable c3 p171)
		(processable c3 p180)
		(processable c3 p181)
		(processable c3 p182)
		(processable c3 p183)
		(processable c3 p184)
		(processable c3 p185)
		(processable c3 p186)
		(processable c3 p187)

		(= (processing-cost c0 p24) 7)
		(= (processing-time c0 p24) 1)
		(= (processing-cost c0 p25) 4)
		(= (processing-time c0 p25) 1)
		(= (processing-cost c0 p26) 4)
		(= (processing-time c0 p26) 1)
		(= (processing-cost c0 p27) 2)
		(= (processing-time c0 p27) 2)
		(= (processing-cost c0 p40) 17)
		(= (processing-time c0 p40) 4)
		(= (processing-cost c0 p41) 6)
		(= (processing-time c0 p41) 1)
		(= (processing-cost c0 p42) 16)
		(= (processing-time c0 p42) 2)
		(= (processing-cost c0 p43) 12)
		(= (processing-time c0 p43) 2)
		(= (processing-cost c0 p44) 11)
		(= (processing-time c0 p44) 2)
		(= (processing-cost c0 p45) 7)
		(= (processing-time c0 p45) 1)
		(= (processing-cost c0 p46) 12)
		(= (processing-time c0 p46) 2)
		(= (processing-cost c0 p47) 9)
		(= (processing-time c0 p47) 2)
		(= (processing-cost c0 p56) 2)
		(= (processing-time c0 p56) 1)
		(= (processing-cost c0 p57) 7)
		(= (processing-time c0 p57) 1)
		(= (processing-cost c0 p58) 13)
		(= (processing-time c0 p58) 1)
		(= (processing-cost c0 p59) 12)
		(= (processing-time c0 p59) 2)
		(= (processing-cost c0 p60) 11)
		(= (processing-time c0 p60) 4)
		(= (processing-cost c0 p61) 7)
		(= (processing-time c0 p61) 2)
		(= (processing-cost c0 p62) 9)
		(= (processing-time c0 p62) 1)
		(= (processing-cost c0 p63) 13)
		(= (processing-time c0 p63) 3)
		(= (processing-cost c0 p76) 10)
		(= (processing-time c0 p76) 1)
		(= (processing-cost c0 p77) 12)
		(= (processing-time c0 p77) 1)
		(= (processing-cost c0 p78) 9)
		(= (processing-time c0 p78) 1)
		(= (processing-cost c0 p79) 6)
		(= (processing-time c0 p79) 2)
		(= (processing-cost c0 p104) 20)
		(= (processing-time c0 p104) 4)
		(= (processing-cost c0 p105) 6)
		(= (processing-time c0 p105) 2)
		(= (processing-cost c0 p106) 18)
		(= (processing-time c0 p106) 3)
		(= (processing-cost c0 p107) 15)
		(= (processing-time c0 p107) 2)
		(= (processing-cost c0 p116) 10)
		(= (processing-time c0 p116) 1)
		(= (processing-cost c0 p117) 8)
		(= (processing-time c0 p117) 2)
		(= (processing-cost c0 p118) 5)
		(= (processing-time c0 p118) 4)
		(= (processing-cost c0 p119) 6)
		(= (processing-time c0 p119) 2)
		(= (processing-cost c0 p132) 12)
		(= (processing-time c0 p132) 1)
		(= (processing-cost c0 p133) 10)
		(= (processing-time c0 p133) 1)
		(= (processing-cost c0 p134) 16)
		(= (processing-time c0 p134) 2)
		(= (processing-cost c0 p135) 15)
		(= (processing-time c0 p135) 4)
		(= (processing-cost c0 p136) 11)
		(= (processing-time c0 p136) 1)
		(= (processing-cost c0 p137) 15)
		(= (processing-time c0 p137) 5)
		(= (processing-cost c0 p138) 11)
		(= (processing-time c0 p138) 1)
		(= (processing-cost c0 p139) 12)
		(= (processing-time c0 p139) 2)
		(= (processing-cost c0 p140) 12)
		(= (processing-time c0 p140) 2)
		(= (processing-cost c0 p141) 12)
		(= (processing-time c0 p141) 1)
		(= (processing-cost c0 p142) 3)
		(= (processing-time c0 p142) 1)
		(= (processing-cost c0 p143) 14)
		(= (processing-time c0 p143) 2)
		(= (processing-cost c0 p144) 4)
		(= (processing-time c0 p144) 1)
		(= (processing-cost c0 p145) 9)
		(= (processing-time c0 p145) 4)
		(= (processing-cost c0 p146) 4)
		(= (processing-time c0 p146) 1)
		(= (processing-cost c0 p147) 8)
		(= (processing-time c0 p147) 3)
		(= (processing-cost c0 p156) 6)
		(= (processing-time c0 p156) 1)
		(= (processing-cost c0 p157) 5)
		(= (processing-time c0 p157) 1)
		(= (processing-cost c0 p158) 8)
		(= (processing-time c0 p158) 1)
		(= (processing-cost c0 p159) 17)
		(= (processing-time c0 p159) 2)
		(= (processing-cost c0 p168) 8)
		(= (processing-time c0 p168) 1)
		(= (processing-cost c0 p169) 32)
		(= (processing-time c0 p169) 7)
		(= (processing-cost c0 p170) 7)
		(= (processing-time c0 p170) 1)
		(= (processing-cost c0 p171) 13)
		(= (processing-time c0 p171) 2)
		(= (processing-cost c0 p176) 11)
		(= (processing-time c0 p176) 2)
		(= (processing-cost c0 p177) 4)
		(= (processing-time c0 p177) 2)
		(= (processing-cost c0 p178) 13)
		(= (processing-time c0 p178) 2)
		(= (processing-cost c0 p179) 7)
		(= (processing-time c0 p179) 2)
		(= (processing-cost c0 p180) 24)
		(= (processing-time c0 p180) 5)
		(= (processing-cost c0 p181) 7)
		(= (processing-time c0 p181) 1)
		(= (processing-cost c0 p182) 5)
		(= (processing-time c0 p182) 1)
		(= (processing-cost c0 p183) 7)
		(= (processing-time c0 p183) 2)
		(= (processing-cost c1 p0) 8)
		(= (processing-time c1 p0) 1)
		(= (processing-cost c1 p1) 18)
		(= (processing-time c1 p1) 6)
		(= (processing-cost c1 p2) 10)
		(= (processing-time c1 p2) 1)
		(= (processing-cost c1 p3) 16)
		(= (processing-time c1 p3) 2)
		(= (processing-cost c1 p12) 10)
		(= (processing-time c1 p12) 1)
		(= (processing-cost c1 p13) 16)
		(= (processing-time c1 p13) 2)
		(= (processing-cost c1 p14) 3)
		(= (processing-time c1 p14) 1)
		(= (processing-cost c1 p15) 22)
		(= (processing-time c1 p15) 3)
		(= (processing-cost c1 p28) 11)
		(= (processing-time c1 p28) 3)
		(= (processing-cost c1 p29) 12)
		(= (processing-time c1 p29) 2)
		(= (processing-cost c1 p30) 11)
		(= (processing-time c1 p30) 1)
		(= (processing-cost c1 p31) 9)
		(= (processing-time c1 p31) 2)
		(= (processing-cost c1 p44) 5)
		(= (processing-time c1 p44) 1)
		(= (processing-cost c1 p45) 11)
		(= (processing-time c1 p45) 2)
		(= (processing-cost c1 p46) 3)
		(= (processing-time c1 p46) 2)
		(= (processing-cost c1 p47) 10)
		(= (processing-time c1 p47) 2)
		(= (processing-cost c1 p48) 16)
		(= (processing-time c1 p48) 4)
		(= (processing-cost c1 p49) 11)
		(= (processing-time c1 p49) 1)
		(= (processing-cost c1 p50) 8)
		(= (processing-time c1 p50) 1)
		(= (processing-cost c1 p51) 14)
		(= (processing-time c1 p51) 2)
		(= (processing-cost c1 p64) 7)
		(= (processing-time c1 p64) 1)
		(= (processing-cost c1 p65) 8)
		(= (processing-time c1 p65) 1)
		(= (processing-cost c1 p66) 6)
		(= (processing-time c1 p66) 2)
		(= (processing-cost c1 p67) 7)
		(= (processing-time c1 p67) 3)
		(= (processing-cost c1 p72) 10)
		(= (processing-time c1 p72) 4)
		(= (processing-cost c1 p73) 13)
		(= (processing-time c1 p73) 1)
		(= (processing-cost c1 p74) 9)
		(= (processing-time c1 p74) 2)
		(= (processing-cost c1 p75) 12)
		(= (processing-time c1 p75) 2)
		(= (processing-cost c1 p80) 21)
		(= (processing-time c1 p80) 6)
		(= (processing-cost c1 p81) 15)
		(= (processing-time c1 p81) 6)
		(= (processing-cost c1 p82) 13)
		(= (processing-time c1 p82) 1)
		(= (processing-cost c1 p83) 15)
		(= (processing-time c1 p83) 2)
		(= (processing-cost c1 p92) 4)
		(= (processing-time c1 p92) 1)
		(= (processing-cost c1 p93) 6)
		(= (processing-time c1 p93) 2)
		(= (processing-cost c1 p94) 18)
		(= (processing-time c1 p94) 4)
		(= (processing-cost c1 p95) 2)
		(= (processing-time c1 p95) 2)
		(= (processing-cost c1 p96) 20)
		(= (processing-time c1 p96) 5)
		(= (processing-cost c1 p97) 8)
		(= (processing-time c1 p97) 1)
		(= (processing-cost c1 p98) 4)
		(= (processing-time c1 p98) 1)
		(= (processing-cost c1 p99) 8)
		(= (processing-time c1 p99) 2)
		(= (processing-cost c1 p100) 16)
		(= (processing-time c1 p100) 2)
		(= (processing-cost c1 p101) 11)
		(= (processing-time c1 p101) 2)
		(= (processing-cost c1 p102) 10)
		(= (processing-time c1 p102) 2)
		(= (processing-cost c1 p103) 16)
		(= (processing-time c1 p103) 3)
		(= (processing-cost c1 p108) 9)
		(= (processing-time c1 p108) 1)
		(= (processing-cost c1 p109) 7)
		(= (processing-time c1 p109) 1)
		(= (processing-cost c1 p110) 9)
		(= (processing-time c1 p110) 1)
		(= (processing-cost c1 p111) 6)
		(= (processing-time c1 p111) 2)
		(= (processing-cost c1 p116) 9)
		(= (processing-time c1 p116) 4)
		(= (processing-cost c1 p117) 6)
		(= (processing-time c1 p117) 1)
		(= (processing-cost c1 p118) 5)
		(= (processing-time c1 p118) 1)
		(= (processing-cost c1 p119) 5)
		(= (processing-time c1 p119) 2)
		(= (processing-cost c1 p128) 4)
		(= (processing-time c1 p128) 2)
		(= (processing-cost c1 p129) 7)
		(= (processing-time c1 p129) 1)
		(= (processing-cost c1 p130) 4)
		(= (processing-time c1 p130) 1)
		(= (processing-cost c1 p131) 10)
		(= (processing-time c1 p131) 2)
		(= (processing-cost c1 p156) 1)
		(= (processing-time c1 p156) 1)
		(= (processing-cost c1 p157) 10)
		(= (processing-time c1 p157) 6)
		(= (processing-cost c1 p158) 7)
		(= (processing-time c1 p158) 1)
		(= (processing-cost c1 p159) 6)
		(= (processing-time c1 p159) 2)
		(= (processing-cost c1 p160) 8)
		(= (processing-time c1 p160) 2)
		(= (processing-cost c1 p161) 13)
		(= (processing-time c1 p161) 3)
		(= (processing-cost c1 p162) 13)
		(= (processing-time c1 p162) 2)
		(= (processing-cost c1 p163) 12)
		(= (processing-time c1 p163) 3)
		(= (processing-cost c1 p172) 5)
		(= (processing-time c1 p172) 1)
		(= (processing-cost c1 p173) 7)
		(= (processing-time c1 p173) 1)
		(= (processing-cost c1 p174) 4)
		(= (processing-time c1 p174) 1)
		(= (processing-cost c1 p175) 12)
		(= (processing-time c1 p175) 2)
		(= (processing-cost c1 p188) 14)
		(= (processing-time c1 p188) 3)
		(= (processing-cost c1 p189) 12)
		(= (processing-time c1 p189) 2)
		(= (processing-cost c1 p190) 11)
		(= (processing-time c1 p190) 1)
		(= (processing-cost c1 p191) 4)
		(= (processing-time c1 p191) 2)
		(= (processing-cost c1 p196) 10)
		(= (processing-time c1 p196) 1)
		(= (processing-cost c1 p197) 4)
		(= (processing-time c1 p197) 1)
		(= (processing-cost c1 p198) 7)
		(= (processing-time c1 p198) 1)
		(= (processing-cost c1 p199) 12)
		(= (processing-time c1 p199) 2)
		(= (processing-cost c2 p0) 12)
		(= (processing-time c2 p0) 1)
		(= (processing-cost c2 p1) 11)
		(= (processing-time c2 p1) 3)
		(= (processing-cost c2 p2) 4)
		(= (processing-time c2 p2) 3)
		(= (processing-cost c2 p3) 16)
		(= (processing-time c2 p3) 4)
		(= (processing-cost c2 p4) 13)
		(= (processing-time c2 p4) 1)
		(= (processing-cost c2 p5) 11)
		(= (processing-time c2 p5) 1)
		(= (processing-cost c2 p6) 8)
		(= (processing-time c2 p6) 1)
		(= (processing-cost c2 p7) 10)
		(= (processing-time c2 p7) 3)
		(= (processing-cost c2 p8) 21)
		(= (processing-time c2 p8) 5)
		(= (processing-cost c2 p9) 3)
		(= (processing-time c2 p9) 1)
		(= (processing-cost c2 p10) 11)
		(= (processing-time c2 p10) 1)
		(= (processing-cost c2 p11) 7)
		(= (processing-time c2 p11) 2)
		(= (processing-cost c2 p32) 14)
		(= (processing-time c2 p32) 2)
		(= (processing-cost c2 p33) 6)
		(= (processing-time c2 p33) 1)
		(= (processing-cost c2 p34) 3)
		(= (processing-time c2 p34) 1)
		(= (processing-cost c2 p35) 13)
		(= (processing-time c2 p35) 2)
		(= (processing-cost c2 p52) 9)
		(= (processing-time c2 p52) 1)
		(= (processing-cost c2 p53) 3)
		(= (processing-time c2 p53) 1)
		(= (processing-cost c2 p54) 10)
		(= (processing-time c2 p54) 1)
		(= (processing-cost c2 p55) 17)
		(= (processing-time c2 p55) 2)
		(= (processing-cost c2 p88) 10)
		(= (processing-time c2 p88) 6)
		(= (processing-cost c2 p89) 6)
		(= (processing-time c2 p89) 3)
		(= (processing-cost c2 p90) 9)
		(= (processing-time c2 p90) 1)
		(= (processing-cost c2 p91) 13)
		(= (processing-time c2 p91) 3)
		(= (processing-cost c2 p104) 20)
		(= (processing-time c2 p104) 5)
		(= (processing-cost c2 p105) 13)
		(= (processing-time c2 p105) 2)
		(= (processing-cost c2 p106) 8)
		(= (processing-time c2 p106) 1)
		(= (processing-cost c2 p107) 18)
		(= (processing-time c2 p107) 2)
		(= (processing-cost c2 p108) 8)
		(= (processing-time c2 p108) 2)
		(= (processing-cost c2 p109) 14)
		(= (processing-time c2 p109) 2)
		(= (processing-cost c2 p110) 7)
		(= (processing-time c2 p110) 2)
		(= (processing-cost c2 p111) 12)
		(= (processing-time c2 p111) 2)
		(= (processing-cost c2 p112) 4)
		(= (processing-time c2 p112) 1)
		(= (processing-cost c2 p113) 11)
		(= (processing-time c2 p113) 1)
		(= (processing-cost c2 p114) 6)
		(= (processing-time c2 p114) 1)
		(= (processing-cost c2 p115) 12)
		(= (processing-time c2 p115) 2)
		(= (processing-cost c2 p120) 7)
		(= (processing-time c2 p120) 2)
		(= (processing-cost c2 p121) 11)
		(= (processing-time c2 p121) 1)
		(= (processing-cost c2 p122) 11)
		(= (processing-time c2 p122) 1)
		(= (processing-cost c2 p123) 9)
		(= (processing-time c2 p123) 2)
		(= (processing-cost c2 p124) 9)
		(= (processing-time c2 p124) 1)
		(= (processing-cost c2 p125) 16)
		(= (processing-time c2 p125) 4)
		(= (processing-cost c2 p126) 10)
		(= (processing-time c2 p126) 1)
		(= (processing-cost c2 p127) 9)
		(= (processing-time c2 p127) 2)
		(= (processing-cost c2 p140) 30)
		(= (processing-time c2 p140) 7)
		(= (processing-cost c2 p141) 16)
		(= (processing-time c2 p141) 2)
		(= (processing-cost c2 p142) 5)
		(= (processing-time c2 p142) 1)
		(= (processing-cost c2 p143) 10)
		(= (processing-time c2 p143) 3)
		(= (processing-cost c2 p144) 14)
		(= (processing-time c2 p144) 2)
		(= (processing-cost c2 p145) 13)
		(= (processing-time c2 p145) 3)
		(= (processing-cost c2 p146) 4)
		(= (processing-time c2 p146) 1)
		(= (processing-cost c2 p147) 6)
		(= (processing-time c2 p147) 2)
		(= (processing-cost c2 p148) 21)
		(= (processing-time c2 p148) 3)
		(= (processing-cost c2 p149) 7)
		(= (processing-time c2 p149) 1)
		(= (processing-cost c2 p150) 6)
		(= (processing-time c2 p150) 1)
		(= (processing-cost c2 p151) 9)
		(= (processing-time c2 p151) 2)
		(= (processing-cost c2 p152) 17)
		(= (processing-time c2 p152) 3)
		(= (processing-cost c2 p153) 19)
		(= (processing-time c2 p153) 5)
		(= (processing-cost c2 p154) 17)
		(= (processing-time c2 p154) 3)
		(= (processing-cost c2 p155) 13)
		(= (processing-time c2 p155) 3)
		(= (processing-cost c2 p164) 10)
		(= (processing-time c2 p164) 4)
		(= (processing-cost c2 p165) 6)
		(= (processing-time c2 p165) 1)
		(= (processing-cost c2 p166) 4)
		(= (processing-time c2 p166) 1)
		(= (processing-cost c2 p167) 8)
		(= (processing-time c2 p167) 2)
		(= (processing-cost c2 p184) 4)
		(= (processing-time c2 p184) 1)
		(= (processing-cost c2 p185) 7)
		(= (processing-time c2 p185) 1)
		(= (processing-cost c2 p186) 3)
		(= (processing-time c2 p186) 1)
		(= (processing-cost c2 p187) 15)
		(= (processing-time c2 p187) 2)
		(= (processing-cost c2 p192) 9)
		(= (processing-time c2 p192) 3)
		(= (processing-cost c2 p193) 18)
		(= (processing-time c2 p193) 4)
		(= (processing-cost c2 p194) 11)
		(= (processing-time c2 p194) 1)
		(= (processing-cost c2 p195) 9)
		(= (processing-time c2 p195) 2)
		(= (processing-cost c3 p4) 8)
		(= (processing-time c3 p4) 2)
		(= (processing-cost c3 p5) 12)
		(= (processing-time c3 p5) 1)
		(= (processing-cost c3 p6) 10)
		(= (processing-time c3 p6) 1)
		(= (processing-cost c3 p7) 15)
		(= (processing-time c3 p7) 2)
		(= (processing-cost c3 p8) 44)
		(= (processing-time c3 p8) 9)
		(= (processing-cost c3 p9) 13)
		(= (processing-time c3 p9) 1)
		(= (processing-cost c3 p10) 10)
		(= (processing-time c3 p10) 1)
		(= (processing-cost c3 p11) 10)
		(= (processing-time c3 p11) 2)
		(= (processing-cost c3 p16) 5)
		(= (processing-time c3 p16) 1)
		(= (processing-cost c3 p17) 11)
		(= (processing-time c3 p17) 1)
		(= (processing-cost c3 p18) 12)
		(= (processing-time c3 p18) 1)
		(= (processing-cost c3 p19) 12)
		(= (processing-time c3 p19) 2)
		(= (processing-cost c3 p20) 7)
		(= (processing-time c3 p20) 2)
		(= (processing-cost c3 p21) 8)
		(= (processing-time c3 p21) 1)
		(= (processing-cost c3 p22) 14)
		(= (processing-time c3 p22) 4)
		(= (processing-cost c3 p23) 8)
		(= (processing-time c3 p23) 2)
		(= (processing-cost c3 p36) 6)
		(= (processing-time c3 p36) 2)
		(= (processing-cost c3 p37) 11)
		(= (processing-time c3 p37) 1)
		(= (processing-cost c3 p38) 7)
		(= (processing-time c3 p38) 1)
		(= (processing-cost c3 p39) 4)
		(= (processing-time c3 p39) 2)
		(= (processing-cost c3 p60) 10)
		(= (processing-time c3 p60) 1)
		(= (processing-cost c3 p61) 4)
		(= (processing-time c3 p61) 1)
		(= (processing-cost c3 p62) 1)
		(= (processing-time c3 p62) 1)
		(= (processing-cost c3 p63) 12)
		(= (processing-time c3 p63) 2)
		(= (processing-cost c3 p68) 5)
		(= (processing-time c3 p68) 1)
		(= (processing-cost c3 p69) 3)
		(= (processing-time c3 p69) 1)
		(= (processing-cost c3 p70) 12)
		(= (processing-time c3 p70) 1)
		(= (processing-cost c3 p71) 11)
		(= (processing-time c3 p71) 2)
		(= (processing-cost c3 p72) 27)
		(= (processing-time c3 p72) 5)
		(= (processing-cost c3 p73) 8)
		(= (processing-time c3 p73) 1)
		(= (processing-cost c3 p74) 1)
		(= (processing-time c3 p74) 1)
		(= (processing-cost c3 p75) 9)
		(= (processing-time c3 p75) 2)
		(= (processing-cost c3 p84) 33)
		(= (processing-time c3 p84) 8)
		(= (processing-cost c3 p85) 13)
		(= (processing-time c3 p85) 1)
		(= (processing-cost c3 p86) 4)
		(= (processing-time c3 p86) 1)
		(= (processing-cost c3 p87) 10)
		(= (processing-time c3 p87) 2)
		(= (processing-cost c3 p88) 5)
		(= (processing-time c3 p88) 1)
		(= (processing-cost c3 p89) 12)
		(= (processing-time c3 p89) 3)
		(= (processing-cost c3 p90) 9)
		(= (processing-time c3 p90) 4)
		(= (processing-cost c3 p91) 4)
		(= (processing-time c3 p91) 4)
		(= (processing-cost c3 p92) 11)
		(= (processing-time c3 p92) 1)
		(= (processing-cost c3 p93) 8)
		(= (processing-time c3 p93) 1)
		(= (processing-cost c3 p94) 12)
		(= (processing-time c3 p94) 1)
		(= (processing-cost c3 p95) 11)
		(= (processing-time c3 p95) 2)
		(= (processing-cost c3 p168) 9)
		(= (processing-time c3 p168) 3)
		(= (processing-cost c3 p169) 11)
		(= (processing-time c3 p169) 1)
		(= (processing-cost c3 p170) 7)
		(= (processing-time c3 p170) 1)
		(= (processing-cost c3 p171) 10)
		(= (processing-time c3 p171) 4)
		(= (processing-cost c3 p180) 12)
		(= (processing-time c3 p180) 2)
		(= (processing-cost c3 p181) 11)
		(= (processing-time c3 p181) 1)
		(= (processing-cost c3 p182) 2)
		(= (processing-time c3 p182) 1)
		(= (processing-cost c3 p183) 16)
		(= (processing-time c3 p183) 2)
		(= (processing-cost c3 p184) 16)
		(= (processing-time c3 p184) 2)
		(= (processing-cost c3 p185) 11)
		(= (processing-time c3 p185) 1)
		(= (processing-cost c3 p186) 6)
		(= (processing-time c3 p186) 1)
		(= (processing-cost c3 p187) 12)
		(= (processing-time c3 p187) 3)


		(not-depends p0)

		(depends-one p1 p2)

		(not-depends p2)

		(not-depends p3)

		(not-depends p4)

		(not-depends p5)

		(depends-one p6 p5)

		(not-depends p7)

		(not-depends p8)

		(not-depends p9)

		(depends-one p10 p11)

		(depends-one p11 p9)

		(not-depends p12)

		(not-depends p13)

		(not-depends p14)

		(not-depends p15)

		(not-depends p16)

		(not-depends p17)

		(not-depends p18)

		(depends-one p19 p17)

		(not-depends p20)

		(not-depends p21)

		(not-depends p22)

		(not-depends p23)

		(depends-one p24 p27)

		(not-depends p25)

		(not-depends p26)

		(not-depends p27)

		(not-depends p28)

		(not-depends p29)

		(depends-one p30 p31)

		(not-depends p31)

		(not-depends p32)

		(depends-one p33 p35)

		(depends-one p34 p35)

		(not-depends p35)

		(not-depends p36)

		(not-depends p37)

		(depends-one p38 p37)

		(depends-one p39 p37)

		(not-depends p40)

		(not-depends p41)

		(not-depends p42)

		(not-depends p43)

		(not-depends p44)

		(not-depends p45)

		(depends-one p46 p45)

		(not-depends p47)

		(depends-one p48 p51)

		(depends-one p49 p50)

		(not-depends p50)

		(not-depends p51)

		(not-depends p52)

		(not-depends p53)

		(depends-one p54 p55)

		(not-depends p55)

		(not-depends p56)

		(not-depends p57)

		(not-depends p58)

		(depends-one p59 p57)

		(not-depends p60)

		(not-depends p61)

		(not-depends p62)

		(not-depends p63)

		(not-depends p64)

		(depends-one p65 p67)

		(depends-one p66 p65)

		(depends-one p67 p64)

		(not-depends p68)

		(not-depends p69)

		(not-depends p70)

		(not-depends p71)

		(not-depends p72)

		(not-depends p73)

		(depends-one p74 p72)

		(not-depends p75)

		(not-depends p76)

		(not-depends p77)

		(not-depends p78)

		(not-depends p79)

		(not-depends p80)

		(not-depends p81)

		(not-depends p82)

		(depends-one p83 p80)

		(not-depends p84)

		(depends-one p85 p86)

		(not-depends p86)

		(depends-one p87 p85)

		(not-depends p88)

		(not-depends p89)

		(not-depends p90)

		(not-depends p91)

		(depends-one p92 p94)

		(depends-one p93 p95)

		(not-depends p94)

		(not-depends p95)

		(depends-one p96 p98)

		(depends-one p97 p98)

		(not-depends p98)

		(not-depends p99)

		(depends-one p100 p102)

		(not-depends p101)

		(not-depends p102)

		(not-depends p103)

		(depends-one p104 p105)

		(not-depends p105)

		(depends-one p106 p107)

		(not-depends p107)

		(not-depends p108)

		(depends-one p109 p108)

		(not-depends p110)

		(not-depends p111)

		(not-depends p112)

		(not-depends p113)

		(not-depends p114)

		(not-depends p115)

		(not-depends p116)

		(not-depends p117)

		(not-depends p118)

		(not-depends p119)

		(not-depends p120)

		(depends-one p121 p123)

		(not-depends p122)

		(not-depends p123)

		(depends-one p124 p126)

		(depends-one p125 p127)

		(depends-one p126 p127)

		(not-depends p127)

		(not-depends p128)

		(not-depends p129)

		(not-depends p130)

		(depends-one p131 p129)

		(not-depends p132)

		(not-depends p133)

		(not-depends p134)

		(not-depends p135)

		(not-depends p136)

		(not-depends p137)

		(not-depends p138)

		(not-depends p139)

		(not-depends p140)

		(not-depends p141)

		(not-depends p142)

		(not-depends p143)

		(depends-one p144 p147)

		(not-depends p145)

		(not-depends p146)

		(not-depends p147)

		(not-depends p148)

		(not-depends p149)

		(not-depends p150)

		(not-depends p151)

		(depends-one p152 p155)

		(not-depends p153)

		(depends-one p154 p155)

		(not-depends p155)

		(not-depends p156)

		(not-depends p157)

		(not-depends p158)

		(not-depends p159)

		(not-depends p160)

		(not-depends p161)

		(depends-one p162 p161)

		(not-depends p163)

		(depends-one p164 p167)

		(not-depends p165)

		(not-depends p166)

		(not-depends p167)

		(not-depends p168)

		(not-depends p169)

		(not-depends p170)

		(not-depends p171)

		(not-depends p172)

		(depends-one p173 p175)

		(not-depends p174)

		(not-depends p175)

		(not-depends p176)

		(not-depends p177)

		(not-depends p178)

		(not-depends p179)

		(not-depends p180)

		(not-depends p181)

		(not-depends p182)

		(not-depends p183)

		(not-depends p184)

		(not-depends p185)

		(not-depends p186)

		(not-depends p187)

		(not-depends p188)

		(not-depends p189)

		(not-depends p190)

		(not-depends p191)

		(depends-one p192 p195)

		(not-depends p193)

		(depends-one p194 p193)

		(not-depends p195)

		(depends-one p196 p198)

		(not-depends p197)

		(not-depends p198)

		(not-depends p199)

		(not-completed p0)

		(not-completed p1)

		(not-completed p2)

		(not-completed p3)

		(not-completed p4)

		(not-completed p5)

		(not-completed p6)

		(not-completed p7)

		(not-completed p8)

		(not-completed p9)

		(not-completed p10)

		(not-completed p11)

		(not-completed p12)

		(not-completed p13)

		(not-completed p14)

		(not-completed p15)

		(not-completed p16)

		(not-completed p17)

		(not-completed p18)

		(not-completed p19)

		(not-completed p20)

		(not-completed p21)

		(not-completed p22)

		(not-completed p23)

		(not-completed p24)

		(not-completed p25)

		(not-completed p26)

		(not-completed p27)

		(not-completed p28)

		(not-completed p29)

		(not-completed p30)

		(not-completed p31)

		(not-completed p32)

		(not-completed p33)

		(not-completed p34)

		(not-completed p35)

		(not-completed p36)

		(not-completed p37)

		(not-completed p38)

		(not-completed p39)

		(not-completed p40)

		(not-completed p41)

		(not-completed p42)

		(not-completed p43)

		(not-completed p44)

		(not-completed p45)

		(not-completed p46)

		(not-completed p47)

		(not-completed p48)

		(not-completed p49)

		(not-completed p50)

		(not-completed p51)

		(not-completed p52)

		(not-completed p53)

		(not-completed p54)

		(not-completed p55)

		(not-completed p56)

		(not-completed p57)

		(not-completed p58)

		(not-completed p59)

		(not-completed p60)

		(not-completed p61)

		(not-completed p62)

		(not-completed p63)

		(not-completed p64)

		(not-completed p65)

		(not-completed p66)

		(not-completed p67)

		(not-completed p68)

		(not-completed p69)

		(not-completed p70)

		(not-completed p71)

		(not-completed p72)

		(not-completed p73)

		(not-completed p74)

		(not-completed p75)

		(not-completed p76)

		(not-completed p77)

		(not-completed p78)

		(not-completed p79)

		(not-completed p80)

		(not-completed p81)

		(not-completed p82)

		(not-completed p83)

		(not-completed p84)

		(not-completed p85)

		(not-completed p86)

		(not-completed p87)

		(not-completed p88)

		(not-completed p89)

		(not-completed p90)

		(not-completed p91)

		(not-completed p92)

		(not-completed p93)

		(not-completed p94)

		(not-completed p95)

		(not-completed p96)

		(not-completed p97)

		(not-completed p98)

		(not-completed p99)

		(not-completed p100)

		(not-completed p101)

		(not-completed p102)

		(not-completed p103)

		(not-completed p104)

		(not-completed p105)

		(not-completed p106)

		(not-completed p107)

		(not-completed p108)

		(not-completed p109)

		(not-completed p110)

		(not-completed p111)

		(not-completed p112)

		(not-completed p113)

		(not-completed p114)

		(not-completed p115)

		(not-completed p116)

		(not-completed p117)

		(not-completed p118)

		(not-completed p119)

		(not-completed p120)

		(not-completed p121)

		(not-completed p122)

		(not-completed p123)

		(not-completed p124)

		(not-completed p125)

		(not-completed p126)

		(not-completed p127)

		(not-completed p128)

		(not-completed p129)

		(not-completed p130)

		(not-completed p131)

		(not-completed p132)

		(not-completed p133)

		(not-completed p134)

		(not-completed p135)

		(not-completed p136)

		(not-completed p137)

		(not-completed p138)

		(not-completed p139)

		(not-completed p140)

		(not-completed p141)

		(not-completed p142)

		(not-completed p143)

		(not-completed p144)

		(not-completed p145)

		(not-completed p146)

		(not-completed p147)

		(not-completed p148)

		(not-completed p149)

		(not-completed p150)

		(not-completed p151)

		(not-completed p152)

		(not-completed p153)

		(not-completed p154)

		(not-completed p155)

		(not-completed p156)

		(not-completed p157)

		(not-completed p158)

		(not-completed p159)

		(not-completed p160)

		(not-completed p161)

		(not-completed p162)

		(not-completed p163)

		(not-completed p164)

		(not-completed p165)

		(not-completed p166)

		(not-completed p167)

		(not-completed p168)

		(not-completed p169)

		(not-completed p170)

		(not-completed p171)

		(not-completed p172)

		(not-completed p173)

		(not-completed p174)

		(not-completed p175)

		(not-completed p176)

		(not-completed p177)

		(not-completed p178)

		(not-completed p179)

		(not-completed p180)

		(not-completed p181)

		(not-completed p182)

		(not-completed p183)

		(not-completed p184)

		(not-completed p185)

		(not-completed p186)

		(not-completed p187)

		(not-completed p188)

		(not-completed p189)

		(not-completed p190)

		(not-completed p191)

		(not-completed p192)

		(not-completed p193)

		(not-completed p194)

		(not-completed p195)

		(not-completed p196)

		(not-completed p197)

		(not-completed p198)

		(not-completed p199)


	)

	(:goal
		(and
			(completed p0)
			(completed p1)
			(completed p2)
			(completed p3)
			(completed p4)
			(completed p5)
			(completed p6)
			(completed p7)
			(completed p8)
			(completed p9)
			(completed p10)
			(completed p11)
			(completed p12)
			(completed p13)
			(completed p14)
			(completed p15)
			(completed p16)
			(completed p17)
			(completed p18)
			(completed p19)
			(completed p20)
			(completed p21)
			(completed p22)
			(completed p23)
			(completed p24)
			(completed p25)
			(completed p26)
			(completed p27)
			(completed p28)
			(completed p29)
			(completed p30)
			(completed p31)
			(completed p32)
			(completed p33)
			(completed p34)
			(completed p35)
			(completed p36)
			(completed p37)
			(completed p38)
			(completed p39)
			(completed p40)
			(completed p41)
			(completed p42)
			(completed p43)
			(completed p44)
			(completed p45)
			(completed p46)
			(completed p47)
			(completed p48)
			(completed p49)
			(completed p50)
			(completed p51)
			(completed p52)
			(completed p53)
			(completed p54)
			(completed p55)
			(completed p56)
			(completed p57)
			(completed p58)
			(completed p59)
			(completed p60)
			(completed p61)
			(completed p62)
			(completed p63)
			(completed p64)
			(completed p65)
			(completed p66)
			(completed p67)
			(completed p68)
			(completed p69)
			(completed p70)
			(completed p71)
			(completed p72)
			(completed p73)
			(completed p74)
			(completed p75)
			(completed p76)
			(completed p77)
			(completed p78)
			(completed p79)
			(completed p80)
			(completed p81)
			(completed p82)
			(completed p83)
			(completed p84)
			(completed p85)
			(completed p86)
			(completed p87)
			(completed p88)
			(completed p89)
			(completed p90)
			(completed p91)
			(completed p92)
			(completed p93)
			(completed p94)
			(completed p95)
			(completed p96)
			(completed p97)
			(completed p98)
			(completed p99)
			(completed p100)
			(completed p101)
			(completed p102)
			(completed p103)
			(completed p104)
			(completed p105)
			(completed p106)
			(completed p107)
			(completed p108)
			(completed p109)
			(completed p110)
			(completed p111)
			(completed p112)
			(completed p113)
			(completed p114)
			(completed p115)
			(completed p116)
			(completed p117)
			(completed p118)
			(completed p119)
			(completed p120)
			(completed p121)
			(completed p122)
			(completed p123)
			(completed p124)
			(completed p125)
			(completed p126)
			(completed p127)
			(completed p128)
			(completed p129)
			(completed p130)
			(completed p131)
			(completed p132)
			(completed p133)
			(completed p134)
			(completed p135)
			(completed p136)
			(completed p137)
			(completed p138)
			(completed p139)
			(completed p140)
			(completed p141)
			(completed p142)
			(completed p143)
			(completed p144)
			(completed p145)
			(completed p146)
			(completed p147)
			(completed p148)
			(completed p149)
			(completed p150)
			(completed p151)
			(completed p152)
			(completed p153)
			(completed p154)
			(completed p155)
			(completed p156)
			(completed p157)
			(completed p158)
			(completed p159)
			(completed p160)
			(completed p161)
			(completed p162)
			(completed p163)
			(completed p164)
			(completed p165)
			(completed p166)
			(completed p167)
			(completed p168)
			(completed p169)
			(completed p170)
			(completed p171)
			(completed p172)
			(completed p173)
			(completed p174)
			(completed p175)
			(completed p176)
			(completed p177)
			(completed p178)
			(completed p179)
			(completed p180)
			(completed p181)
			(completed p182)
			(completed p183)
			(completed p184)
			(completed p185)
			(completed p186)
			(completed p187)
			(completed p188)
			(completed p189)
			(completed p190)
			(completed p191)
			(completed p192)
			(completed p193)
			(completed p194)
			(completed p195)
			(completed p196)
			(completed p197)
			(completed p198)
			(completed p199)
		)
	)

	(:metric minimize (total-time))		; optimal cost = 1035

)