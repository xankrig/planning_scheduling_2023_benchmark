(define 
    
    (domain tube-factory)

    (:requirements :typing :equality :durative-actions :fluents :action-costs :ps-task :ps-task-require)

    (:types
        ; task to be processed
        task - object
        resource - object
        attribute - object
        machine server - resource
	semi-machine auto-machine - machine
        
    )

    (:attributes
         ;static attribute
        (setupable ?m - machine ?s - server)
        (setuped ?m - machine ?t - task)
        
        
    )
    (:static
        (requires-manual ?t - task)
	    (requires-semi ?t - task)
	    (requires-auto ?t - task)
    )

    (:predicates   
	(processed ?t - task)       
        ; task ?t is already processed
        (not-processed ?t - task)	

    )
    
    (:functions

        (setup-time ?s - server ?m - machine)
	(setup-cost ?s - server ?m - machine)

        ; time in which the task ?t is processed by the machine
        ; configuration ?c
        (processing-time ?m - machine ?t - task)
	(processing-cost ?m - machine ?t - task)

        (processing-time-server ?s - server ?t - task)
	(processing-cost-server ?s - server ?t - task)

        
        
    )
       
    (:maintenance-activity setup_machine
        :parameters
        (
            ?t1 ?t2 - task
            ?s - server
        )  
        :resource    
        (
            ?m - machine
        )
        :attributes
        (and
            (setuped ?m ?t1)
        )
	:duration (= ?duration (setup-time ?s ?m))
	:cost (= ?cost (setup-cost ?s ?m))
        :static
        (and
            (setupable ?m ?s)
        )
        :rem-effect
        (and
	    (setuped ?m ?t1) 
	)
        :add-effect
        (and
            (setuped ?m ?t2)         
	)
    )
 
    (:production-activity execute_task_manual 
        :parameters
        (
            ?t - task
        )
        :attributes
        (and 
            (for ?s - server) ()    
        )
	:duration (= ?duration (processing-time-server ?s ?t))
	:cost (= ?cost (processing-cost-server ?s ?t))
	:static
        (and
            (requires-manual ?t)
        )
        :precondition
        (and
            (not-processed ?t)  
        )
        :del-effect
        (and
            (not-processed ?t)
        )
        :add-effect
        (and
	    (processed ?t)
        )
    )

    (:production-activity execute_task_semi
        :parameters
        (
            ?t - task
        )
        :attributes
        (and
            (for ?s - server) ()
            (for ?m - semi-machine) (and (setuped ?m ?t)) ;static attributes 
        )
	:duration (= ?duration (processing-time ?m ?t))
	:cost (= ?cost (processing-cost ?m ?t))
	:static
        (and
            (requires-semi ?t)
        )
        :precondition
        (and
            (not-processed ?t)
	)
        :del-effect
        (and
	    (not-processed ?t)
        )
        :add-effect
        (and
            (processed ?t)
        )
    )

    (:production-activity execute_task_auto
        :parameters
        (
            ?t - task
        )
        :attributes
        (and        
            (for ?m - auto-machine) (and (setuped ?m ?t)) ;static attributes 
        )
	:duration (= ?duration (processing-time ?m ?t))
	:cost (= ?cost (processing-cost ?m ?t))
	:static 
        (and
            (requires-auto ?t)
        )
        :precondition
        (and
            (not-processed ?t)
        )
        :del-effect
        (and
	    (not-processed ?t)
        )
        :add-effect
        (and
            (processed ?t)
        )
    )
)
