(define 

    (domain tube-factory)

    (:requirements :strips :typing :conditional-effects :negative-preconditions :equality)
    (:types
        task - object
        resource - object
        attribute - object
        machine server - resource
	semi-machine auto-machine - machine
    )
    (:predicates
	    (processed ?t - task)
	    (not-processed ?t - task)
	    (setupable ?m - machine ?s - server)
	    (setuped ?m - machine ?t - task)
	    (requires-manual ?t - task)
	    (requires-semi ?t - task)
	    (requires-auto ?t - task)
    )
    (:functions
	    (setup-cost ?s - server ?m - machine)
	    (processing-cost ?m - machine ?t - task)
	    (processing-cost-server ?s - server ?t - task)
	    (total-cost)
    )
    (:action setup_machine
	:parameters
	(
	    ?t1 ?t2 - task
	    ?s - server
	    ?m - machine
	)
	:precondition
        (and
	    (setupable ?m ?s)
	    (setuped ?m ?t1)
	)
	:effect
	(and
	    (not (setuped ?m ?t1))
	    (setuped ?m ?t2)
	    (increase (total-cost) (setup-cost ?s ?m))
	)
    )
    (:action execute_task_manual
	:parameters
	(
	    ?t - task
	    ?s - server
	)
	:precondition
        (and
	    (requires-manual ?t)
	    (not-processed ?t)
        )
	:effect
	(and
	    (not (not-processed ?t))
	    (processed ?t)
	    (increase (total-cost) (processing-cost-server ?s ?t))
	)
    )
    (:action execute_task_semi
	:parameters
	(
	    ?t - task
	    ?s - server
	    ?m - semi-machine
	)
	:precondition
        (and
	    (requires-semi ?t)
	    (not-processed ?t)
	    (setuped ?m ?t)
	)
	:effect
	(and
	    (not (not-processed ?t))
	    (processed ?t)
	    (increase (total-cost) (processing-cost ?m ?t))
	)
    )
    (:action execute_task_auto
	:parameters
	(
	    ?t - task
	    ?m - auto-machine
	)
	:precondition
        (and
	    (requires-auto ?t)
	    (not-processed ?t)
	    (setuped ?m ?t)
        )
	:effect
	(and
	    (not (not-processed ?t))
	    (processed ?t)
	    (increase (total-cost) (processing-cost ?m ?t))
	)
    )
)

