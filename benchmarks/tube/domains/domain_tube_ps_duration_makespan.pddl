(define 

    (domain tube-factory)

(:requirements :typing :equality :durative-actions :fluents :action-costs :ps-task-require)
    (:types
        task - object
        resource - object
        attribute - object
        machine server - resource
	semi-machine auto-machine - machine
    )
    (:predicates
	    (processed ?t - task)
	    (not-processed ?t - task)
	    (setupable ?m - machine ?s - server)
	    (setuped ?m - machine ?t - task)
	    (requires-manual ?t - task)
	    (requires-semi ?t - task)
	    (requires-auto ?t - task)
	    (available ?r - resource)
    )
    (:functions
	    (setup-time ?s - server ?m - machine)
	    (processing-time ?m - machine ?t - task)
	    (processing-time-server ?s - server ?t - task)
    )
    (:durative-action setup_machine
	:parameters
	(
	    ?t1 ?t2 - task
	    ?s - server
	    ?m - machine
	)
	:duration (= ?duration (setup-time ?s ?m))
	:condition
        (and
	    (at start (setupable ?m ?s))
	    (at start (setuped ?m ?t1))
	    (at start (available ?s))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (setuped ?m ?t1)))
	    (at start (not (available ?s)))
	    (at start (not (available ?m)))
	    (at end (setuped ?m ?t2))
	    (at end (available ?s))
	    (at end (available ?m))
	)
    )
    (:durative-action execute_task_manual
	:parameters
	(
	    ?t - task
	    ?s - server
	)
	:duration (= ?duration (processing-time-server ?s ?t))
	:condition
        (and
	    (at start (requires-manual ?t))
	    (at start (not-processed ?t))
	    (at start (available ?s))
        )
	:effect
	(and
	    (at start (not (not-processed ?t)))
	    (at start (not (available ?s)))
	    (at end (processed ?t))
	    (at end (available ?s))
	)
    )
    (:durative-action execute_task_semi
	:parameters
	(
	    ?t - task
	    ?s - server
	    ?m - semi-machine
	)
	:duration (= ?duration (processing-time ?m ?t))
	:condition
        (and
	    (at start (requires-semi ?t))
	    (at start (not-processed ?t))
	    (at start (setuped ?m ?t))
	    (at start (available ?s))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (not-processed ?t)))
	    (at start (not (available ?s)))
	    (at start (not (available ?m)))
	    (at end (processed ?t))
	    (at end (available ?s))
	    (at end (available ?m))
	)
    )
    (:durative-action execute_task_auto
	:parameters
	(
	    ?t - task
	    ?m - auto-machine
	)
	:duration (= ?duration (processing-time ?m ?t))
	:condition
        (and
	    (at start (requires-auto ?t))
	    (at start (not-processed ?t))
	    (at start (setuped ?m ?t))
	    (at start (available ?m))
        )
	:effect
	(and
	    (at start (not (not-processed ?t)))
	    (at start (not (available ?m)))
	    (at end (processed ?t))
	    (at end (available ?m))
	)
    )
)

