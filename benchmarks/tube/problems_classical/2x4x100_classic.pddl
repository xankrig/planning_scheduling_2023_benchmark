(define
	(problem tube-factory-instance)
	(:domain tube-factory)
	(:objects
		m0 - auto-machine
		m1 - semi-machine
		s0 s1 s2 s3 - server
		t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25 t26 t27 t28 t29 t30 t31 t32 t33 t34 t35 t36 t37 t38 t39 t40 t41 t42 t43 t44 t45 t46 t47 t48 t49 t50 t51 t52 t53 t54 t55 t56 t57 t58 t59 t60 t61 t62 t63 t64 t65 t66 t67 t68 t69 t70 t71 t72 t73 t74 t75 t76 t77 t78 t79 t80 t81 t82 t83 t84 t85 t86 t87 t88 t89 t90 t91 t92 t93 t94 t95 t96 t97 t98 t99 - task
	)
	(:init
		(setuped m0 t2)
		(setuped m1 t1)
		(not-processed t0)
		(not-processed t1)
		(not-processed t2)
		(not-processed t3)
		(not-processed t4)
		(not-processed t5)
		(not-processed t6)
		(not-processed t7)
		(not-processed t8)
		(not-processed t9)
		(not-processed t10)
		(not-processed t11)
		(not-processed t12)
		(not-processed t13)
		(not-processed t14)
		(not-processed t15)
		(not-processed t16)
		(not-processed t17)
		(not-processed t18)
		(not-processed t19)
		(not-processed t20)
		(not-processed t21)
		(not-processed t22)
		(not-processed t23)
		(not-processed t24)
		(not-processed t25)
		(not-processed t26)
		(not-processed t27)
		(not-processed t28)
		(not-processed t29)
		(not-processed t30)
		(not-processed t31)
		(not-processed t32)
		(not-processed t33)
		(not-processed t34)
		(not-processed t35)
		(not-processed t36)
		(not-processed t37)
		(not-processed t38)
		(not-processed t39)
		(not-processed t40)
		(not-processed t41)
		(not-processed t42)
		(not-processed t43)
		(not-processed t44)
		(not-processed t45)
		(not-processed t46)
		(not-processed t47)
		(not-processed t48)
		(not-processed t49)
		(not-processed t50)
		(not-processed t51)
		(not-processed t52)
		(not-processed t53)
		(not-processed t54)
		(not-processed t55)
		(not-processed t56)
		(not-processed t57)
		(not-processed t58)
		(not-processed t59)
		(not-processed t60)
		(not-processed t61)
		(not-processed t62)
		(not-processed t63)
		(not-processed t64)
		(not-processed t65)
		(not-processed t66)
		(not-processed t67)
		(not-processed t68)
		(not-processed t69)
		(not-processed t70)
		(not-processed t71)
		(not-processed t72)
		(not-processed t73)
		(not-processed t74)
		(not-processed t75)
		(not-processed t76)
		(not-processed t77)
		(not-processed t78)
		(not-processed t79)
		(not-processed t80)
		(not-processed t81)
		(not-processed t82)
		(not-processed t83)
		(not-processed t84)
		(not-processed t85)
		(not-processed t86)
		(not-processed t87)
		(not-processed t88)
		(not-processed t89)
		(not-processed t90)
		(not-processed t91)
		(not-processed t92)
		(not-processed t93)
		(not-processed t94)
		(not-processed t95)
		(not-processed t96)
		(not-processed t97)
		(not-processed t98)
		(not-processed t99)
		(setupable m0 s1)
		(setupable m0 s3)
		(setupable m1 s0)
		(setupable m1 s2)
		(= (setup-cost s0 m1) 14)
		(= (setup-cost s1 m0) 10)
		(= (setup-cost s2 m1) 25)
		(= (setup-cost s3 m0) 12)
		(requires-manual t0)
		(requires-semi t1)
		(requires-auto t2)
		(requires-manual t3)
		(requires-manual t4)
		(requires-manual t5)
		(requires-manual t6)
		(requires-auto t7)
		(requires-semi t8)
		(requires-semi t9)
		(requires-semi t10)
		(requires-semi t11)
		(requires-semi t12)
		(requires-auto t13)
		(requires-auto t14)
		(requires-auto t15)
		(requires-semi t16)
		(requires-semi t17)
		(requires-auto t18)
		(requires-manual t19)
		(requires-semi t20)
		(requires-semi t21)
		(requires-auto t22)
		(requires-semi t23)
		(requires-auto t24)
		(requires-semi t25)
		(requires-auto t26)
		(requires-auto t27)
		(requires-semi t28)
		(requires-semi t29)
		(requires-semi t30)
		(requires-semi t31)
		(requires-manual t32)
		(requires-auto t33)
		(requires-manual t34)
		(requires-manual t35)
		(requires-manual t36)
		(requires-auto t37)
		(requires-auto t38)
		(requires-semi t39)
		(requires-manual t40)
		(requires-auto t41)
		(requires-auto t42)
		(requires-auto t43)
		(requires-auto t44)
		(requires-auto t45)
		(requires-manual t46)
		(requires-auto t47)
		(requires-semi t48)
		(requires-auto t49)
		(requires-manual t50)
		(requires-manual t51)
		(requires-manual t52)
		(requires-semi t53)
		(requires-manual t54)
		(requires-manual t55)
		(requires-semi t56)
		(requires-auto t57)
		(requires-auto t58)
		(requires-auto t59)
		(requires-auto t60)
		(requires-auto t61)
		(requires-manual t62)
		(requires-auto t63)
		(requires-auto t64)
		(requires-manual t65)
		(requires-manual t66)
		(requires-semi t67)
		(requires-auto t68)
		(requires-semi t69)
		(requires-manual t70)
		(requires-manual t71)
		(requires-manual t72)
		(requires-semi t73)
		(requires-auto t74)
		(requires-semi t75)
		(requires-auto t76)
		(requires-manual t77)
		(requires-auto t78)
		(requires-auto t79)
		(requires-auto t80)
		(requires-manual t81)
		(requires-semi t82)
		(requires-auto t83)
		(requires-auto t84)
		(requires-manual t85)
		(requires-semi t86)
		(requires-auto t87)
		(requires-manual t88)
		(requires-semi t89)
		(requires-auto t90)
		(requires-semi t91)
		(requires-auto t92)
		(requires-manual t93)
		(requires-auto t94)
		(requires-auto t95)
		(requires-manual t96)
		(requires-auto t97)
		(requires-auto t98)
		(requires-auto t99)
		(= (processing-cost m0 t2) 22)
		(= (processing-cost m0 t7) 22)
		(= (processing-cost m0 t13) 15)
		(= (processing-cost m0 t14) 3)
		(= (processing-cost m0 t15) 17)
		(= (processing-cost m0 t18) 54)
		(= (processing-cost m0 t22) 10)
		(= (processing-cost m0 t24) 21)
		(= (processing-cost m0 t26) 50)
		(= (processing-cost m0 t27) 8)
		(= (processing-cost m0 t33) 16)
		(= (processing-cost m0 t37) 7)
		(= (processing-cost m0 t38) 31)
		(= (processing-cost m0 t41) 2)
		(= (processing-cost m0 t42) 12)
		(= (processing-cost m0 t43) 17)
		(= (processing-cost m0 t44) 52)
		(= (processing-cost m0 t45) 3)
		(= (processing-cost m0 t47) 25)
		(= (processing-cost m0 t49) 24)
		(= (processing-cost m0 t57) 30)
		(= (processing-cost m0 t58) 25)
		(= (processing-cost m0 t59) 12)
		(= (processing-cost m0 t60) 12)
		(= (processing-cost m0 t61) 26)
		(= (processing-cost m0 t63) 14)
		(= (processing-cost m0 t64) 9)
		(= (processing-cost m0 t68) 38)
		(= (processing-cost m0 t74) 23)
		(= (processing-cost m0 t76) 14)
		(= (processing-cost m0 t78) 38)
		(= (processing-cost m0 t79) 15)
		(= (processing-cost m0 t80) 48)
		(= (processing-cost m0 t83) 9)
		(= (processing-cost m0 t84) 30)
		(= (processing-cost m0 t87) 8)
		(= (processing-cost m0 t90) 25)
		(= (processing-cost m0 t92) 14)
		(= (processing-cost m0 t94) 6)
		(= (processing-cost m0 t95) 16)
		(= (processing-cost m0 t97) 19)
		(= (processing-cost m0 t98) 14)
		(= (processing-cost m0 t99) 29)
		(= (processing-cost m1 t1) 25)
		(= (processing-cost m1 t8) 11)
		(= (processing-cost m1 t9) 40)
		(= (processing-cost m1 t10) 44)
		(= (processing-cost m1 t11) 42)
		(= (processing-cost m1 t12) 14)
		(= (processing-cost m1 t16) 51)
		(= (processing-cost m1 t17) 39)
		(= (processing-cost m1 t20) 35)
		(= (processing-cost m1 t21) 42)
		(= (processing-cost m1 t23) 45)
		(= (processing-cost m1 t25) 32)
		(= (processing-cost m1 t28) 35)
		(= (processing-cost m1 t29) 10)
		(= (processing-cost m1 t30) 12)
		(= (processing-cost m1 t31) 45)
		(= (processing-cost m1 t39) 25)
		(= (processing-cost m1 t48) 34)
		(= (processing-cost m1 t53) 20)
		(= (processing-cost m1 t56) 24)
		(= (processing-cost m1 t67) 36)
		(= (processing-cost m1 t69) 33)
		(= (processing-cost m1 t73) 30)
		(= (processing-cost m1 t75) 49)
		(= (processing-cost m1 t82) 35)
		(= (processing-cost m1 t86) 10)
		(= (processing-cost m1 t89) 24)
		(= (processing-cost m1 t91) 72)
		(= (processing-cost-server s0 t0) 93)
		(= (processing-cost-server s0 t3) 24)
		(= (processing-cost-server s0 t4) 33)
		(= (processing-cost-server s0 t5) 17)
		(= (processing-cost-server s0 t6) 23)
		(= (processing-cost-server s0 t19) 22)
		(= (processing-cost-server s0 t32) 37)
		(= (processing-cost-server s0 t34) 69)
		(= (processing-cost-server s0 t35) 24)
		(= (processing-cost-server s0 t36) 28)
		(= (processing-cost-server s0 t40) 44)
		(= (processing-cost-server s0 t46) 32)
		(= (processing-cost-server s0 t50) 52)
		(= (processing-cost-server s0 t51) 34)
		(= (processing-cost-server s0 t52) 22)
		(= (processing-cost-server s0 t54) 10)
		(= (processing-cost-server s0 t55) 38)
		(= (processing-cost-server s0 t62) 46)
		(= (processing-cost-server s0 t65) 73)
		(= (processing-cost-server s0 t66) 56)
		(= (processing-cost-server s0 t70) 18)
		(= (processing-cost-server s0 t71) 46)
		(= (processing-cost-server s0 t72) 56)
		(= (processing-cost-server s0 t77) 80)
		(= (processing-cost-server s0 t81) 18)
		(= (processing-cost-server s0 t85) 12)
		(= (processing-cost-server s0 t88) 19)
		(= (processing-cost-server s0 t93) 14)
		(= (processing-cost-server s0 t96) 38)
		(= (processing-cost-server s1 t0) 16)
		(= (processing-cost-server s1 t3) 66)
		(= (processing-cost-server s1 t4) 61)
		(= (processing-cost-server s1 t5) 48)
		(= (processing-cost-server s1 t6) 49)
		(= (processing-cost-server s1 t19) 26)
		(= (processing-cost-server s1 t32) 27)
		(= (processing-cost-server s1 t34) 20)
		(= (processing-cost-server s1 t35) 20)
		(= (processing-cost-server s1 t36) 39)
		(= (processing-cost-server s1 t40) 35)
		(= (processing-cost-server s1 t46) 75)
		(= (processing-cost-server s1 t50) 51)
		(= (processing-cost-server s1 t51) 51)
		(= (processing-cost-server s1 t52) 26)
		(= (processing-cost-server s1 t54) 49)
		(= (processing-cost-server s1 t55) 73)
		(= (processing-cost-server s1 t62) 71)
		(= (processing-cost-server s1 t65) 33)
		(= (processing-cost-server s1 t66) 56)
		(= (processing-cost-server s1 t70) 18)
		(= (processing-cost-server s1 t71) 21)
		(= (processing-cost-server s1 t72) 35)
		(= (processing-cost-server s1 t77) 44)
		(= (processing-cost-server s1 t81) 29)
		(= (processing-cost-server s1 t85) 46)
		(= (processing-cost-server s1 t88) 69)
		(= (processing-cost-server s1 t93) 28)
		(= (processing-cost-server s1 t96) 45)
		(= (processing-cost-server s2 t0) 41)
		(= (processing-cost-server s2 t3) 26)
		(= (processing-cost-server s2 t4) 93)
		(= (processing-cost-server s2 t5) 13)
		(= (processing-cost-server s2 t6) 21)
		(= (processing-cost-server s2 t19) 44)
		(= (processing-cost-server s2 t32) 67)
		(= (processing-cost-server s2 t34) 40)
		(= (processing-cost-server s2 t35) 62)
		(= (processing-cost-server s2 t36) 31)
		(= (processing-cost-server s2 t40) 18)
		(= (processing-cost-server s2 t46) 76)
		(= (processing-cost-server s2 t50) 15)
		(= (processing-cost-server s2 t51) 63)
		(= (processing-cost-server s2 t52) 40)
		(= (processing-cost-server s2 t54) 45)
		(= (processing-cost-server s2 t55) 62)
		(= (processing-cost-server s2 t62) 37)
		(= (processing-cost-server s2 t65) 80)
		(= (processing-cost-server s2 t66) 54)
		(= (processing-cost-server s2 t70) 25)
		(= (processing-cost-server s2 t71) 31)
		(= (processing-cost-server s2 t72) 38)
		(= (processing-cost-server s2 t77) 46)
		(= (processing-cost-server s2 t81) 24)
		(= (processing-cost-server s2 t85) 39)
		(= (processing-cost-server s2 t88) 75)
		(= (processing-cost-server s2 t93) 48)
		(= (processing-cost-server s2 t96) 15)
		(= (processing-cost-server s3 t0) 42)
		(= (processing-cost-server s3 t3) 59)
		(= (processing-cost-server s3 t4) 39)
		(= (processing-cost-server s3 t5) 50)
		(= (processing-cost-server s3 t6) 17)
		(= (processing-cost-server s3 t19) 41)
		(= (processing-cost-server s3 t32) 20)
		(= (processing-cost-server s3 t34) 34)
		(= (processing-cost-server s3 t35) 61)
		(= (processing-cost-server s3 t36) 20)
		(= (processing-cost-server s3 t40) 8)
		(= (processing-cost-server s3 t46) 19)
		(= (processing-cost-server s3 t50) 55)
		(= (processing-cost-server s3 t51) 22)
		(= (processing-cost-server s3 t52) 67)
		(= (processing-cost-server s3 t54) 51)
		(= (processing-cost-server s3 t55) 17)
		(= (processing-cost-server s3 t62) 44)
		(= (processing-cost-server s3 t65) 27)
		(= (processing-cost-server s3 t66) 27)
		(= (processing-cost-server s3 t70) 17)
		(= (processing-cost-server s3 t71) 65)
		(= (processing-cost-server s3 t72) 18)
		(= (processing-cost-server s3 t77) 50)
		(= (processing-cost-server s3 t81) 58)
		(= (processing-cost-server s3 t85) 40)
		(= (processing-cost-server s3 t88) 22)
		(= (processing-cost-server s3 t93) 51)
		(= (processing-cost-server s3 t96) 32)
	)
	(:goal
		(and
			(processed t0)
			(processed t1)
			(processed t2)
			(processed t3)
			(processed t4)
			(processed t5)
			(processed t6)
			(processed t7)
			(processed t8)
			(processed t9)
			(processed t10)
			(processed t11)
			(processed t12)
			(processed t13)
			(processed t14)
			(processed t15)
			(processed t16)
			(processed t17)
			(processed t18)
			(processed t19)
			(processed t20)
			(processed t21)
			(processed t22)
			(processed t23)
			(processed t24)
			(processed t25)
			(processed t26)
			(processed t27)
			(processed t28)
			(processed t29)
			(processed t30)
			(processed t31)
			(processed t32)
			(processed t33)
			(processed t34)
			(processed t35)
			(processed t36)
			(processed t37)
			(processed t38)
			(processed t39)
			(processed t40)
			(processed t41)
			(processed t42)
			(processed t43)
			(processed t44)
			(processed t45)
			(processed t46)
			(processed t47)
			(processed t48)
			(processed t49)
			(processed t50)
			(processed t51)
			(processed t52)
			(processed t53)
			(processed t54)
			(processed t55)
			(processed t56)
			(processed t57)
			(processed t58)
			(processed t59)
			(processed t60)
			(processed t61)
			(processed t62)
			(processed t63)
			(processed t64)
			(processed t65)
			(processed t66)
			(processed t67)
			(processed t68)
			(processed t69)
			(processed t70)
			(processed t71)
			(processed t72)
			(processed t73)
			(processed t74)
			(processed t75)
			(processed t76)
			(processed t77)
			(processed t78)
			(processed t79)
			(processed t80)
			(processed t81)
			(processed t82)
			(processed t83)
			(processed t84)
			(processed t85)
			(processed t86)
			(processed t87)
			(processed t88)
			(processed t89)
			(processed t90)
			(processed t91)
			(processed t92)
			(processed t93)
			(processed t94)
			(processed t95)
			(processed t96)
			(processed t97)
			(processed t98)
			(processed t99)
		)
	)
	(:metric minimize (total-cost))
)