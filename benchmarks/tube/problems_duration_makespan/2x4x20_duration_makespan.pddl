(define
	(problem tube-factory-instance)
	(:domain tube-factory)
	(:objects
		m1 - auto-machine
		m0 - semi-machine
		s0 s1 s2 s3 - server
		t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 - task
	)
	(:init
		(available m1)
		(available m0)
		(available s0)
		(available s1)
		(available s2)
		(available s3)
		(setuped m0 t1)
		(setuped m1 t0)
		(not-processed t0)
		(not-processed t1)
		(not-processed t2)
		(not-processed t3)
		(not-processed t4)
		(not-processed t5)
		(not-processed t6)
		(not-processed t7)
		(not-processed t8)
		(not-processed t9)
		(not-processed t10)
		(not-processed t11)
		(not-processed t12)
		(not-processed t13)
		(not-processed t14)
		(not-processed t15)
		(not-processed t16)
		(not-processed t17)
		(not-processed t18)
		(not-processed t19)
		(setupable m0 s1)
		(setupable m0 s2)
		(setupable m0 s3)
		(setupable m1 s0)
		(= (setup-time s0 m1) 2)
		(= (setup-time s1 m0) 6)
		(= (setup-time s2 m0) 6)
		(= (setup-time s3 m0) 6)
		(requires-auto t0)
		(requires-semi t1)
		(requires-manual t2)
		(requires-semi t3)
		(requires-semi t4)
		(requires-manual t5)
		(requires-semi t6)
		(requires-manual t7)
		(requires-manual t8)
		(requires-semi t9)
		(requires-manual t10)
		(requires-semi t11)
		(requires-auto t12)
		(requires-auto t13)
		(requires-manual t14)
		(requires-auto t15)
		(requires-auto t16)
		(requires-manual t17)
		(requires-auto t18)
		(requires-manual t19)
		(= (processing-time m0 t1) 5)
		(= (processing-time m0 t3) 11)
		(= (processing-time m0 t4) 6)
		(= (processing-time m0 t6) 4)
		(= (processing-time m0 t9) 9)
		(= (processing-time m0 t11) 11)
		(= (processing-time m1 t0) 3)
		(= (processing-time m1 t12) 7)
		(= (processing-time m1 t13) 5)
		(= (processing-time m1 t15) 4)
		(= (processing-time m1 t16) 5)
		(= (processing-time m1 t18) 10)
		(= (processing-time-server s0 t2) 16)
		(= (processing-time-server s0 t5) 11)
		(= (processing-time-server s0 t7) 12)
		(= (processing-time-server s0 t8) 15)
		(= (processing-time-server s0 t10) 15)
		(= (processing-time-server s0 t14) 18)
		(= (processing-time-server s0 t17) 12)
		(= (processing-time-server s0 t19) 20)
		(= (processing-time-server s1 t2) 12)
		(= (processing-time-server s1 t5) 14)
		(= (processing-time-server s1 t7) 14)
		(= (processing-time-server s1 t8) 16)
		(= (processing-time-server s1 t10) 11)
		(= (processing-time-server s1 t14) 14)
		(= (processing-time-server s1 t17) 14)
		(= (processing-time-server s1 t19) 10)
		(= (processing-time-server s2 t2) 11)
		(= (processing-time-server s2 t5) 13)
		(= (processing-time-server s2 t7) 14)
		(= (processing-time-server s2 t8) 11)
		(= (processing-time-server s2 t10) 16)
		(= (processing-time-server s2 t14) 15)
		(= (processing-time-server s2 t17) 11)
		(= (processing-time-server s2 t19) 15)
		(= (processing-time-server s3 t2) 10)
		(= (processing-time-server s3 t5) 8)
		(= (processing-time-server s3 t7) 16)
		(= (processing-time-server s3 t8) 10)
		(= (processing-time-server s3 t10) 9)
		(= (processing-time-server s3 t14) 18)
		(= (processing-time-server s3 t17) 14)
		(= (processing-time-server s3 t19) 12)
	)
	(:goal
		(and
			(processed t0)
			(processed t1)
			(processed t2)
			(processed t3)
			(processed t4)
			(processed t5)
			(processed t6)
			(processed t7)
			(processed t8)
			(processed t9)
			(processed t10)
			(processed t11)
			(processed t12)
			(processed t13)
			(processed t14)
			(processed t15)
			(processed t16)
			(processed t17)
			(processed t18)
			(processed t19)	
		)
	)
	(:metric minimize (total-time))
)