(define

	(problem tube-factory-instance)

	(:domain tube-factory)

	(:objects
		m1 - auto-machine
		m0 - semi-machine
		s0 s1 s2 s3 - server
		t0 t1 t2 t3 t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25 t26 t27 t28 t29 t30 t31 t32 t33 t34 t35 t36 t37 t38 t39 t40 t41 t42 t43 t44 t45 t46 t47 t48 t49 t50 t51 t52 t53 t54 t55 t56 t57 t58 t59 t60 t61 t62 t63 t64 t65 t66 t67 t68 t69 t70 t71 t72 t73 t74 t75 t76 t77 t78 t79 - task
	)

	(:init
		(available-server s0)
		(available-server s1)
		(available-server s2)
		(available-server s3)
		(available-machine m0)
		(available-machine m1)
		(setuped m0 t4)
		(setuped m1 t0)
		(not-processed t0)
		(not-processed t1)
		(not-processed t2)
		(not-processed t3)
		(not-processed t4)
		(not-processed t5)
		(not-processed t6)
		(not-processed t7)
		(not-processed t8)
		(not-processed t9)
		(not-processed t10)
		(not-processed t11)
		(not-processed t12)
		(not-processed t13)
		(not-processed t14)
		(not-processed t15)
		(not-processed t16)
		(not-processed t17)
		(not-processed t18)
		(not-processed t19)
		(not-processed t20)
		(not-processed t21)
		(not-processed t22)
		(not-processed t23)
		(not-processed t24)
		(not-processed t25)
		(not-processed t26)
		(not-processed t27)
		(not-processed t28)
		(not-processed t29)
		(not-processed t30)
		(not-processed t31)
		(not-processed t32)
		(not-processed t33)
		(not-processed t34)
		(not-processed t35)
		(not-processed t36)
		(not-processed t37)
		(not-processed t38)
		(not-processed t39)
		(not-processed t40)
		(not-processed t41)
		(not-processed t42)
		(not-processed t43)
		(not-processed t44)
		(not-processed t45)
		(not-processed t46)
		(not-processed t47)
		(not-processed t48)
		(not-processed t49)
		(not-processed t50)
		(not-processed t51)
		(not-processed t52)
		(not-processed t53)
		(not-processed t54)
		(not-processed t55)
		(not-processed t56)
		(not-processed t57)
		(not-processed t58)
		(not-processed t59)
		(not-processed t60)
		(not-processed t61)
		(not-processed t62)
		(not-processed t63)
		(not-processed t64)
		(not-processed t65)
		(not-processed t66)
		(not-processed t67)
		(not-processed t68)
		(not-processed t69)
		(not-processed t70)
		(not-processed t71)
		(not-processed t72)
		(not-processed t73)
		(not-processed t74)
		(not-processed t75)
		(not-processed t76)
		(not-processed t77)
		(not-processed t78)
		(not-processed t79)

		(setupable m0 s1)
		(setupable m0 s3)
		(setupable m1 s0)
		(setupable m1 s2)


		(= (setup-cost s0 m1) 25)
		(= (setup-time s0 m1) 6)
		(= (setup-cost s1 m0) 37)
		(= (setup-time s1 m0) 10)
		(= (setup-cost s2 m1) 23)
		(= (setup-time s2 m1) 8)
		(= (setup-cost s3 m0) 29)
		(= (setup-time s3 m0) 5)


		(requires-auto t0)
		(requires-manual t1)
		(requires-manual t2)
		(requires-auto t3)
		(requires-semi t4)
		(requires-manual t5)
		(requires-auto t6)
		(requires-auto t7)
		(requires-semi t8)
		(requires-manual t9)
		(requires-auto t10)
		(requires-semi t11)
		(requires-semi t12)
		(requires-auto t13)
		(requires-manual t14)
		(requires-semi t15)
		(requires-semi t16)
		(requires-semi t17)
		(requires-auto t18)
		(requires-auto t19)
		(requires-auto t20)
		(requires-semi t21)
		(requires-manual t22)
		(requires-manual t23)
		(requires-manual t24)
		(requires-manual t25)
		(requires-manual t26)
		(requires-auto t27)
		(requires-manual t28)
		(requires-semi t29)
		(requires-semi t30)
		(requires-auto t31)
		(requires-semi t32)
		(requires-manual t33)
		(requires-semi t34)
		(requires-manual t35)
		(requires-semi t36)
		(requires-auto t37)
		(requires-semi t38)
		(requires-semi t39)
		(requires-semi t40)
		(requires-auto t41)
		(requires-manual t42)
		(requires-semi t43)
		(requires-auto t44)
		(requires-manual t45)
		(requires-semi t46)
		(requires-semi t47)
		(requires-semi t48)
		(requires-semi t49)
		(requires-auto t50)
		(requires-auto t51)
		(requires-manual t52)
		(requires-auto t53)
		(requires-auto t54)
		(requires-auto t55)
		(requires-manual t56)
		(requires-manual t57)
		(requires-semi t58)
		(requires-manual t59)
		(requires-manual t60)
		(requires-semi t61)
		(requires-manual t62)
		(requires-semi t63)
		(requires-manual t64)
		(requires-manual t65)
		(requires-auto t66)
		(requires-manual t67)
		(requires-auto t68)
		(requires-auto t69)
		(requires-semi t70)
		(requires-semi t71)
		(requires-auto t72)
		(requires-manual t73)
		(requires-semi t74)
		(requires-manual t75)
		(requires-semi t76)
		(requires-semi t77)
		(requires-auto t78)
		(requires-manual t79)

		(= (processing-cost m0 t4) 42)
		(= (processing-time m0 t4) 17)
		(= (processing-cost m0 t8) 25)
		(= (processing-time m0 t8) 9)
		(= (processing-cost m0 t11) 19)
		(= (processing-time m0 t11) 7)
		(= (processing-cost m0 t12) 23)
		(= (processing-time m0 t12) 10)
		(= (processing-cost m0 t15) 32)
		(= (processing-time m0 t15) 14)
		(= (processing-cost m0 t16) 24)
		(= (processing-time m0 t16) 9)
		(= (processing-cost m0 t17) 18)
		(= (processing-time m0 t17) 9)
		(= (processing-cost m0 t21) 47)
		(= (processing-time m0 t21) 15)
		(= (processing-cost m0 t29) 47)
		(= (processing-time m0 t29) 14)
		(= (processing-cost m0 t30) 15)
		(= (processing-time m0 t30) 5)
		(= (processing-cost m0 t32) 39)
		(= (processing-time m0 t32) 11)
		(= (processing-cost m0 t34) 33)
		(= (processing-time m0 t34) 11)
		(= (processing-cost m0 t36) 14)
		(= (processing-time m0 t36) 9)
		(= (processing-cost m0 t38) 57)
		(= (processing-time m0 t38) 14)
		(= (processing-cost m0 t39) 33)
		(= (processing-time m0 t39) 12)
		(= (processing-cost m0 t40) 23)
		(= (processing-time m0 t40) 7)
		(= (processing-cost m0 t43) 31)
		(= (processing-time m0 t43) 11)
		(= (processing-cost m0 t46) 34)
		(= (processing-time m0 t46) 7)
		(= (processing-cost m0 t47) 28)
		(= (processing-time m0 t47) 11)
		(= (processing-cost m0 t48) 26)
		(= (processing-time m0 t48) 8)
		(= (processing-cost m0 t49) 36)
		(= (processing-time m0 t49) 10)
		(= (processing-cost m0 t58) 56)
		(= (processing-time m0 t58) 13)
		(= (processing-cost m0 t61) 9)
		(= (processing-time m0 t61) 6)
		(= (processing-cost m0 t63) 29)
		(= (processing-time m0 t63) 5)
		(= (processing-cost m0 t70) 50)
		(= (processing-time m0 t70) 11)
		(= (processing-cost m0 t71) 14)
		(= (processing-time m0 t71) 12)
		(= (processing-cost m0 t74) 21)
		(= (processing-time m0 t74) 13)
		(= (processing-cost m0 t76) 21)
		(= (processing-time m0 t76) 15)
		(= (processing-cost m0 t77) 38)
		(= (processing-time m0 t77) 8)
		(= (processing-cost m1 t0) 43)
		(= (processing-time m1 t0) 11)
		(= (processing-cost m1 t3) 16)
		(= (processing-time m1 t3) 5)
		(= (processing-cost m1 t6) 13)
		(= (processing-time m1 t6) 3)
		(= (processing-cost m1 t7) 14)
		(= (processing-time m1 t7) 9)
		(= (processing-cost m1 t10) 58)
		(= (processing-time m1 t10) 13)
		(= (processing-cost m1 t13) 27)
		(= (processing-time m1 t13) 7)
		(= (processing-cost m1 t18) 28)
		(= (processing-time m1 t18) 6)
		(= (processing-cost m1 t19) 64)
		(= (processing-time m1 t19) 14)
		(= (processing-cost m1 t20) 28)
		(= (processing-time m1 t20) 5)
		(= (processing-cost m1 t27) 6)
		(= (processing-time m1 t27) 4)
		(= (processing-cost m1 t31) 10)
		(= (processing-time m1 t31) 2)
		(= (processing-cost m1 t37) 31)
		(= (processing-time m1 t37) 6)
		(= (processing-cost m1 t41) 18)
		(= (processing-time m1 t41) 6)
		(= (processing-cost m1 t44) 13)
		(= (processing-time m1 t44) 5)
		(= (processing-cost m1 t50) 6)
		(= (processing-time m1 t50) 3)
		(= (processing-cost m1 t51) 24)
		(= (processing-time m1 t51) 5)
		(= (processing-cost m1 t53) 16)
		(= (processing-time m1 t53) 4)
		(= (processing-cost m1 t54) 33)
		(= (processing-time m1 t54) 6)
		(= (processing-cost m1 t55) 24)
		(= (processing-time m1 t55) 10)
		(= (processing-cost m1 t66) 28)
		(= (processing-time m1 t66) 5)
		(= (processing-cost m1 t68) 8)
		(= (processing-time m1 t68) 1)
		(= (processing-cost m1 t69) 21)
		(= (processing-time m1 t69) 5)
		(= (processing-cost m1 t72) 10)
		(= (processing-time m1 t72) 9)
		(= (processing-cost m1 t78) 8)
		(= (processing-time m1 t78) 2)
		(= (processing-cost-server s0 t1) 50)
		(= (processing-time-server s0 t1) 10)
		(= (processing-cost-server s0 t2) 32)
		(= (processing-time-server s0 t2) 12)
		(= (processing-cost-server s0 t5) 15)
		(= (processing-time-server s0 t5) 9)
		(= (processing-cost-server s0 t9) 26)
		(= (processing-time-server s0 t9) 10)
		(= (processing-cost-server s0 t14) 13)
		(= (processing-time-server s0 t14) 11)
		(= (processing-cost-server s0 t22) 54)
		(= (processing-time-server s0 t22) 13)
		(= (processing-cost-server s0 t23) 69)
		(= (processing-time-server s0 t23) 20)
		(= (processing-cost-server s0 t24) 29)
		(= (processing-time-server s0 t24) 12)
		(= (processing-cost-server s0 t25) 90)
		(= (processing-time-server s0 t25) 21)
		(= (processing-cost-server s0 t26) 25)
		(= (processing-time-server s0 t26) 9)
		(= (processing-cost-server s0 t28) 39)
		(= (processing-time-server s0 t28) 11)
		(= (processing-cost-server s0 t33) 53)
		(= (processing-time-server s0 t33) 16)
		(= (processing-cost-server s0 t35) 20)
		(= (processing-time-server s0 t35) 9)
		(= (processing-cost-server s0 t42) 51)
		(= (processing-time-server s0 t42) 11)
		(= (processing-cost-server s0 t45) 33)
		(= (processing-time-server s0 t45) 13)
		(= (processing-cost-server s0 t52) 51)
		(= (processing-time-server s0 t52) 16)
		(= (processing-cost-server s0 t56) 22)
		(= (processing-time-server s0 t56) 15)
		(= (processing-cost-server s0 t57) 28)
		(= (processing-time-server s0 t57) 11)
		(= (processing-cost-server s0 t59) 30)
		(= (processing-time-server s0 t59) 13)
		(= (processing-cost-server s0 t60) 44)
		(= (processing-time-server s0 t60) 19)
		(= (processing-cost-server s0 t62) 16)
		(= (processing-time-server s0 t62) 9)
		(= (processing-cost-server s0 t64) 27)
		(= (processing-time-server s0 t64) 9)
		(= (processing-cost-server s0 t65) 26)
		(= (processing-time-server s0 t65) 19)
		(= (processing-cost-server s0 t67) 13)
		(= (processing-time-server s0 t67) 10)
		(= (processing-cost-server s0 t73) 43)
		(= (processing-time-server s0 t73) 12)
		(= (processing-cost-server s0 t75) 22)
		(= (processing-time-server s0 t75) 19)
		(= (processing-cost-server s0 t79) 28)
		(= (processing-time-server s0 t79) 11)
		(= (processing-cost-server s1 t1) 33)
		(= (processing-time-server s1 t1) 8)
		(= (processing-cost-server s1 t2) 10)
		(= (processing-time-server s1 t2) 9)
		(= (processing-cost-server s1 t5) 48)
		(= (processing-time-server s1 t5) 14)
		(= (processing-cost-server s1 t9) 44)
		(= (processing-time-server s1 t9) 19)
		(= (processing-cost-server s1 t14) 54)
		(= (processing-time-server s1 t14) 17)
		(= (processing-cost-server s1 t22) 53)
		(= (processing-time-server s1 t22) 13)
		(= (processing-cost-server s1 t23) 22)
		(= (processing-time-server s1 t23) 9)
		(= (processing-cost-server s1 t24) 49)
		(= (processing-time-server s1 t24) 13)
		(= (processing-cost-server s1 t25) 28)
		(= (processing-time-server s1 t25) 18)
		(= (processing-cost-server s1 t26) 12)
		(= (processing-time-server s1 t26) 12)
		(= (processing-cost-server s1 t28) 16)
		(= (processing-time-server s1 t28) 9)
		(= (processing-cost-server s1 t33) 11)
		(= (processing-time-server s1 t33) 8)
		(= (processing-cost-server s1 t35) 40)
		(= (processing-time-server s1 t35) 11)
		(= (processing-cost-server s1 t42) 22)
		(= (processing-time-server s1 t42) 18)
		(= (processing-cost-server s1 t45) 46)
		(= (processing-time-server s1 t45) 13)
		(= (processing-cost-server s1 t52) 65)
		(= (processing-time-server s1 t52) 21)
		(= (processing-cost-server s1 t56) 21)
		(= (processing-time-server s1 t56) 10)
		(= (processing-cost-server s1 t57) 38)
		(= (processing-time-server s1 t57) 9)
		(= (processing-cost-server s1 t59) 25)
		(= (processing-time-server s1 t59) 12)
		(= (processing-cost-server s1 t60) 29)
		(= (processing-time-server s1 t60) 7)
		(= (processing-cost-server s1 t62) 40)
		(= (processing-time-server s1 t62) 12)
		(= (processing-cost-server s1 t64) 51)
		(= (processing-time-server s1 t64) 15)
		(= (processing-cost-server s1 t65) 44)
		(= (processing-time-server s1 t65) 19)
		(= (processing-cost-server s1 t67) 32)
		(= (processing-time-server s1 t67) 14)
		(= (processing-cost-server s1 t73) 20)
		(= (processing-time-server s1 t73) 11)
		(= (processing-cost-server s1 t75) 47)
		(= (processing-time-server s1 t75) 13)
		(= (processing-cost-server s1 t79) 37)
		(= (processing-time-server s1 t79) 10)
		(= (processing-cost-server s2 t1) 22)
		(= (processing-time-server s2 t1) 13)
		(= (processing-cost-server s2 t2) 47)
		(= (processing-time-server s2 t2) 15)
		(= (processing-cost-server s2 t5) 61)
		(= (processing-time-server s2 t5) 17)
		(= (processing-cost-server s2 t9) 46)
		(= (processing-time-server s2 t9) 18)
		(= (processing-cost-server s2 t14) 55)
		(= (processing-time-server s2 t14) 15)
		(= (processing-cost-server s2 t22) 74)
		(= (processing-time-server s2 t22) 17)
		(= (processing-cost-server s2 t23) 29)
		(= (processing-time-server s2 t23) 14)
		(= (processing-cost-server s2 t24) 26)
		(= (processing-time-server s2 t24) 21)
		(= (processing-cost-server s2 t25) 84)
		(= (processing-time-server s2 t25) 19)
		(= (processing-cost-server s2 t26) 32)
		(= (processing-time-server s2 t26) 13)
		(= (processing-cost-server s2 t28) 29)
		(= (processing-time-server s2 t28) 8)
		(= (processing-cost-server s2 t33) 55)
		(= (processing-time-server s2 t33) 12)
		(= (processing-cost-server s2 t35) 42)
		(= (processing-time-server s2 t35) 17)
		(= (processing-cost-server s2 t42) 61)
		(= (processing-time-server s2 t42) 15)
		(= (processing-cost-server s2 t45) 30)
		(= (processing-time-server s2 t45) 13)
		(= (processing-cost-server s2 t52) 21)
		(= (processing-time-server s2 t52) 8)
		(= (processing-cost-server s2 t56) 31)
		(= (processing-time-server s2 t56) 12)
		(= (processing-cost-server s2 t57) 17)
		(= (processing-time-server s2 t57) 7)
		(= (processing-cost-server s2 t59) 20)
		(= (processing-time-server s2 t59) 10)
		(= (processing-cost-server s2 t60) 20)
		(= (processing-time-server s2 t60) 7)
		(= (processing-cost-server s2 t62) 81)
		(= (processing-time-server s2 t62) 18)
		(= (processing-cost-server s2 t64) 44)
		(= (processing-time-server s2 t64) 19)
		(= (processing-cost-server s2 t65) 61)
		(= (processing-time-server s2 t65) 14)
		(= (processing-cost-server s2 t67) 30)
		(= (processing-time-server s2 t67) 9)
		(= (processing-cost-server s2 t73) 20)
		(= (processing-time-server s2 t73) 9)
		(= (processing-cost-server s2 t75) 56)
		(= (processing-time-server s2 t75) 14)
		(= (processing-cost-server s2 t79) 38)
		(= (processing-time-server s2 t79) 11)
		(= (processing-cost-server s3 t1) 37)
		(= (processing-time-server s3 t1) 8)
		(= (processing-cost-server s3 t2) 32)
		(= (processing-time-server s3 t2) 16)
		(= (processing-cost-server s3 t5) 57)
		(= (processing-time-server s3 t5) 12)
		(= (processing-cost-server s3 t9) 23)
		(= (processing-time-server s3 t9) 19)
		(= (processing-cost-server s3 t14) 27)
		(= (processing-time-server s3 t14) 8)
		(= (processing-cost-server s3 t22) 64)
		(= (processing-time-server s3 t22) 15)
		(= (processing-cost-server s3 t23) 51)
		(= (processing-time-server s3 t23) 11)
		(= (processing-cost-server s3 t24) 46)
		(= (processing-time-server s3 t24) 20)
		(= (processing-cost-server s3 t25) 51)
		(= (processing-time-server s3 t25) 17)
		(= (processing-cost-server s3 t26) 48)
		(= (processing-time-server s3 t26) 10)
		(= (processing-cost-server s3 t28) 26)
		(= (processing-time-server s3 t28) 12)
		(= (processing-cost-server s3 t33) 62)
		(= (processing-time-server s3 t33) 15)
		(= (processing-cost-server s3 t35) 58)
		(= (processing-time-server s3 t35) 13)
		(= (processing-cost-server s3 t42) 70)
		(= (processing-time-server s3 t42) 17)
		(= (processing-cost-server s3 t45) 17)
		(= (processing-time-server s3 t45) 13)
		(= (processing-cost-server s3 t52) 24)
		(= (processing-time-server s3 t52) 8)
		(= (processing-cost-server s3 t56) 72)
		(= (processing-time-server s3 t56) 17)
		(= (processing-cost-server s3 t57) 35)
		(= (processing-time-server s3 t57) 14)
		(= (processing-cost-server s3 t59) 19)
		(= (processing-time-server s3 t59) 9)
		(= (processing-cost-server s3 t60) 17)
		(= (processing-time-server s3 t60) 7)
		(= (processing-cost-server s3 t62) 25)
		(= (processing-time-server s3 t62) 9)
		(= (processing-cost-server s3 t64) 11)
		(= (processing-time-server s3 t64) 11)
		(= (processing-cost-server s3 t65) 37)
		(= (processing-time-server s3 t65) 15)
		(= (processing-cost-server s3 t67) 16)
		(= (processing-time-server s3 t67) 9)
		(= (processing-cost-server s3 t73) 18)
		(= (processing-time-server s3 t73) 9)
		(= (processing-cost-server s3 t75) 58)
		(= (processing-time-server s3 t75) 13)
		(= (processing-cost-server s3 t79) 33)
		(= (processing-time-server s3 t79) 13)


	)

	(:goal
		(and
			(processed t0)
			(processed t1)
			(processed t2)
			(processed t3)
			(processed t4)
			(processed t5)
			(processed t6)
			(processed t7)
			(processed t8)
			(processed t9)
			(processed t10)
			(processed t11)
			(processed t12)
			(processed t13)
			(processed t14)
			(processed t15)
			(processed t16)
			(processed t17)
			(processed t18)
			(processed t19)
			(processed t20)
			(processed t21)
			(processed t22)
			(processed t23)
			(processed t24)
			(processed t25)
			(processed t26)
			(processed t27)
			(processed t28)
			(processed t29)
			(processed t30)
			(processed t31)
			(processed t32)
			(processed t33)
			(processed t34)
			(processed t35)
			(processed t36)
			(processed t37)
			(processed t38)
			(processed t39)
			(processed t40)
			(processed t41)
			(processed t42)
			(processed t43)
			(processed t44)
			(processed t45)
			(processed t46)
			(processed t47)
			(processed t48)
			(processed t49)
			(processed t50)
			(processed t51)
			(processed t52)
			(processed t53)
			(processed t54)
			(processed t55)
			(processed t56)
			(processed t57)
			(processed t58)
			(processed t59)
			(processed t60)
			(processed t61)
			(processed t62)
			(processed t63)
			(processed t64)
			(processed t65)
			(processed t66)
			(processed t67)
			(processed t68)
			(processed t69)
			(processed t70)
			(processed t71)
			(processed t72)
			(processed t73)
			(processed t74)
			(processed t75)
			(processed t76)
			(processed t77)
			(processed t78)
			(processed t79)
		)
	)

	(:metric minimize (total-time))

)