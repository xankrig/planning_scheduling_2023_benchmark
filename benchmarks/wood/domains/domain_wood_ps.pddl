;; Woodworking
;;

(define (domain woodworking-ps)
  (:requirements :typing :action-costs :durative-actions :negative-preconditions :ps-task)
  (:types
      acolour awood woodobj resource surface treatmentstatus aboardsize apartsize - object
      machine - resource
      highspeed-saw glazer grinder immersion-varnisher planer saw spray-varnisher - machine
      board part - woodobj
  )
  (:constants
      verysmooth smooth rough - surface
      varnished glazed untreated colourfragments - treatmentstatus
      natural - acolour
      small medium large - apartsize
  )
  (:attributes
	(available-board ?obj - board) ;should be those environmental attributes of objects be considered as statics?

	(in-highspeed-saw ?b - board ?m - highspeed-saw)
        (empty ?m - highspeed-saw)
        (has-colour ?machine - machine ?colour - acolour)   
 	
  )
  (:static
	(woodboard ?obj - board ?wood - awood)

        (boardsize-successor ?size1 - aboardsize ?size2 - aboardsize)
        (contains-part ?b - board ?p - part)
        (goalsize ?part - part ?size - apartsize)


  )
  (:predicates 
        (woodpart ?obj - part ?wood - awood)

        (grind-treatment-change ?old - treatmentstatus ?new - treatmentstatus)
        (is-smooth ?surface - surface)
	(unused ?obj - part)
        (available-part ?obj - part) ;should be those environmental attributes of objects be considered as statics?

        (surface-condition ?obj - woodobj ?surface - surface)
        (treatment ?obj - part ?treatment - treatmentstatus)
        (colour ?obj - part ?colour - acolour)
        (boardsize ?board - board ?size - aboardsize)

  )

      
  (:functions (total-cost) - number
  	(processing-spray-varnish-time ?m - machine ?obj - part) - number
  	(processing-spray-varnish-cost ?m - machine ?obj - part) - number
	(processing-immersion-varnish-time ?m - machine ?obj - part) - number
	(processing-immersion-varnish-cost ?m - machine ?obj - part) - number
  	(processing-glaze-time ?m - machine ?obj - part) - number
  	(processing-glaze-cost ?m - machine ?obj - part) - number
  	(processing-grind-time ?m - machine ?obj - part) - number
  	(processing-grind-cost ?m - machine ?obj - part) - number
  	(processing-plane-time ?m - machine ?obj - part) - number
  	(processing-plane-cost ?m - machine ?obj - part) - number
	;(processing-time ?m - machine ?obj - part) - number
	(load-time ?m - machine ?b - board) - number
	(load-cost ?m - machine ?b - board) - number
  )

  (:production-activity do-immersion-varnish
    :parameters (?x - part
                 ?newcolour - acolour ?surface - surface)
    :attributes (and
	(for ?m - immersion-varnisher) (and (has-colour ?m ?newcolour))
    )
    :duration (= ?duration (processing-immersion-varnish-time ?m ?x))
    :cost (= ?cost (processing-immersion-varnish-cost ?m ?x))
    :static (and
            
    )
    :precondition (and
            (available-part ?x)
            (surface-condition ?x ?surface)
            (is-smooth ?surface)
            (treatment ?x untreated)
    )
    :del-effect (and
            (available-part ?x)
            (treatment ?x untreated)
            (colour ?x natural)
    )
    :add-effect (and
            (available-part ?x)
            ;(increase (total-cost) 10)
            (treatment ?x varnished)
            (colour ?x ?newcolour)
    ))

  (:production-activity do-spray-varnish
    :parameters (?x - part
                 ?newcolour - acolour ?surface - surface)
    :attributes (and
	(for ?m - spray-varnisher) (and (has-colour ?m ?newcolour))
    )
    :duration (= ?duration (processing-spray-varnish-time ?m ?x))
    :cost (= ?cost (processing-spray-varnish-cost ?m ?x))
    :static (and

    )
    :precondition (and
	    (available-part ?x)
            (surface-condition ?x ?surface)
            (is-smooth ?surface)
            (treatment ?x untreated)
    )
    :del-effect (and 
            (available-part ?x)
            (treatment ?x untreated)
            (colour ?x natural)
    )
    :add-effect (and 
            (available-part ?x)
            ;(increase (total-cost) (spray-varnish-cost ?x))
            (treatment ?x varnished)
            (colour ?x ?newcolour)
    ))
  (:production-activity do-glaze
    :parameters (?x - part
                 ?newcolour - acolour)
    :attributes (and
	(for ?m - glazer) (and (has-colour ?m ?newcolour))
    )
    :duration (= ?duration (processing-glaze-time ?m ?x))
    :cost (= ?cost (processing-glaze-cost ?m ?x))
    :static (and

    )
    :precondition (and
	    (available-part ?x)
            (treatment ?x untreated)
    )
    :del-effect (and 
            (available-part ?x)
            (treatment ?x untreated)
            (colour ?x natural)
    )
    :add-effect (and 
            (available-part ?x)
            ;(increase (total-cost) (glaze-cost ?x))
            (treatment ?x glazed)
            (colour ?x ?newcolour)
    ))

  (:production-activity do-grind
    :parameters (?x - part ?oldsurface - surface
                 ?oldcolour - acolour 
                 ?oldtreatment ?newtreatment - treatmentstatus) 
    :attributes (and
	(for ?m - grinder) ()
    )
    :duration (= ?duration (processing-grind-time ?m ?x))
    :cost (= ?cost (processing-grind-cost ?m ?x))
    :static (and

    )
    :precondition (and 
	    (available-part ?x)
            (surface-condition ?x ?oldsurface)
            (is-smooth ?oldsurface)
            (colour ?x ?oldcolour)
            (treatment ?x ?oldtreatment)
            (grind-treatment-change ?oldtreatment ?newtreatment)
	)
    :del-effect (and
            (available-part ?x)
            (surface-condition ?x ?oldsurface)
            (treatment ?x ?oldtreatment)
            (colour ?x ?oldcolour)
    )
    :add-effect (and
            (available-part ?x)
            ;(increase (total-cost) (grind-cost ?x))
            (surface-condition ?x verysmooth)
            (treatment ?x ?newtreatment)
            (colour ?x natural)
		))
  (:production-activity do-plane
    :parameters (?x - part ?oldsurface - surface
                 ?oldcolour - acolour ?oldtreatment - treatmentstatus) 
    :attributes (and
	(for ?m - planer) ()
    )
    :duration (= ?duration (processing-plane-time ?m ?x))
    :cost (= ?cost (processing-plane-cost ?m ?x))
    :static (and

    )
    :precondition (and 
	    (available-part ?x)
            (surface-condition ?x ?oldsurface)
            (treatment ?x ?oldtreatment)
            (colour ?x ?oldcolour)
    )
    :del-effect (and
            (available-part ?x)
            (surface-condition ?x ?oldsurface)
            (treatment ?x ?oldtreatment)
            (colour ?x ?oldcolour)
	)
    :add-effect (and
            (available-part ?x)
            ;(increase (total-cost) (plane-cost ?x))
            (surface-condition ?x smooth)
            (treatment ?x untreated)
            (colour ?x natural)
	))

  (:maintenance-activity load-highspeed-saw
    :parameters (?b - board)
    :resource 
    (
    	?m - highspeed-saw
    )
    :attributes (and
	(empty ?m)
	(available-board ?b)
    )
    :duration (= ?duration (load-time ?m ?b))
    :cost (= ?cost (load-cost ?m ?b))
    :static (and
	    
    )
    :rem-effect (and
            (available-board ?b) 
            (empty ?m)
    )
    :add-effect (and
            (in-highspeed-saw ?b ?m)
    ))
            
  (:maintenance-activity unload-highspeed-saw
    :parameters (?b - board)
    :resource
    (
    	?m - highspeed-saw
    )
    :attributes (and
	(in-highspeed-saw ?b ?m)
    )
    :duration (= ?duration (load-time ?m ?b))
    :cost (= ?cost (load-cost ?m ?b))
    :static (and

    )
    :rem-effect (and
            (in-highspeed-saw ?b ?m)
    )
    :add-effect (and
            (available-board ?b)
            (empty ?m)
    ))       
  (:production-activity cut-board-small
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface ?size_before ?size_after - aboardsize)
    :attributes 
    (and
	(for ?m - highspeed-saw) (and (in-highspeed-saw ?b ?m))
    )
    :duration (= ?duration 10)
    :cost (= ?cost 10)
    :static 
    (and
	    (woodboard ?b ?w)
            (goalsize ?p small)
            (boardsize-successor ?size_after ?size_before)
    )
    :precondition (and
            (unused ?p)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
    )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
     ))
  (:production-activity cut-board-medium
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface 
                 ?size_before ?s1 ?size_after - aboardsize)
    :attributes 
    (and
	(for ?m - highspeed-saw) (and (in-highspeed-saw ?b ?m))
    )
    :duration (= ?duration 15)
    :cost (= ?cost 15)
    :static 
    (and
	    (woodboard ?b ?w)
            (goalsize ?p medium)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before)
    )
    :precondition (and
            (unused ?p)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
    )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
    ))
  (:production-activity cut-board-large
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface 
                 ?size_before ?s1 ?s2 ?size_after - aboardsize)
    :attributes 
    (and
	(for ?m - highspeed-saw) (and (in-highspeed-saw ?b ?m))
    )
    :duration (= ?duration 20)
    :cost (= ?cost 20)
    :static 
    (and
	    (woodboard ?b ?w)
            (goalsize ?p large)
	    (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?s2)
            (boardsize-successor ?s2 ?size_before)
    )
    :precondition (and
            (unused ?p)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
    )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
    ))
  (:production-activity do-saw-small
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface ?size_before ?size_after - aboardsize) 
    :attributes 
    (and
	(for ?m - saw) (and (available-board ?b))
    )
    :duration (= ?duration 10)
    :cost (= ?cost 10)
    :static 
    (and
            (woodboard ?b ?w)
            (goalsize ?p small)
	    (boardsize-successor ?size_after ?size_before)
    )
    :precondition (and 
            (unused ?p)
            ;(wood ?b ?w)
            (surface-condition ?b ?surface)
            (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
    )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
		))
  (:production-activity do-saw-medium
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface 
                 ?size_before ?s1 ?size_after - aboardsize) 
    :attributes 
    (and
	(for ?m - saw) (and (available-board ?b))
    )
    :duration (= ?duration 15)
    :cost (= ?cost 15)
    :static 
    (and
            (woodboard ?b ?w)
            (goalsize ?p medium)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?size_before)
    )
    :precondition (and 
            (unused ?p)
            (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
    )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
		))
  (:production-activity do-saw-large
    :parameters (?b - board ?p - part ?w - awood
                 ?surface - surface 
                 ?size_before ?s1 ?s2 ?size_after - aboardsize)
    :attributes 
    (and
	(for ?m - saw) (and (available-board ?b))
    )
    :duration (= ?duration 20)
    :cost (= ?cost 20)
    :static (and
            (woodboard ?b ?w)
            (goalsize ?p large)
            (boardsize-successor ?size_after ?s1)
            (boardsize-successor ?s1 ?s2)
            (boardsize-successor ?s2 ?size_before)
    )
    :precondition (and
	(unused ?p)
        (surface-condition ?b ?surface) 
        (boardsize ?b ?size_before)
    )
    :del-effect (and
	    (unused ?p)
     )
    :add-effect (and
            (available-part ?p)
            (woodpart ?p ?w)
            (surface-condition ?p ?surface)
            (colour ?p natural)
            (treatment ?p untreated)
            (boardsize ?b ?size_after)
	))
)
