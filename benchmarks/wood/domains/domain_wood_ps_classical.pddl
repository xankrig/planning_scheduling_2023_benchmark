(define (domain woodworking-ps)

    (:requirements :strips :typing :conditional-effects :negative-preconditions :equality)
    (:types
      acolour awood woodobj resource surface treatmentstatus aboardsize apartsize - object
      machine - resource
      highspeed-saw glazer grinder immersion-varnisher planer saw spray-varnisher - machine
      board part - woodobj
  )
    (:constants
      verysmooth smooth rough - surface
      varnished glazed untreated colourfragments - treatmentstatus
      natural - acolour
      small medium large - apartsize
  )
    (:predicates
	    (woodpart ?obj - part ?wood - awood)
	    (grind-treatment-change ?old - treatmentstatus ?new - treatmentstatus)
	    (is-smooth ?surface - surface)
	    (unused ?obj - part)
	    (available-part ?obj - part)
	    (surface-condition ?obj - woodobj ?surface - surface)
	    (treatment ?obj - part ?treatment - treatmentstatus)
	    (colour ?obj - part ?colour - acolour)
	    (boardsize ?board - board ?size - aboardsize)
	    (available-board ?obj - board)
	    (in-highspeed-saw ?b - board ?m - highspeed-saw)
	    (empty ?m - highspeed-saw)
	    (has-colour ?machine - machine ?colour - acolour)
	    (woodboard ?obj - board ?wood - awood)
	    (boardsize-successor ?size1 - aboardsize ?size2 - aboardsize)
	    (contains-part ?b - board ?p - part)
	    (goalsize ?part - part ?size - apartsize)
    )
    (:functions
	    (processing-spray-varnish-cost ?m - machine ?obj - part) - number
	    (processing-immersion-varnish-cost ?m - machine ?obj - part) - number
	    (processing-glaze-cost ?m - machine ?obj - part) - number
	    (processing-grind-cost ?m - machine ?obj - part) - number
	    (processing-plane-cost ?m - machine ?obj - part) - number
	    (load-cost ?m - machine ?b - board) - number
	    (total-cost)
    )
    (:action do-immersion-varnish
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?surface - surface
	    ?m - immersion-varnisher
	)
	:precondition
	(and
	    (available-part ?x)
	    (surface-condition ?x ?surface)
	    (is-smooth ?surface)
	    (treatment ?x untreated)
	    (has-colour ?m ?newcolour)
    )
	:effect
	(and
	    (not (available-part ?x))
	    (not (treatment ?x untreated))
	    (not (colour ?x natural))
	    (available-part ?x)
	    (treatment ?x varnished)
	    (colour ?x ?newcolour)
	    (increase (total-cost) (processing-immersion-varnish-cost ?m ?x))
	)
    )
    (:action do-spray-varnish
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?surface - surface
	    ?m - spray-varnisher
	)
	:precondition
	(and
	    (available-part ?x)
	    (surface-condition ?x ?surface)
	    (is-smooth ?surface)
	    (treatment ?x untreated)
	    (has-colour ?m ?newcolour)
    )
	:effect
	(and
	    (not (available-part ?x))
	    (not (treatment ?x untreated))
	    (not (colour ?x natural))
	    (available-part ?x)
	    (treatment ?x varnished)
	    (colour ?x ?newcolour)
	    (increase (total-cost) (processing-spray-varnish-cost ?m ?x))
	)
    )
    (:action do-glaze
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?m - glazer
	)
	:precondition
	(and
	    (available-part ?x)
	    (treatment ?x untreated)
	    (has-colour ?m ?newcolour)
    )
	:effect
	(and
	    (not (available-part ?x))
	    (not (treatment ?x untreated))
	    (not (colour ?x natural))
	    (available-part ?x)
	    (treatment ?x glazed)
	    (colour ?x ?newcolour)
	    (increase (total-cost) (processing-glaze-cost ?m ?x))
	)
    )
    (:action do-grind
	:parameters
	(
	    ?x - part
	    ?oldsurface - surface
	    ?oldcolour - acolour
	    ?oldtreatment ?newtreatment - treatmentstatus
	    ?m - grinder
	)
	:precondition
	(and
	    (available-part ?x)
	    (surface-condition ?x ?oldsurface)
	    (is-smooth ?oldsurface)
	    (colour ?x ?oldcolour)
	    (treatment ?x ?oldtreatment)
	    (grind-treatment-change ?oldtreatment ?newtreatment)
	)
	:effect
	(and
	    (not (available-part ?x))
	    (not (surface-condition ?x ?oldsurface))
	    (not (treatment ?x ?oldtreatment))
	    (not (colour ?x ?oldcolour))
	    (available-part ?x)
	    (surface-condition ?x verysmooth)
	    (treatment ?x ?newtreatment)
	    (colour ?x natural)
	    (increase (total-cost) (processing-grind-cost ?m ?x))
	)
    )
    (:action do-plane
	:parameters
	(
	    ?x - part
	    ?oldsurface - surface
	    ?oldcolour - acolour
	    ?oldtreatment - treatmentstatus
	    ?m - planer
	)
	:precondition
	(and
	    (available-part ?x)
	    (surface-condition ?x ?oldsurface)
	    (treatment ?x ?oldtreatment)
	    (colour ?x ?oldcolour)
    )
	:effect
	(and
	    (not (available-part ?x))
	    (not (surface-condition ?x ?oldsurface))
	    (not (treatment ?x ?oldtreatment))
	    (not (colour ?x ?oldcolour))
	    (available-part ?x)
	    (surface-condition ?x smooth)
	    (treatment ?x untreated)
	    (colour ?x natural)
	    (increase (total-cost) (processing-plane-cost ?m ?x))
	)
    )
    (:action load-highspeed-saw
	:parameters
	(
	    ?b - board
	    ?m - highspeed-saw
	)
	:precondition
	(and
	    (empty ?m)
	    (available-board ?b)
	)
	:effect
	(and
	    (not (available-board ?b))
	    (not (empty ?m))
	    (in-highspeed-saw ?b ?m)
	    (increase (total-cost) (load-cost ?m ?b))
	)
    )
    (:action unload-highspeed-saw
	:parameters
	(
	    ?b - board
	    ?m - highspeed-saw
	)
	:precondition
	(and
	    (in-highspeed-saw ?b ?m)
	)
	:effect
	(and
	    (not (in-highspeed-saw ?b ?m))
	    (available-board ?b)
	    (empty ?m)
	    (increase (total-cost) (load-cost ?m ?b))
	)
    )
    (:action cut-board-small
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:precondition
    (and
	    (woodboard ?b ?w)
	    (goalsize ?p small)
	    (boardsize-successor ?size_after ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (in-highspeed-saw ?b ?m)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 10)
	)
    )
    (:action cut-board-medium
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:precondition
    (and
	    (woodboard ?b ?w)
	    (goalsize ?p medium)
	    (boardsize-successor ?size_after ?s1)
	    (boardsize-successor ?s1 ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (in-highspeed-saw ?b ?m)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 15)
	)
    )
    (:action cut-board-large
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?s2 ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:precondition
    (and
	    (woodboard ?b ?w)
	    (goalsize ?p large)
	    (boardsize-successor ?size_after ?s1)
	    (boardsize-successor ?s1 ?s2)
	    (boardsize-successor ?s2 ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (in-highspeed-saw ?b ?m)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 20)
	)
    )
    (:action do-saw-small
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?size_after - aboardsize
	    ?m - saw
	)
	:precondition
    (and
	    (woodboard ?b ?w)
	    (goalsize ?p small)
	    (boardsize-successor ?size_after ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (available-board ?b)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 10)
	)
    )
    (:action do-saw-medium
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?size_after - aboardsize
	    ?m - saw
	)
	:precondition
    (and
	    (woodboard ?b ?w)
	    (goalsize ?p medium)
	    (boardsize-successor ?size_after ?s1)
	    (boardsize-successor ?s1 ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (available-board ?b)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 15)
	)
    )
    (:action do-saw-large
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?s2 ?size_after - aboardsize
	    ?m - saw
	)
	:precondition
	(and
	    (woodboard ?b ?w)
	    (goalsize ?p large)
	    (boardsize-successor ?size_after ?s1)
	    (boardsize-successor ?s1 ?s2)
	    (boardsize-successor ?s2 ?size_before)
	    (unused ?p)
	    (surface-condition ?b ?surface)
	    (boardsize ?b ?size_before)
	    (available-board ?b)
    )
	:effect
	(and
	    (not (unused ?p))
	    (available-part ?p)
	    (woodpart ?p ?w)
	    (surface-condition ?p ?surface)
	    (colour ?p natural)
	    (treatment ?p untreated)
	    (boardsize ?b ?size_after)
	    (increase (total-cost) 20)
	)
    )
)

