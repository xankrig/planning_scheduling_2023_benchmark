(define (domain woodworking-ps)

(:requirements :typing :action-costs :durative-actions :negative-preconditions)
    (:types
      acolour awood woodobj resource surface treatmentstatus aboardsize apartsize - object
      machine - resource
      highspeed-saw glazer grinder immersion-varnisher planer saw spray-varnisher - machine
      board part - woodobj
  )
    (:constants
      verysmooth smooth rough - surface
      varnished glazed untreated colourfragments - treatmentstatus
      natural - acolour
      small medium large - apartsize
  )
    (:predicates
	    (woodpart ?obj - part ?wood - awood)
	    (grind-treatment-change ?old - treatmentstatus ?new - treatmentstatus)
	    (is-smooth ?surface - surface)
	    (unused ?obj - part)
	    (available-part ?obj - part)
	    (surface-condition ?obj - woodobj ?surface - surface)
	    (treatment ?obj - part ?treatment - treatmentstatus)
	    (colour ?obj - part ?colour - acolour)
	    (boardsize ?board - board ?size - aboardsize)
	    (available-board ?obj - board)
	    (in-highspeed-saw ?b - board ?m - highspeed-saw)
	    (empty ?m - highspeed-saw)
	    (has-colour ?machine - machine ?colour - acolour)
	    (woodboard ?obj - board ?wood - awood)
	    (boardsize-successor ?size1 - aboardsize ?size2 - aboardsize)
	    (contains-part ?b - board ?p - part)
	    (goalsize ?part - part ?size - apartsize)
	    (available ?r - resource)
    )
    (:functions
	    (processing-spray-varnish-time ?m - machine ?obj - part) - number
	    (processing-immersion-varnish-time ?m - machine ?obj - part) - number
	    (processing-glaze-time ?m - machine ?obj - part) - number
	    (processing-grind-time ?m - machine ?obj - part) - number
	    (processing-plane-time ?m - machine ?obj - part) - number
	    (load-time ?m - machine ?b - board) - number
    )
    (:durative-action do-immersion-varnish
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?surface - surface
	    ?m - immersion-varnisher
	)
	:duration (= ?duration (processing-immersion-varnish-time ?m ?x))
	:condition
	(and
	    (at start (available-part ?x))
	    (at start (surface-condition ?x ?surface))
	    (at start (is-smooth ?surface))
	    (at start (treatment ?x untreated))
	    (at start (has-colour ?m ?newcolour))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (available-part ?x)))
	    (at start (not (treatment ?x untreated)))
	    (at start (not (colour ?x natural)))
	    (at start (not (available ?m)))
	    (at end (available-part ?x))
	    (at end (treatment ?x varnished))
	    (at end (colour ?x ?newcolour))
	    (at end (available ?m))
	)
    )
    (:durative-action do-spray-varnish
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?surface - surface
	    ?m - spray-varnisher
	)
	:duration (= ?duration (processing-spray-varnish-time ?m ?x))
	:condition
	(and
	    (at start (available-part ?x))
	    (at start (surface-condition ?x ?surface))
	    (at start (is-smooth ?surface))
	    (at start (treatment ?x untreated))
	    (at start (has-colour ?m ?newcolour))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (available-part ?x)))
	    (at start (not (treatment ?x untreated)))
	    (at start (not (colour ?x natural)))
	    (at start (not (available ?m)))
	    (at end (available-part ?x))
	    (at end (treatment ?x varnished))
	    (at end (colour ?x ?newcolour))
	    (at end (available ?m))
	)
    )
    (:durative-action do-glaze
	:parameters
	(
	    ?x - part
	    ?newcolour - acolour
	    ?m - glazer
	)
	:duration (= ?duration (processing-glaze-time ?m ?x))
	:condition
	(and
	    (at start (available-part ?x))
	    (at start (treatment ?x untreated))
	    (at start (has-colour ?m ?newcolour))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (available-part ?x)))
	    (at start (not (treatment ?x untreated)))
	    (at start (not (colour ?x natural)))
	    (at start (not (available ?m)))
	    (at end (available-part ?x))
	    (at end (treatment ?x glazed))
	    (at end (colour ?x ?newcolour))
	    (at end (available ?m))
	)
    )
    (:durative-action do-grind
	:parameters
	(
	    ?x - part
	    ?oldsurface - surface
	    ?oldcolour - acolour
	    ?oldtreatment ?newtreatment - treatmentstatus
	    ?m - grinder
	)
	:duration (= ?duration (processing-grind-time ?m ?x))
	:condition
	(and
	    (at start (available-part ?x))
	    (at start (surface-condition ?x ?oldsurface))
	    (at start (is-smooth ?oldsurface))
	    (at start (colour ?x ?oldcolour))
	    (at start (treatment ?x ?oldtreatment))
	    (at start (grind-treatment-change ?oldtreatment ?newtreatment))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (available-part ?x)))
	    (at start (not (surface-condition ?x ?oldsurface)))
	    (at start (not (treatment ?x ?oldtreatment)))
	    (at start (not (colour ?x ?oldcolour)))
	    (at start (not (available ?m)))
	    (at end (available-part ?x))
	    (at end (surface-condition ?x verysmooth))
	    (at end (treatment ?x ?newtreatment))
	    (at end (colour ?x natural))
	    (at end (available ?m))
	)
    )
    (:durative-action do-plane
	:parameters
	(
	    ?x - part
	    ?oldsurface - surface
	    ?oldcolour - acolour
	    ?oldtreatment - treatmentstatus
	    ?m - planer
	)
	:duration (= ?duration (processing-plane-time ?m ?x))
	:condition
	(and
	    (at start (available-part ?x))
	    (at start (surface-condition ?x ?oldsurface))
	    (at start (treatment ?x ?oldtreatment))
	    (at start (colour ?x ?oldcolour))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (available-part ?x)))
	    (at start (not (surface-condition ?x ?oldsurface)))
	    (at start (not (treatment ?x ?oldtreatment)))
	    (at start (not (colour ?x ?oldcolour)))
	    (at start (not (available ?m)))
	    (at end (available-part ?x))
	    (at end (surface-condition ?x smooth))
	    (at end (treatment ?x untreated))
	    (at end (colour ?x natural))
	    (at end (available ?m))
	)
    )
    (:durative-action load-highspeed-saw
	:parameters
	(
	    ?b - board
	    ?m - highspeed-saw
	)
	:duration (= ?duration (load-time ?m ?b))
	:condition
	(and
	    (at start (empty ?m))
	    (at start (available-board ?b))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (available-board ?b)))
	    (at start (not (empty ?m)))
	    (at start (not (available ?m)))
	    (at end (in-highspeed-saw ?b ?m))
	    (at end (available ?m))
	)
    )
    (:durative-action unload-highspeed-saw
	:parameters
	(
	    ?b - board
	    ?m - highspeed-saw
	)
	:duration (= ?duration (load-time ?m ?b))
	:condition
	(and
	    (at start (in-highspeed-saw ?b ?m))
	    (at start (available ?m))
	)
	:effect
	(and
	    (at start (not (in-highspeed-saw ?b ?m)))
	    (at start (not (available ?m)))
	    (at end (available-board ?b))
	    (at end (empty ?m))
	    (at end (available ?m))
	)
    )
    (:durative-action cut-board-small
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:duration (= ?duration 10)
	:condition
    (and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p small))
	    (at start (boardsize-successor ?size_after ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (in-highspeed-saw ?b ?m))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
    (:durative-action cut-board-medium
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:duration (= ?duration 15)
	:condition
    (and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p medium))
	    (at start (boardsize-successor ?size_after ?s1))
	    (at start (boardsize-successor ?s1 ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (in-highspeed-saw ?b ?m))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
    (:durative-action cut-board-large
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?s2 ?size_after - aboardsize
	    ?m - highspeed-saw
	)
	:duration (= ?duration 20)
	:condition
    (and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p large))
	    (at start (boardsize-successor ?size_after ?s1))
	    (at start (boardsize-successor ?s1 ?s2))
	    (at start (boardsize-successor ?s2 ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (in-highspeed-saw ?b ?m))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
    (:durative-action do-saw-small
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?size_after - aboardsize
	    ?m - saw
	)
	:duration (= ?duration 10)
	:condition
    (and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p small))
	    (at start (boardsize-successor ?size_after ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (available-board ?b))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
    (:durative-action do-saw-medium
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?size_after - aboardsize
	    ?m - saw
	)
	:duration (= ?duration 15)
	:condition
    (and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p medium))
	    (at start (boardsize-successor ?size_after ?s1))
	    (at start (boardsize-successor ?s1 ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (available-board ?b))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
    (:durative-action do-saw-large
	:parameters
	(
	    ?b - board
	    ?p - part
	    ?w - awood
	    ?surface - surface
	    ?size_before ?s1 ?s2 ?size_after - aboardsize
	    ?m - saw
	)
	:duration (= ?duration 20)
	:condition
	(and
	    (at start (woodboard ?b ?w))
	    (at start (goalsize ?p large))
	    (at start (boardsize-successor ?size_after ?s1))
	    (at start (boardsize-successor ?s1 ?s2))
	    (at start (boardsize-successor ?s2 ?size_before))
	    (at start (unused ?p))
	    (at start (surface-condition ?b ?surface))
	    (at start (boardsize ?b ?size_before))
	    (at start (available-board ?b))
	    (at start (available ?m))
    )
	:effect
	(and
	    (at start (not (unused ?p)))
	    (at start (not (available ?m)))
	    (at end (available-part ?p))
	    (at end (woodpart ?p ?w))
	    (at end (surface-condition ?p ?surface))
	    (at end (colour ?p natural))
	    (at end (treatment ?p untreated))
	    (at end (boardsize ?b ?size_after))
	    (at end (available ?m))
	)
    )
)

