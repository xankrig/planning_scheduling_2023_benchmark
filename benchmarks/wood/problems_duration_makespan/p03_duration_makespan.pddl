; woodworking task with 24 parts and 140% wood
; Machines:
;   1 grinder
;   1 glazer
;   1 immersion-varnisher
;   1 planer
;   1 highspeed-saw
;   1 spray-varnisher
;   1 saw
; random seed: 490926
(define (problem wood-prob)
  (:domain woodworking-ps)
  (:objects
    grinder0 - grinder
    glazer0 - glazer
    immersion-varnisher0 - immersion-varnisher
    planer0 - planer
    highspeed-saw0 - highspeed-saw
    spray-varnisher0 - spray-varnisher
    saw0 - saw
    black mauve green blue red white - acolour
    beech pine mahogany walnut cherry teak - awood
    p0 p1 p2 p3 p7 - part
    b0 b1 b2 b3 b4 b5 b6 b7 b8 b9 - board
    s0 s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11 s12 - aboardsize
  )
  (:init
		(available grinder0)
		(available glazer0)
		(available immersion-varnisher0)
		(available planer0)
		(available highspeed-saw0)
		(available spray-varnisher0)
		(available saw0)
	(= (load-time highspeed-saw0 b0) 7)
	(= (load-time highspeed-saw0 b1) 9)
	(= (load-time highspeed-saw0 b2) 5)
	(= (load-time highspeed-saw0 b3) 9)
	(= (load-time highspeed-saw0 b4) 10)
	(= (load-time highspeed-saw0 b5) 10)
	(= (load-time highspeed-saw0 b6) 6)
	(= (load-time highspeed-saw0 b7) 9)
	(= (load-time highspeed-saw0 b8) 5)
	(= (load-time highspeed-saw0 b9) 10)
    (grind-treatment-change varnished colourfragments)
    (grind-treatment-change glazed untreated)
    (grind-treatment-change untreated untreated)
    (grind-treatment-change colourfragments untreated)
    (is-smooth smooth)
    (is-smooth verysmooth)
    (boardsize-successor s0 s1)
    (boardsize-successor s1 s2)
    (boardsize-successor s2 s3)
    (boardsize-successor s3 s4)
    (boardsize-successor s4 s5)
    (boardsize-successor s5 s6)
    (boardsize-successor s6 s7)
    (boardsize-successor s7 s8)
    (boardsize-successor s8 s9)
    (boardsize-successor s9 s10)
    (boardsize-successor s10 s11)
    (boardsize-successor s11 s12)
    (has-colour glazer0 blue)
    (has-colour glazer0 mauve)
    (has-colour glazer0 white)
    (has-colour glazer0 natural)
    (has-colour glazer0 red)
    (has-colour immersion-varnisher0 blue)
    (has-colour immersion-varnisher0 mauve)
    (has-colour immersion-varnisher0 natural)
    (has-colour immersion-varnisher0 red)
    (empty highspeed-saw0)
    (has-colour spray-varnisher0 blue)
    (has-colour spray-varnisher0 mauve)
    (has-colour spray-varnisher0 natural)
    (has-colour spray-varnisher0 red)
    (unused p0)
    (goalsize p0 large)
	(= (processing-spray-varnish-time spray-varnisher0 p0) 3)
	(= (processing-immersion-varnish-time immersion-varnisher0 p0) 3)
	(= (processing-glaze-time glazer0 p0) 3)
	(= (processing-grind-time grinder0 p0) 7)
	(= (processing-plane-time planer0 p0) 5)
    (unused p1)
    (goalsize p1 medium)
	(= (processing-spray-varnish-time spray-varnisher0 p1) 2)
	(= (processing-immersion-varnish-time immersion-varnisher0 p1) 3)
	(= (processing-glaze-time glazer0 p1) 3)
	(= (processing-grind-time grinder0 p1) 5)
	(= (processing-plane-time planer0 p1) 3)
    (unused p2)
    (goalsize p2 medium)
	(= (processing-spray-varnish-time spray-varnisher0 p2) 2)
	(= (processing-immersion-varnish-time immersion-varnisher0 p2) 3)
	(= (processing-glaze-time glazer0 p2) 3)
	(= (processing-grind-time grinder0 p2) 5)
	(= (processing-plane-time planer0 p2) 3)
    (available-part p3)
    (colour p3 green)
    (woodpart p3 pine)
    (surface-condition p3 verysmooth)
    (treatment p3 varnished)
    (goalsize p3 small)
	(= (processing-spray-varnish-time spray-varnisher0 p3) 1)
	(= (processing-immersion-varnish-time immersion-varnisher0 p3) 3)
	(= (processing-glaze-time glazer0 p3) 2)
	(= (processing-grind-time grinder0 p3) 3)
	(= (processing-plane-time planer0 p3) 2)
    (unused p7)
    (goalsize p7 large)
	(= (processing-spray-varnish-time spray-varnisher0 p7) 3)
	(= (processing-immersion-varnish-time immersion-varnisher0 p7) 3)
	(= (processing-glaze-time glazer0 p7) 3)
	(= (processing-grind-time grinder0 p7) 7)
	(= (processing-plane-time planer0 p7) 5)
    (boardsize b0 s9)
    (woodboard b0 cherry)
    (surface-condition b0 rough)
    (available-board b0)
    (boardsize b1 s12)
    (woodboard b1 mahogany)
    (surface-condition b1 rough)
    (available-board b1)
    (boardsize b2 s7)
    (woodboard b2 mahogany)
    (surface-condition b2 smooth)
    (available-board b2)
    (boardsize b3 s9)
    (woodboard b3 pine)
    (surface-condition b3 rough)
    (available-board b3)
    (boardsize b4 s8)
    (woodboard b4 walnut)
    (surface-condition b4 rough)
    (available-board b4)
    (boardsize b5 s6)
    (woodboard b5 walnut)
    (surface-condition b5 rough)
    (available-board b5)
    (boardsize b6 s10)
    (woodboard b6 teak)
    (surface-condition b6 rough)
    (available-board b6)
    (boardsize b7 s3)
    (woodboard b7 teak)
    (surface-condition b7 rough)
    (available-board b7)
    (boardsize b8 s8)
    (woodboard b8 beech)
    (surface-condition b8 smooth)
    (available-board b8)
    (boardsize b9 s6)
    (woodboard b9 beech)
    (surface-condition b9 rough)
    (available-board b9)
  )
  (:goal
    (and
      (available-part p0)
      (woodpart p0 cherry)
      (treatment p0 varnished)
      (available-part p1)
      (colour p1 blue)
      (treatment p1 glazed)
      (available-part p2)
      (woodpart p2 beech)
      (treatment p2 varnished)
      (available-part p3)
      (colour p3 red)
      (woodpart p3 pine)
      (surface-condition p3 smooth)
      (treatment p3 glazed)
      (available-part p7)
      (colour p7 red)
      (woodpart p7 beech)
      (surface-condition p7 smooth)
      (treatment p7 varnished)
    )
  )
  (:metric minimize (total-time))
)
