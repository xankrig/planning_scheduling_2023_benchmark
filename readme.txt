The benchmarks are grouped in respective folders - Reconfigurable Machines, Tube factory and Woodworking (rmt, tube and wood folders respectively).

The "domains" subfolders contain domain files. Note that "_ps" suffix represent PS task, "ps_classical" suffix its compilation to classical planning and "ps_duration_makespan" suffix its compilation to temporal planning. Problem instances are in separate folders - the folder suffixes are the same (with the same meaning) as above. 


