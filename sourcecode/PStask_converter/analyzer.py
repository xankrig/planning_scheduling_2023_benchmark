import re

import parsers as ps
import helpers as he

keywords = ["(:types", "(:requirements", "(:predicates", ":ps-task", "(:attributes", ":attributes",
                "(:production-activity", "(:maintenance-activity"]
types = ["object"]
predicates = []
attributes = []
statics = []
functions = []
all_predicates = {}
subtypes = {}
current_types = []

def magic_check(info):
    if info in keywords:
        match info[0]:
            case "(:requirements":
                flag = 1
            case "(:types":
                flag = 2
                print("Parsing types...")
            case "(:attributes":
                flag = 3
                print("Parsing attributes...")
            case "(:statics":
                flag = 4
                print("Parsing static predicates...")
            case "(:predicates":
                flag = 5
                print("Parsing predicates...")
            case "(:functions":
                flag = 6
                print("Parsing functions...")
            case "(:maintenance-activity" | "(:production-activity":
                flag = 7
                print("Parsing activity...")
    return flag

def activity_magic_check(info):
    if info in keywords:
        match info[0]:
            case ":parameters":
                flag = 1
                print("Parsing activity params...")
            case ":attributes":
                flag = 2
                print("Parsing activity attributes...")
            case ":duration":
                flag = 3
                print("Parsing activity duration...")
            case ":static":
                flag = 4
                print("Parsing activity static predicates...")
            case ":precondition":
                flag = 5
                print("Parsing preconditions...")
            case ":del-effect" | ":rem-effect":
                flag = 6
                print("Parsing del-effects...")
            case ":add-effect":
                flag = 7
                print("Parsing add-effects...")
    return flag

def one_check(info, line, all_text, comment, flag):
    if info in keywords:
        match info:
            case "(:types":
                flag = 7
                print("Parsing types...")
                all_text.append(line)
            case "(:requirements":
                if ":ps-task" in info:
                    info.remove(":ps-task")
                elif ":ps-task)" in info:
                    info[info.index(":ps-task)") - 1] = f"{info[info.index(':ps-task)') - 1]})"
                    info.remove(":ps-task)")
                    print("PS task specification found, continuing")
                    info.append(comment)
                    all_text.append(" ".join(info))
                else:
                    raise Exception("PS task specification not found, closing the program")
            case "(:attributes":
                flag = 1
                print("Parsing attributes...")
            case "(:predicates":
                flag = 2
                print("Parsing predicates...")
                all_text.append(line)
            case "(:maintenance-activity" | "(:production-activity":
                flag = 3
                print("Parsing activity...")
                info[0] = "    (:durative-action"
                info.append(comment)
                all_text.append(" ".join(info) + "\n")
    else: raise Exception("Unknown keyword") #all_text.append(line)
    return flag

def line_analyze(line, info, all_text, flag, multiflag, blockflag, num_brackets, saved_info):
    def standard_block(elem, savedblock, num_brackets, multiflag, blockflag):
        if elem == ")" and num_brackets == 1:
            blockflag = he.finalize_preds(all_text, savedblock)
        elif elem == ")" and num_brackets > 1:
            savedblock.append(saved_info.copy())
            saved_info.clear()
            num_brackets -= 1
        elif elem.count(")") == len(elem) and num_brackets > 1:
            blockflag, num_brackets = he.preds_multiple_finish(elem.count(")"), all_text, savedblock, saved_info,
                                                               num_brackets, all_predicates, current_types)
        elif elem.count(")") == num_brackets and num_brackets > 1:
            saved_info.append(elem[:-2])
            blockflag, num_brackets = he.preds_multiple_finish(elem.count(")"), all_text, savedblock, saved_info,
                                                               num_brackets, all_predicates, current_types)
        elif elem == "(":
            num_brackets += 1
        elif elem[0] == "(":
            saved_info.append(elem[1:])
        else:
            if multiflag == 1:
                if elem not in subtypes.keys():
                    raise Exception(f"Unknown type '{info}' provided")
                current_types.append(elem)
                multiflag = 0
            elif elem[0] == "-":
                multiflag = 1
            elif elem[-1] == ")":
                elem = elem[:-1]
            saved_info.append(elem)
        return num_brackets, multiflag, blockflag

    if len(info) == 0:
        all_text.append(line)
    else:
        lineflag = 0
        if blockflag == 0:
            flag = magic_check(info[0])
            blockflag = 1
            lineflag = 1
            num_brackets += 1 #make a parser - divide for cases - go for elem in line
        for i in range(len(lineflag, info)):
            elem = info[i]
            match flag:
                case 1: #requirements
                    if elem == ")":
                        num_brackets, blockflag = he.finalize_reqs(saved_info, all_text, num_brackets)
                    else:
                        if elem[0] != ":":
                            raise Exception("Incorrect requirement")
                        elif elem.count(")") == num_brackets:
                            saved_info.append(elem[:-1])
                            num_brackets, blockflag = he.finalize_reqs(saved_info, all_text, num_brackets)
                        else:
                            saved_info.append(elem)
                case 2: #types
                    if elem == ")":
                        num_brackets, blockflag = he.finalize_types(num_brackets, all_text, subtypes) # -1 0
                    else:
                        if elem == "-":
                            multiflag = 1
                        elif multiflag == 1:
                            multiflag = he.full_check_types(elem, types, saved_info, subtypes) #0
                        elif multiflag == 1 and elem.count(")") == num_brackets:
                            multiflag = he.full_check_types(elem, types, saved_info, subtypes)
                            num_brackets, blockflag = he.finalize_types(num_brackets, all_text, subtypes)
                        else:
                            saved_info.append(elem)
                case 3: #attributes
                    num_brackets, multiflag, blockflag = standard_block(elem, attributes, num_brackets, multiflag, blockflag)
                case 4: #statics
                    num_brackets, multiflag, blockflag = standard_block(elem, statics, num_brackets, multiflag, blockflag)
                case 5: #predicates
                    num_brackets, multiflag, blockflag = standard_block(elem, predicates, num_brackets, multiflag, blockflag)
                case 6: #functions
                    num_brackets, multiflag, blockflag = standard_block(elem, functions, num_brackets, multiflag, blockflag)
                case 7: #activities
                    if blockflag == 1:
                        a_flag = activity_magic_check(elem)
                        blockflag = 2
                    else:

                        return
                    return
    return flag, multiflag, num_brackets, blockflag

def analyze(info, flag, a_flag, multiflag, line, resources, saved_parameters, saved_attributes, all_text, comment, identablock):
    match multiflag:
        case 1:
            return #ps.block_divider()
        case 2:
            return
        case 3:
            return
        case _:
            match flag:  # parse pddl blocks
                case 1:
                    flag = ps.attr_parsing(info, flag, saved_attributes, comment)  # attributes
                case 2:
                    flag = ps.pred_parsing(info, flag, line, all_text, saved_attributes, resources, comment)  # predicates
                case 3:
                    flag, a_flag = ps.activity_parsing(info, flag, a_flag, line, resources,
                                                       saved_parameters, saved_attributes, all_text)  # activities
                case 7:  # check resources in types
                    if info[-1] == "resource":
                        resources.append(info[0])
                    elif info[0] == ")":
                        flag = 0
                    all_text.append(line)
                case _:  # line not in block(empty or comment)/begin of block
                    flag, multiflag = line_analyze(line, info, all_text, comment, flag)

def analyze_old(info, flag, a_flag, line, resources, saved_parameters, saved_attributes, all_text, comment):
    match flag:  # parse pddl blocks
        case 1:
            flag = ps.attr_parsing(info, flag, saved_attributes, comment)  # attributes
        case 2:
            flag = ps.pred_parsing(info, flag, line, all_text, saved_attributes, resources, comment)  # predicates
        case 3:
            flag, a_flag = ps.activity_parsing(info, flag, a_flag, line, resources,
                                                     saved_parameters, saved_attributes, all_text)  # activities
        case 7:  # check resources in types
            if info[-1] == "resource":
                resources.append(info[0])
            elif info[0] == ")":
                flag = 0
            all_text.append(line)
        case _:  # line not in block(empty or comment)/begin of block
            flag, multiflag = line_analyze(line, info, all_text, comment, flag)
    return flag, a_flag
