types = []
types_parented = {}

def write_classic(filename, path, data):
    new_file = filename.split(".")[0]
    new_file += "_classic.pddl"
    new_path = f"{path}\\{new_file}"
    with open(new_path, "w") as f:
        for item in data:
            f.write(item)

def write_cost(filename, path, data):
    new_file = filename.split(".")[0]
    new_file += "_duration_cost.pddl"
    new_path = f"{path}\\{new_file}"
    with open(new_path, "w") as f:
        for item in data:
            f.write(item)

def adapt_classic(filename, path, temporalfile):
    keywords = ["(:types", "(:requirements", "(:predicates", "(:functions", "(:durative-action"]
    with open(temporalfile) as f:
        new_text = []
        new_text_cost = []
        flag = 0
        saved_functions = {}
        effect = False
        dur = False
        remembered_function = ""
        for line in f:
            info = line.split(";")[0].split()
            if info == [] or line == "\n" or line.replace(" ", "") == "\n":
                new_text.append(line)
            elif info[0] in keywords:
                match info[0]:
                    case "(:requirements":
                        new_text.append(f"    (:requirements :strips :typing :conditional-effects :negative-preconditions :equality)\n")
                        new_text_cost.append(f"    (:requirements :strips :fluents :durative-actions :timed-initial-literals :typing :conditional-effects :negative-preconditions :duration-inequalities :equality :action-costs)\n")
                        continue
                    case "(:predicates":
                        flag = 1
                    case "(:functions":
                        flag = 2
                    case "(:durative-action":
                        new_text.append(f"    (:action {info[1]}\n")
                        flag = 3
                if info[0] != "(:durative-action":
                    new_text.append(line)
            else:
                match flag:
                    case 0:
                        new_text.append(line)
                    case 1:
                        if info[0] == "(available":
                            new_text_cost.append(line)
                            continue
                        elif info[0] == ")":
                            flag = 0
                            new_text.append(line)
                        else:
                            new_text.append(line)
                    case 2:
                        if info == [] or line == "\n" or line.replace(" ", "") == "\n":
                            continue
                        elif info[0] == ")":
                            new_text.append(f"\t    (total-cost)\n")
                            new_text_cost.append(f"\t    (total-cost)\n")
                            new_text.append(line)
                            flag = 0
                        else:
                            new_text.append(line)

                    case 3:
                        match info[0]:
                            case ":parameters":
                                flag = 4
                                new_text.append(line)
                            case ":duration":
                                if info[3][-1] != ')':

                                    todel = info[3:]
                                    todel[-1] = todel[-1][:-2]
                                    todelflag = False
                                    for delete in new_text:
                                        first = delete.strip().split(" ")[0]
                                        if todelflag:
                                            if first == ")":
                                                break
                                            else:
                                                if first == todel[0]:
                                                    new_text.remove(delete)
                                                    break
                                        else:
                                            if first != "(:functions":
                                                continue
                                            else:
                                                todelflag = not todelflag
                                                continue
                                types.clear()
                                types_parented.clear()
                            case ":cost":
                                if info[3][-1] == ')':
                                    remembered_function = info[3][:-1]
                                    dur = True
                                else:
                                    remembered_function = f"{info[3][1:]} {info[4]} {info[5]}"
                                continue
                            case ":condition":
                                new_text.append(f"\t:precondition\n")
                            case "(at":
                                if "(available" in info:
                                    new_text_cost.append(line)
                                    continue
                                else:
                                    info[-1] = info[-1][:-1]
                                    del info[0:2]
                                    new_text.append(f"\t    {f' '.join(info)}\n")
                            case ":effect":
                                effect = not effect
                                new_text.append(line)
                            case ")":
                                if effect:
                                    if not dur:
                                        new_text.append(f"\t    (increase (total-cost) ({remembered_function}\n")
                                        new_text_cost.append(f"\t    (at end (increase (total-cost) ({remembered_function})\n")
                                    else:
                                        new_text.append(f"\t    (increase (total-cost) {remembered_function})\n")
                                        new_text_cost.append(f"\t    (at end (increase (total-cost) {remembered_function}))\n")
                                    remembered_function = ""
                                    effect = not effect
                                    dur = False
                                new_text.append(line)
                                #flag = 0
                            case _:
                                new_text.append(line)

                    case 4:
                        info = line.replace("\t", "").replace("\n", "").split(" ")
                        if info[0] != ")" and info[0] != "(":
                            remembered = []
                            linetypes = line.strip().split(" ")
                            for type in linetypes:
                                if type not in types and type not in ['-', ')']:
                                    types.append(type)
                                if type == "-":
                                    continue
                                if type == linetypes[-1]:
                                    for subtype in remembered:
                                        types_parented[subtype] = type
                                    if linetypes[-1] == "resource":
                                        resources = remembered.copy()
                                    remembered.clear()
                                else:
                                    remembered.append(type)
                        elif info[0] == ")":
                            flag = 3
                        new_text.append(line)
            new_text_cost.append(line)

    write_classic(filename, path, new_text)
    write_cost(filename, path, new_text_cost)