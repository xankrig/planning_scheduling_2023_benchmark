def check_type_exists(info, typelist):
    if info not in typelist:
        raise Exception(f"Unknown type '{info}' provided in attributes")

def finalize_reqs(info, text, brackets):
    brackets -= 1
    text.append(f"    (:requirements {f' '.join(info)}\n")
    info.clear()
    return brackets, 0

def full_check_types(elem, types, saved_info, subtypes):
    check_type_exists(elem, types)
    for type in saved_info:
        types.append(type)
    subtypes[elem] = saved_info.copy()
    saved_info.clear()
    return 0

def finalize_types(brackets, text, subtypes):
    text.append(f"    (:types\n")
    for key, value in subtypes:
        text.append(f"\t{f' '.join(value)} - {key}\n")
    text.append(f"    )\n")
    brackets -= 1
    return brackets, 0

def preds_multiple_finish(elem_count, text, block, info, brackets, dict_preds, types):
    block.append(info.copy())
    dict_preds[info[0]] = types.copy()
    info.clear()
    types.clear()
    brackets -= elem_count
    return finalize_preds(text, block), brackets

def finalize_preds(text, info):
    text.append(f"    (\n")
    for pred in info:
        text.append(f"\t ({f' '.join(pred)})\n")
    text.append(f"    )\n")
    return 0