#!/usr/bin/env python3
import argparse
import random
import os
import math
import parsers as ps
import classicadapter as ca
#import analyzer as anz


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help="input absolute path with demands input file")
    return parser.parse_args()


def write_file(filename, data):
    new_file = filename.split(".")[0]
    new_file += "_duration_makespan.pddl"
    temp = filename.split(".")[0]
    temp += "_temp.pddl"
    path = os.path.dirname(args.input_file)
    new_path = f"{path}\\{new_file}"
    temp_path = f"{path}\\{temp}"
    with open(new_path, "w") as f:
        for item in data:
            check = item.replace('\t', "").replace("\n", "").split(" ")
            while "" in check: check.remove('')
            if check[0] != ":cost" and "cost" not in check[0].split("-"):
                f.write(item)
    with open(temp_path, "w") as f:
        for item in data:
            f.write(item)
    return temp_path

def main():
    filename = os.path.basename(args.input_file)
    keywords = ["(:types", "(:constants", "(:requirements", "(:predicates", "(:functions", ":ps-task", "(:attributes", ":attributes",
                "(:static", "(:production-activity", "(:maintenance-activity"]
    flagger = 0 #for blocks in domain
    a_flagger = 0 #for subblocks in activity
    multiflagger = 0
    all_text = []
    resources = []
    pr_resources = []
    with open(args.input_file) as f:
        saved_attributes = []
        saved_parameters = []
        types = ["object"]
        types_parented = {"object": ""}
        all_attributes = []
        action_params = []
        action_attributes = []
        a_name = ""
        #identablock = []
        for line in f:
            info = line.split(";")[0].split()
            #comment = ""
            #if len(line.split(";")) == 2: #line with comment
            #    comment = f";{line.split(';')[1]}"
            if info == [] or line == "\n" or line.replace(" ", "") == "\n":
                continue
            #flagger, a_flagger = anz.analyze(info, flagger, a_flagger, multiflagger, line, resources, saved_parameters,
            #                                 saved_attributes, all_text, comment, identablock)
            if multiflagger != 0:
                continue
            else:
                match flagger: #parse pddl blocks
                    case 1 | 8:
                        flagger = ps.attr_parsing(line, info, flagger, saved_attributes, types) #attributes
                    case 2 | 6:
                        flagger = ps.pred_parsing(info, flagger, line, all_text, saved_attributes, resources, types, all_attributes) #predicates
                    case 3 | 4:
                        flagger, a_flagger = ps.activity_parsing(info, flagger, a_flagger, line, resources,
                                                                 saved_parameters, saved_attributes, all_text,
                                                                 all_attributes,
                                                                 action_params, action_attributes, types, a_name, types_parented) #activities
                    case 7 | 9: #check resources in types
                        remembered = []
                        if info[0] == ")":
                            flagger = 0
                        all_text.append(line)
                        linetypes = line.strip().split(" ")
                        for type in linetypes:
                            if type not in types and type not in ['-', ')']:
                                types.append(type)
                            if type == "-":
                                continue
                            if type == linetypes[-1]:
                                for subtype in remembered:
                                    types_parented[subtype] = type
                                if linetypes[-1] == "resource":
                                    resources = remembered.copy()
                                remembered.clear()
                            else:
                                remembered.append(type)
                    case _: #line not in block(empty or comment)/begin of block
                        if info == []: #and comment != "":
                            continue
                            #all_text.append("\n")
                        elif info[0] in keywords:
                            match info[0]:
                                case "(:types":
                                    flagger = 7
                                    print("Parsing types...")
                                    all_text.append(f"    (:types\n")
                                case "(:constants":
                                    flagger = 9
                                    print("Parsing constant types...")
                                    all_text.append(f"    (:constants\n")
                                case "(:requirements":
                                    if ":ps-task" in info:
                                        info.remove(":ps-task")
                                        all_text.append(" ".join(info) + "\n")
                                    elif ":ps-task)" in info:
                                        info[info.index(":ps-task)") - 1] = f"{info[info.index(':ps-task)') - 1]})"
                                        info.remove(":ps-task)")
                                        print("PS task specification found, continuing")
                                        #info.append(comment)
                                        all_text.append(" ".join(info) + "\n")
                                    else:
                                        raise Exception("PS task specification not found, closing the program")
                                case "(:attributes":
                                    flagger = 1
                                    print("Parsing attributes...")
                                case "(:predicates":
                                    flagger = 2
                                    print("Parsing predicates...")
                                    all_text.append(f"    (:predicates\n")
                                case "(:functions":
                                    flagger = 6
                                    print("Parsing functions...")
                                    all_text.append(f"    (:functions\n")
                                case "(:static":
                                    flagger = 8
                                    print("Parsing static attributes...")
                                    #all_text.append(f"    (:functions\n")
                                case "(:maintenance-activity" | "(:production-activity":
                                    if info[0] == "(:maintenance-activity":
                                        flagger = 3
                                    else: flagger = 4
                                    print("Parsing activity...")
                                    info[0] = "    (:durative-action"
                                    #info.append(comment)
                                    all_text.append(" ".join(info) + "\n")
                                    a_name = " ".join(info)
                        else:
                            all_text.append(line.split(";")[0] + "\n")
    temporalfile = write_file(filename, all_text)
    for key, item in types_parented.items():
        if item == "resource" or item in pr_resources:
            pr_resources.append(key)
    new_file = filename.split(".")[0]
    new_file += "_resources.txt"
    path = os.path.dirname(args.input_file)
    new_path = f"{path}\\{new_file}"
    with open(new_path, "w") as f:
        for item in pr_resources:
            f.write(item)
            f.write(f"\n")

    ca.adapt_classic(filename, path, temporalfile)

if __name__ == '__main__':
    args = parse_arguments()
    main()