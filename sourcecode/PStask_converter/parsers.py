import re

def check_attr_len(attr):
    if len(attr) == 4:
        if attr[1][0] != "?" or attr[2] != "-":
            raise Exception("Wrong attribute specification")

def check_var(data, typelist):
    naming = 0
    for info in data:
        if naming in (1, 2):
            match info[0]:
                case "-":
                    naming = 2
                case "?":
                    if naming == 2:
                        raise Exception(f"Wrong type '{info}' definition provided in attributes")
                case _:
                    if info[-1] == ")": info = info.replace(")", "")
                    if info not in typelist:
                        raise Exception(f"Unknown type '{info}' provided in attributes")
                    naming = 0
        elif info[0] == "?":
            naming = 1

def attr_parsing(line, info, flagger, saved_attributes, types):

    if info[0] == ")":
        flagger = 0
    elif (line.count("(") - line.count(")") == 0 and line.count("(") > 1) or (line.count("(") - line.count(")") == -1 and len(info) > 2):
        regdata_init = re.split("\)(.*?)\(", line.strip().split(";")[0])
        regdata_init[0] += ")"
        regdata_init[-1] = f"({regdata_init[-1]}"
        regdata = []
        for x in regdata_init:
            if x.strip() != "":
                check_attr_len(x.strip().split(" "))
                regdata.append(x)
        if flagger == 0: regdata[-1] = regdata[-1].strip()[:-1]
        for i in range(1, len(regdata) - 1):
            regdata[i] = f"({regdata[i]})"
        for i in range(0, len(regdata)):
            check_var(regdata[i].strip().split(" "), types)
        for subinfo in regdata:
            saved_attributes.append(subinfo.strip().split(' '))
        if line.count("(") - line.count(")") == -1:
            saved_attributes[-1][-1] = saved_attributes[-1][-1][:-1]
            flagger = 0
    elif info[-1][-1] == info[-1][-2] == ")":
        flagger = 0
        info[-1] = info[-1][:-1]
    else:
        check_attr_len(info)
        check_var(info, types)
        saved_attributes.append(info)
    return flagger


def pred_parsing(info, flagger, line, all_text, saved_attributes, resources, types, attributes):
    def attr_dict_filler(info, atrributes):
        if len(info) == 7:
            attributes.append({"name": info[0].replace("(", ""), "type1": info[3], "type2": info[6].replace(")", "")})
        else:
            attributes.append({"name": info[0].replace("(", ""), "type": info[3].replace(")", "")})

    def final_append(flagger):
        for attr in saved_attributes:
            attr_dict_filler(attr, attributes)
            all_text.append(f"\t    " + f" ".join(attr) + f"\n")
        saved_attributes.clear()
        if flagger == 2:
            attr_dict_filler(["(available", f"?r", "-", "resource"], attributes)
            all_text.append(f"\t    (available ?r - resource)\n")
            resources.clear()
            #saved_attributes.clear()
        all_text.append(f"    )")
        return 0

    if info == []:# and comment != "":
        return flagger
    elif (line.count("(") - line.count(")") == 0 and line.count("(") > 1) or (line.count("(") - line.count(")") == -1 and len(info) > 2):
        regdata_init = re.split("\)(.*?)\(", line.strip().split(";")[0])
        regdata_init[0] += ")"
        regdata_init[-1] = f"({regdata_init[-1]}"
        regdata = []
        for x in regdata_init:
            if x.strip() != "":
                check_attr_len(x.strip().split(" "))
                regdata.append(x)
        for i in range(1, len(regdata) - 1):
            regdata[i] = f"({regdata[i]})"
        for i in range(0, len(regdata)):
            check_var(regdata[i].strip().split(" "), types)
            attr_dict_filler(regdata[i].strip().split(" "), attributes)
        for subinfo in regdata:
            all_text.append(f"\t    {subinfo}\n")
        if line.count("(") - line.count(")") == -1:
            all_text[-1] = f"{all_text[-1][:-2]}\n"
            flagger = final_append(flagger)
    elif info[0] == ")" or info[-1][-1] == info[-1][-2] == ")":
        flagger = final_append(flagger)
    #info.append(comment)
    else:
        check_attr_len(info)
        check_var(info, types)
        attr_dict_filler(info, attributes)
        all_text.append(f"\t    " + f" ".join(info))
    if all_text[-1][-1] != "\n":
        all_text[-1] = f"{all_text[-1]}\n"
    return flagger


def activity_parsing(info, flagger, a_flagger, line, resources, saved_parameters, saved_attributes, all_text, all_attributes,
                     a_params, a_attributes, types, a_name, types_parented):


    def final_write(all_text, saved_attributes): #write effects and finish with activity
        all_text.append("\t:effect\n")
        all_text.append("\t(and\n")
        for i in saved_attributes:
            all_text.append(f"\t    {i}")
        all_text.append(f"\t)\n")
        saved_attributes.clear()
        a_params.clear()
        a_attributes.clear()
        resources.clear()
        all_text.append(f"    )\n")
        return 0, 0


    def check_types(params, type, typelist):
        if type not in typelist:
            raise Exception(f"Unknown type {type} provided in activity {a_name}")
        for info in params:
            if info == "-":
                break
            else:
                a_params.append({info: type})


    def check_attr_naming(attribute, attrlist):
        finder = list(filter(lambda attr: attr['name'] == attribute, attrlist))
        if len(finder) == 0:
            raise Exception(f"Unknown atttribute {attribute} found in activity {a_name}")
        else:
            return finder[0]


    def check_var_existed(var, var_pos, vars, template):
        if var[-1] == ")": var = var.replace(")", "")
        if len(template) == 3:
            name = f"type{var_pos}"
        else: name = f"type"
        needed_type = template[name]
        accepted_types = [needed_type]
        if var in types:
            return
        else: received_type = vars[next(i for i, param in enumerate(vars) if var in param)][var]
        if received_type != needed_type:
            while True:
                parent = types_parented[received_type]
                if parent == needed_type:
                    accepted_types.append(received_type)
                    break
                else:
                    accepted_types.append(received_type)
                    received_type = parent
        if not any(param.get(var, "") in accepted_types for param in vars):
            raise Exception(f"Unknown or wrong type variable {var} found in activity {a_name}")


    def attr_appender(info, i, new_attr):
        if info[i][0] == "(":
            info[i] = info[i].replace("(", "")
            template = check_attr_naming(info[i], all_attributes)
            new_attr.append(f"({info[i]}")
            for j in range(i + 1, i + len(template)):
                check_var_existed(info[j], j - i, a_params, template)
                new_attr.append(info[j])
            if new_attr[-1][-1] == new_attr[-1][-2] == ")":
                new_attr[-1] = new_attr[-1][:-1]
            new_attr[-1] += "\n"
            saved_attributes.append(" ".join(new_attr))
            new_attr.clear()
            return len(template)


    def precond_check(data):
        attribute = data.split()[0]
        for x in ["(", ")"]:
            attribute = attribute.replace(x, "")
        template = check_attr_naming(attribute, all_attributes)
        for j in range(1, len(template)):
            check_var_existed(data.split()[j], j, a_params, template)


    def finalize_precond(fin_line):
        for i in saved_attributes:
            i = i[:-1]
            all_text.append(f"\t    (at start {i})\n")
        for resource in resources:
            all_text.append(f"\t    (at start (available {resource}))\n")
        all_text.append(fin_line)
        saved_attributes.clear()
        return 0


    keywords = [":parameters", ":resource", ":attributes", ":duration", ":precondition", ":rem-effect", ":del-effect",
                ":add-effect", ":static", ":cost"]
    match a_flagger:
        case 1 | 7: #parameters, resources
            if info[0] == "(":
                return flagger, a_flagger
            elif info[0] == ")":
                a_flagger = 0
            else:
                in_flag = False
                info[0] = info[0].replace("(", "")
                if info[-1][-1] == ")":
                     info[-1] = info[-1].replace(")", "")
                     in_flag = True
                elif info[-1].strip() == ")":
                    info.remove(-1)
                    in_flag = True
                gathered_info = []
                for i in range(0, len(info) - 1):
                    if i > 1 and info[i - 1] == "-":
                        continue
                    gathered_info.append(info[i].strip())
                    if info[i].strip() == "-":
                        gathered_info.append(info[i + 1].strip())
                        check_types(gathered_info, gathered_info[-1], types)
                        saved_parameters.append(f"\t    " + f" ".join(gathered_info) + "\n")
                        if a_flagger == 7 or types_parented[gathered_info[-1]] == "resource":
                            for data in gathered_info:
                                if data == "-":
                                    break
                                else:
                                    resources.append(data)
                        gathered_info.clear()
                if in_flag:
                    a_flagger = 0
            return flagger, a_flagger
        case 2: #attributes - specifically
            if len(info) == 0:
                return flagger, a_flagger
            if info[0] == "(for":
                i = 1
                new_param = []
                while info[i] != "(and" and info[i] != '()': #check all fors
                    new_param.append(info[i])
                    if info[i] == "-":
                        new_param.append(info[i+1])
                        if new_param[-1][-1] == ")": new_param[-1] = new_param[-1][:-1]
                        check_types(new_param, new_param[-1], types)
                        saved_parameters.append(f"\t    " + f" ".join(new_param))
                        saved_parameters[-1] += "\n"
                        if new_param[0] not in resources:
                            resources.append(new_param[0])
                        new_param.clear()
                        i += 1
                    i += 1
                if info[i] == "()":
                    return flagger, a_flagger
                else:
                    i += 1 #check all attributes
                    new_attr = []
                    while i < len(info):
                        i += attr_appender(info, i, new_attr)
            elif info[0] == ")":
                a_flagger = 0
            elif info[0][0] == "(" and info[0] != "(and":
                i = 0
                new_attr = []
                while i < len(info):
                    i += attr_appender(info, i, new_attr)
                #saved_attributes.append(line.strip())
            return flagger, a_flagger
        case 3:
            i = 2
            while i < len(info):
                if info[0] not in ("(=" or "(<" or "(>") or info[1] != "?duration":
                    raise Exception(f"Wrong duration specification in {a_name}")
                if info[i][0] == "(":
                    info[i] = info[i].replace("(", "")
                    template = check_attr_naming(info[i], all_attributes)
                    for j in range(i + 1, i + len(template)):
                        check_var_existed(info[j], j - i, a_params, template)
                    i += len(template)
            all_text.append(f"\t{line.strip().split(';')[0]}\n")
            if info[0] == ")" or info[-1][-1] == info[-1][-1] == ")":
                a_flagger = 0
            return flagger, a_flagger
        case 4 | 8: #do the same - check full (?name ?t1 ?t2) as one entity
            def finalize(fin_line):
                for i in saved_attributes:
                    i = i[:-1]
                    all_text.append(f"\t    (at start {i})\n")
                for resource in resources:
                    all_text.append(f"\t    (at start (available {resource}))\n")
                all_text.append(fin_line)
                saved_attributes.clear()
                return 0

            if info[0] == ")" and a_flagger == 4 and flagger == 4:
                a_flagger = finalize_precond(f"{line}")
            elif info[0] == "()" and flagger == 4:
                all_text.append(f"\t\t(and\n")
                a_flagger = 0
            elif info[0] == ")" and a_flagger == 8 and flagger == 3:
                a_flagger = finalize_precond(f"\t)\n")
            elif info[0] == ")" and flagger == 4:
                a_flagger = 0
            elif flagger == 3 and info[0] != "(and":
                all_text.append(f"\t    (at start {f' '.join(info)})\n")
            elif (info[0] == "(and" and len(info) == 1 and flagger == 4 and a_flagger == 8) or flagger == 3:
                all_text.append(f"{line}")

            elif info[0] == "(and" and len(info) == 1 and flagger == 4 and a_flagger == 4:
                return flagger, a_flagger
            elif (line.count("(") - line.count(")") == 0 and line.count("(") > 1) or \
                    (info[0] == "(and" and line.count("(") > 1) or \
                    (info[-1][-1] == info[-1][-2] == ")" and line.count(")") - line.count("(") == 1):
                regdata_init = re.split("\)(.*?)\(", line.strip().split(";")[0])
                if info[0] == "(and":
                    regdata_init[0] = regdata_init[0].replace("(and ", "")
                    all_text.append("\t(and\n")
                if len(regdata_init) != 1:
                    regdata_init[0] += ")"
                    regdata_init[-1] = f"({regdata_init[-1]}"
                regdata = []
                for x in regdata_init:
                    if x.strip() != "":
                        regdata.append(x)
                for i in range(1, len(regdata) - 1):
                    regdata[i] = f"({regdata[i]})"
                for i in range(len(regdata)):
                    precond_check(regdata[i])
                for subinfo in regdata:
                    all_text.append(f"\t    (at start {subinfo})\n")
                if line.count("(") == line.count(")") and info[0] == "(and":
                    if a_flagger == 4 or flagger == 3:
                        a_flagger = finalize_precond(f"\t)\n")
                    elif a_flagger == 8:
                        a_flagger = 0
                elif (info[-1][-1] == info[-1][-2] == ")" and line.count(")") - line.count("(") == 1):
                    if a_flagger == 4 or flagger == 3:
                        a_flagger = finalize_precond(f"\t\n")
                    elif a_flagger == 8:
                        if all_text[-1][-2] == all_text[-1][-3] == ")":
                            all_text[-1] = all_text[-1][:-2]
                            all_text[-1] = f"{all_text[-1]}\n"
                        a_flagger = 0
            else:
                data = f" ".join(info)
                precond_check(data)
                all_text.append(f"\t    (at start {data})\n")
            return flagger, a_flagger
        case 5 | 6:
            if len(info) == 0:
                return flagger, a_flagger
            elif (info[0] == "(and" or info[0] == "(") and len(info) == 1:
                return flagger, a_flagger
            elif info[0] == ")":
                for resource in resources:
                    if a_flagger == 5:
                        saved_attributes.append(f"(at start (not (available {resource})))\n")
                    else:
                        saved_attributes.append(f"(at end (available {resource}))\n")
                a_flagger = 0
            elif len(info) == 1 and len(info[0]) > 1 and info[0][-1] == ")":
                for resource in resources:
                    if a_flagger == 5:
                        saved_attributes.append(f"(at start (not (available {resource})))\n")
                    else:
                        saved_attributes.append(f"(at end (available {resource}))\n")
                a_flagger = 0
                return final_write(all_text, saved_attributes)
            elif len(info) >= 2 and info[-1][-1] == info[-1][-2] == ")" and info[0] != "(and":
                k = line.count(")") - line.count("(") #we assume, that all blocks are more or less standart
                info[-1] = info[-1][:-k]
                data = f" ".join(info)
                precond_check(data)
                if a_flagger == 5:
                    saved_attributes.append(f"(at start (not {data}))\n")
                else:
                    saved_attributes.append(f"(at end {data})\n")
                for resource in resources:
                    if a_flagger == 5:
                        saved_attributes.append(f"(at start (not (available {resource})))\n")
                    else:
                        saved_attributes.append(f"(at end (available {resource}))\n")
                if k == 1:
                    a_flagger = 0
                elif k == 2:
                    return final_write(all_text, saved_attributes)
            elif (line.count("(") - line.count(")") == 0 and line.count("(") > 1) or (info[0] == "(and" and line.count("(") > 1):
                regdata_init = re.split("\)(.*?)\(", line.strip().split(";")[0])
                if info[0] == "(and":
                    regdata_init[0] = regdata_init[0].replace("(and ", "")
                    #all_text.append("\t(and\n")
                if len(regdata_init) != 1:
                    regdata_init[0] += ")"
                    regdata_init[-1] = f"({regdata_init[-1]}"
                else:
                    regdata_init[0] = regdata_init[0].replace("))", ")")
                regdata = []
                for x in regdata_init:
                    if x.strip() != "":
                        regdata.append(x)
                for i in range(1, len(regdata) - 1):
                    regdata[i] = f"({regdata[i]})"
                for i in range(len(regdata)):
                    precond_check(regdata[i])
                for subinfo in regdata:
                    if a_flagger == 5:
                        saved_attributes.append(f"(at start (not {subinfo}))\n")
                    else:
                        saved_attributes.append(f"(at end {subinfo})\n")
                if len(regdata_init) == 1 and line.count("(") == line.count(")"):
                    for resource in resources:
                        if a_flagger == 5:
                            saved_attributes.append(f"(at start (not (available {resource})))\n")
                        else:
                            saved_attributes.append(f"(at end (available {resource}))\n")
                    a_flagger = 0
            else:
                data = ' '.join(info)
                precond_check(data)
                if a_flagger == 5:
                    saved_attributes.append(f"(at start (not {data}))\n")
                else:
                    saved_attributes.append(f"(at end {data})\n")
            return flagger, a_flagger
    if info[0] in keywords:
        match info[0]:
            case ':parameters' | ':resource': #make it more compact
                if len(info) == 1:
                    a_flagger = 1
                    if info[0] == ":resource":
                        a_flagger = 7
                elif len(info) > 2 and info[-1][-1] == ")" or info[-1] == ")" or info[-2] == "-":
                    info[1] = info[1].replace("(", "")
                    if info[-1][-1] == ")":
                        info[-1] = info[-1].replace(")", "")
                    elif info[-1].strip() == ")":
                        info.remove(-1)
                    else:
                        if info[0] == ":resource":
                            a_flagger = 7
                        else:
                            a_flagger = 1
                    gathered_info = []
                    for i in range(1, len(info) - 1):
                        if i > 1 and info[i - 1] == "-":
                            continue
                        gathered_info.append(info[i].strip())
                        if info[i].strip() == "-":
                            gathered_info.append(info[i+1].strip())
                            check_types(gathered_info, gathered_info[-1], types)
                            i = i+2
                            saved_parameters.append(f"\t    " + f" ".join(gathered_info) + "\n")
                            if info[0] == ":resource":
                                for data in gathered_info:
                                    if data == "-":
                                        break
                                    else:
                                        resources.append(data)
                            gathered_info.clear()
                else:
                    a_flagger = 1
                    if info[0] == ":resource":
                        a_flagger = 7
            case ":attributes": #need to make a parser for it...
                a_flagger = 2
            case ":duration":
                all_text.append("\t:parameters\n\t(\n")
                for i in saved_parameters:
                    all_text.append(i)
                all_text.append("\t)\n")
                saved_parameters.clear()
                if len(info) > 2 and info[-1][-1] == ")":
                    i = 3
                    while i < len(info):
                        if info[1] not in ("(=", "(<", "(>") or info[2] != "?duration":
                            raise Exception(f"Wrong duration specification in {a_name}")
                        if info[i][0] == "(":
                            info[i] = info[i].replace("(", "")
                            template = check_attr_naming(info[i], all_attributes)
                            for j in range(i + 1, i + len(template)):
                                check_var_existed(info[j], j - i, a_params, template)
                            i += len(template) + 1
                        elif type(info[i][:-1]) is str:
                            try:
                                dur = int(info[i][:-1])
                                break
                            except ValueError:
                                raise Exception(f"Wrong duration specification in {a_name}")
                    all_text.append(f"\t{line.strip().split(';')[0]}\n")
                else:
                    if line.split(';')[0][-1] != "\n":
                        all_text.append(f"{line.split(';')[0]}\n")
                    else:
                        all_text.append(f"{line.split(';')[0]}")
                    a_flagger = 3
            case ":cost":
                if len(info) > 2 and info[-1][-1] == ")":
                    i = 3
                    while i < len(info):
                        if info[1] not in ("(=", "(<", "(>") or info[2] != "?cost":
                            raise Exception(f"Wrong cost specification in {a_name}")
                        if info[i][0] == "(":
                            info[i] = info[i].replace("(", "")
                            template = check_attr_naming(info[i], all_attributes)
                            for j in range(i + 1, i + len(template)):
                                check_var_existed(info[j], j - i, a_params, template)
                            i += len(template) + 1
                        elif type(info[i][:-1]) is str:
                            try:
                                cost = int(info[i][:-1])
                                break
                            except ValueError:
                                raise Exception(f"Wrong duration specification in {a_name}")
                    all_text.append(f"\t{line.strip().split(';')[0]}\n")
                else:
                    if line.split(';')[0][-1] != "\n":
                        all_text.append(f"{line.split(';')[0]}\n")
                    else:
                        all_text.append(f"{line.split(';')[0]}")
            case ":precondition" | ":static":

                if len(info) > 2 and info[-1][-1] == ")" or info[-1] == ")":
                    if info[0] == ":static":
                        all_text.append(f"\t:condition\n")
                    if info[1] == "(and" and (flagger != 4 or (flagger == 4 and info[0] == "static")):
                        all_text.append("\t(and\n")
                    else:
                        raise Exception(f"Wrong syntax of precondition in activity {a_name}")
                    i = 2
                    gathered_info = []
                    while i < len(info):
                        gathered_info.append(info[i].strip())
                        if info[i][0] == "(":
                            info[i] = info[i].replace("(", "")
                            template = check_attr_naming(info[i], all_attributes)
                            for j in range(i + 1, i + len(template)):
                                check_var_existed(info[j], j - i, a_params, template)
                                gathered_info.append(info[j].strip())
                            all_text.append(f"\t    (at start " + f" ".join(gathered_info) + ")\n")
                            i += len(template)
                            gathered_info.clear()
                    all_text[-1] = all_text[-1][:-2] + "\n"
                    if info[0] == ":precondition" or (info[0] == ":static" and flagger == 3):
                        finalize_precond(f"\t)\n")
                else:
                    if info[-1] == "()" and info[0] == ":static":
                        all_text.append(f"\t:condition\n")
                        all_text.append(f"\t\t(:and\n")
                    #if line.split(';')[0][-1] != "\n":
                    #    all_text.append(f"{line.split(';')[0]}\n")
                    elif flagger == 3 or (flagger == 4 and info[0] == ":static"):
                        all_text.append(f"\t:condition\n")
                    if len(info) == 2 and info[1] == "(and" and (flagger != 4 or (flagger == 4 and info[0] == ":static")):
                        all_text.append("\t(and\n")
                    if info[0] == ":precondition":
                        a_flagger = 4
                    else: a_flagger = 8
            case ":rem-effect" | ":del-effect": #add full line to these as well...
                if len(info) > 2 and info[-1][-1] == ")" or info[-1] == ")":
                    if (info[1] != "(and"):
                        raise Exception(f"Wrong syntax of removed effect in activity {a_name}")
                    i = 2
                    gathered_info = []
                    while i < len(info):
                        gathered_info.append(info[i].strip())
                        if info[i][0] == "(":
                            info[i] = info[i].replace("(", "")
                            template = check_attr_naming(info[i], all_attributes)
                            for j in range(i + 1, i + len(template)):
                                check_var_existed(info[j], j - i, a_params, template)
                                gathered_info.append(info[j].strip())
                            saved_attributes.append(f"\t    (at start (not " + f" ".join(gathered_info) + "))\n")
                            i += len(template)
                            gathered_info.clear()
                    for resource in resources:
                        saved_attributes.append(f"(at start (not (available {resource})))\n")
                else:
                    a_flagger = 5
            case ":add-effect":
                k = line.count(")") - line.count("(")
                if len(info) > 2 and (info[-1][-1] == ")" or info[-1] == ")" or k >= 0):
                    if (info[1] != "(and"):
                        raise Exception(f"Wrong syntax of added effect in activity {a_name}")
                    i = 2
                    gathered_info = []
                    while i < len(info):
                        gathered_info.append(info[i].strip())
                        if info[i][0] == "(":
                            info[i] = info[i].replace("(", "")
                            template = check_attr_naming(info[i], all_attributes)
                            for j in range(i + 1, i + len(template)):
                                check_var_existed(info[j], j - i, a_params, template)
                                gathered_info.append(info[j].strip())
                            saved_attributes.append(f"\t    (at start " + f" ".join(gathered_info) + ")\n")
                            i += len(template)
                            gathered_info.clear()
                    for resource in resources:
                        saved_attributes.append(f"(at start (available {resource}))\n")
                    if k == 1:
                        final_write(all_text, saved_attributes)
                    else:
                        a_flagger = 6
                else:
                    a_flagger = 6
    elif info[0] == ")": #finale
        return final_write(all_text, saved_attributes)
    else:
        raise Exception("UNKNOWN KEYWORD")
    return flagger, a_flagger #collect data and write it,  into one params, same with effects