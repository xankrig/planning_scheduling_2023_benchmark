#!/usr/bin/env python3
import argparse
import os
import random


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', help="input absolute path with demands input file")
    parser.add_argument('resource_file', help="input absolute path with resources helper file")
    parser.add_argument('domain_file', help="input absolute path with domain helper file") #extract needed quantified functions
    return parser.parse_args()

def write_file(filename, data):
    def write_extend(topath, data):
        path = os.path.dirname(args.input_file)
        new_path = f"{path}\\{topath}"
        with open(new_path, "w") as f:
            for item in data:
                f.write(item)

    new_file = filename.split(".")[0]
    new_file_duration = f"{new_file}_duration_makespan.pddl"
    new_file_duration_cost = f"{new_file}_duration_cost.pddl"
    new_file_classic = f"{new_file}_classic.pddl"
    write_extend(new_file_duration_cost, data[0])
    write_extend(new_file_classic, data[1])
    write_extend(new_file_duration, data[2])


def main():
    resources = []
    functions = []
    costs = []
    with open(args.resource_file) as f:
        for line in f:
            resources.append(line[:-1])
    with open(args.domain_file) as f:
        inf = False
        for line in f:
            if line.strip() == "" or line.strip() == "\n" or line.strip() == "\t" or line.strip() == "\t\n" or line.strip().split(";")[0] == "":
                continue
            info = line.strip().split(" ")
            if inf:
                if info[0] == ")": break
                else:
                    name = info[0][1:]
                    if "cost" in name.split("-"):
                        costs.append(name)
                    else: functions.append(name)
            elif info[0] == "(:functions": inf = not inf
            else: continue
    #costs = {}
    #for func in functions:
    #    costs[func] = func.replace('time', 'cost')
    filename = os.path.basename(args.input_file)
    keywords = ["(:objects", "(:init", "(:metric"]
    all_text = []
    all_text_intervals = []
    all_text_cost = []
    scope = 0
    stop = False
    with open(args.input_file) as f:
        resource_objects = []
        for line in f:
            info = line.split()
            if line == "\n":
                continue
            elif info[0] in keywords:
                all_text_cost.append(line)
                match info[0]:
                    case "(:objects":
                        scope = 1
                    case "(:init":
                        scope = 2
                    case "(:metric":
                        info[2] = "(total-cost))"
                        line = f"\t{f' '.join(info)}\n"
                all_text.append(line)
                all_text_intervals.append(line)
            else:
                match scope:
                    case 0:
                        all_text_intervals.append(line)
                        all_text_cost.append(line)
                    case 1:
                        if info[-1] in resources:
                            for i in range(len(info) - 2):
                                resource_objects.append(info[i])
                        all_text_intervals.append(line)
                        all_text_cost.append(line)
                    case 2:
                        if not stop:
                            for resource in resource_objects:
                                all_text.append(f"\t\t(available {resource})\n")
                                all_text_cost.append(f"\t\t(available {resource})\n")
                                #all_text_intervals.append(f"\t(available ?{resource} u0)\n")
                            stop = not stop
                        if info[0] == "(=":
                            if info[1][1:] in functions:
                                all_text_cost.append(line)
                            elif info[1][1:] in costs:
                                all_text_intervals.append(line)
                            else:
                                raise Exception("UNKNOWN FUNCTION")
                        else:
                            all_text_cost.append(line)
                            all_text_intervals.append(line)
                all_text.append(line)
    write_file(filename, [all_text, all_text_intervals, all_text_cost])
#find all the functions in newly transformed file - adapt it with timestamps


if __name__ == '__main__':
    args = parse_arguments()
    main()

